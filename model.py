import numpy as np
import matplotlib.pyplot as plt
import pandas
import math
import keras
import os
import keras
import sys
from keras.models import Sequential, Model, load_model
from keras.layers.wrappers import TimeDistributed
from keras.layers import Dense, Input, LSTM, Embedding, Flatten, Lambda,Activation
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error
import tensorflow as tf
from time import localtime, strftime
from keras import backend as K
import losses
from keras.models import model_from_json
import pickle
from sklearn.linear_model import LinearRegression

from keras.layers.normalization import BatchNormalization

class TrainNet:
	
	currentTime = strftime("%Y-%m-%d %H:%M:%S", localtime())
	
	def __init__(self, args, inputLengths, train_agnostic=True):
		self.args = args
		self.train_agnostic = train_agnostic
		self.inputLengths = inputLengths
	
		if args.load_model != None: 
			
			self.reset_model()
			self.model_nn.load_weights(args.load_model)
			
			adam = keras.optimizers.Adam(lr=self.args.lr, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False)
			self.model_nn.compile(loss='mse', optimizer=adam,metrics=(['mse','mae']))
			
		else:
			self.reset_model()
	
	def make_model_trainNet_agnostic(self,args):
		
		# line_id input
		input_delays = Input(shape=(self.inputLengths[1],), dtype='float32', name='input_delays')
		input_lt = Input(shape=(1,), dtype='float32', name='input_lt') 
		input_wd = Input(shape=(1,), dtype='float32', name='input_wd')
		input_features = Input(shape=(3,), dtype='float32', name='input_features')
		
		
		emb_id = Embedding(output_dim=self.inputLengths[1], input_dim=self.inputLengths[0], input_length=1)(input_id)
		emb_id = Flatten()(emb_id)
		
		emb_lt = Embedding(output_dim=64, input_dim=self.inputLengths[2], input_length=1)(input_lt)
		emb_lt = Flatten()(emb_lt)
		
		emb_wd = Embedding(output_dim=64, input_dim=7, input_length=1)(input_wd)
		emb_wd = Flatten()(emb_wd)
		
		train_information = keras.layers.concatenate([emb_id,emb_lt,emb_wd,input_features])
		#train_information = input_features

		
		layer = Dense(self.inputLengths[1],activation='tanh')(train_information)
		layer = Dense(self.inputLengths[1])(layer)
		attention_layer = keras.layers.Activation('softmax')(layer)
		
		output = keras.layers.Dot(-1,normalize=False)([attention_layer, input_delays])
		
		#output = keras.layers.Multiply()([graph_out,input_f2])
		#output = keras.layers.Add()([output,input_f1])
		
		#emb_id_2 = Embedding(output_dim=128, input_dim=self.inputLengths[0], input_length=1)(input_id)
		#emb_id_2 = Flatten()(emb_id_2)
		
		#layer = keras.layers.concatenate([emb_id_2,graph_out])
		
		#layer = Dense(512,activation='tanh')(layer)
		#layer = Dense(512,activation='tanh')(layer)
		#layer = Dense(512,activation='tanh')(layer)
		#output = Dense(1)(layer)
		
		#rms = keras.optimizers.RMSprop(lr=self.args.lr, rho=0.9, epsilon=None, decay=0.0)
		#adadelta = keras.optimizers.Adadelta(lr=1.0, rho=0.95, epsilon=None, decay=0.0)
		adam = keras.optimizers.Adam(lr=args.lr, beta_1=0.9, beta_2=0.999, epsilon=None, decay=args.decay, amsgrad=False)
		
		all_inputs = [input_id,input_delays]
		
		#concat_output = keras.layers.concatenate([main_output,sigma_output],axis=-1)
		model = Model(inputs=all_inputs, outputs=output)
		model.compile(loss='mse', optimizer=adam,metrics=(['mse','mae']))
		
		return model
		
	def make_model_trainNet_aware(self,args):
		
		# line_id input
		input_id = Input(shape=(1,), dtype='float32', name='input_id')
		
		
		input_delays = Input(shape=(self.inputLengths[1],), dtype='float32', name='input_delays')
		input_lt = Input(shape=(1,), dtype='float32', name='input_lt')
		input_wd = Input(shape=(1,), dtype='float32', name='input_wd')
		input_hour = Input(shape=(1,), dtype='float32', name='input_hour')
		mean = Input(shape=(1,), dtype='float32', name='mean')
		sigma = Input(shape=(1,), dtype='float32', name='sigma')
		input_numerical = Input(shape=(2,), dtype='float32', name='input_num')
		#input_srt = Input(shape=(1,), dtype='float32', name='input_srt')
		
		def _one_hot(x, num_classes):
			return K.one_hot(K.cast(x, 'uint8'),
                          num_classes=num_classes)
		
		
		id_ohe = Lambda(_one_hot,
			arguments={'num_classes': self.inputLengths[0]},
			input_shape=(1,))(input_id)
		id_ohe = Flatten()(id_ohe)
		lt_ohe = Lambda(_one_hot,
			arguments={'num_classes': self.inputLengths[2]},
			input_shape=(1,))(input_lt)
		lt_ohe = Flatten()(lt_ohe)
		wd_ohe = Lambda(_one_hot,
			arguments={'num_classes': 7},
			input_shape=(1,))(input_wd)
		wd_ohe = Flatten()(wd_ohe)
		hour_ohe = Lambda(_one_hot,
			arguments={'num_classes': 24},
			input_shape=(1,))(input_hour)
		hour_ohe = Flatten()(hour_ohe)
		
		#emb_delay = Embedding(output_dim=2, input_dim=2, input_length=1)(input_last_delay)
		#emb_delay = Flatten()(emb_delay)
		
		#input_layer = keras.layers.concatenate([input_last_delay,emb_id,emb_lt,emb_wd,input_hour])

		
		#train_information = input_features

		# we can think of this chunk as the input layer
		"""layer = Dense(self.inputLengths[1])(train_information)
		layer = BatchNormalization()(layer)
		layer = Activation('tanh')(layer)
		
		layer = Dense(self.inputLengths[1])(layer)
		layer = BatchNormalization()(layer)
		layer = Activation('tanh')(layer)
		
		layer = Dense(self.inputLengths[1])(layer)
		layer = BatchNormalization()(layer)
		layr = Activation('tanh')(layer)"""

		"""lt_ohe1 = keras.layers.Multiply()([lt_ohe,input_numerical1])
		lt_ohe2 = keras.layers.Multiply()([lt_ohe,input_numerical2])
		lt_ohe3 = keras.layers.Multiply()([lt_ohe,input_numerical3])"""

		train_information = keras.layers.concatenate([id_ohe,lt_ohe,hour_ohe, wd_ohe,input_numerical])
		#train_information_graph = keras.layers.concatenate([id_ohe])

		#layer = Dense(self.inputLengths[1])(id_ohe)
		layer = Dense(self.inputLengths[1],activation='tanh')(train_information)
		layer = Dense(self.inputLengths[1],activation='tanh')(layer)
		layer_attention = Dense(self.inputLengths[1])(layer)
		attention_layer = keras.layers.Activation('softmax')(layer_attention)
		graph_out = keras.layers.Dot(-1,normalize=False)([input_delays, attention_layer])
		
		layer = keras.layers.concatenate([attention_layer, train_information])
		layer = Dense(self.inputLengths[1],activation='tanh')(layer)
		layer = Dense(self.inputLengths[1],activation='tanh')(layer)
		layer_attention = Dense(self.inputLengths[1])(layer)
		attention_layer2 = keras.layers.Activation('softmax')(layer_attention)
		graph_out2 = keras.layers.Dot(-1,normalize=False)([input_delays, attention_layer2])
		
		"""layer = Dense(self.inputLengths[1],activation='tanh')(train_information)
		layer = Dense(self.inputLengths[1])(layer)
		
		
		
		attention_layer = keras.layers.Activation('softmax')(layer)
		dropout_attention = keras.layers.Dropout(0.5)(attention_layer)
		graph_out_2 = keras.layers.Dot(-1,normalize=False)([input_delays, dropout_attention])"""
		
		
		"""layer = Dense(self.inputLengths[1],activation='tanh')(train_information)
		layer = Dense(self.inputLengths[1],activation='tanh')(layer)

		attention_layer = keras.layers.Activation('softmax')(layer)
		graph_out3 = keras.layers.Dot(-1,normalize=False)([attention_layer, input_delays])"""
		
		"""graph_out = keras.layers.Multiply()([graph_out,input_f2])
		graph_out = keras.layers.Add()([graph_out,input_f1])"""
		
		#train_information = keras.layers.concatenate([emb_id,emb_lt,emb_wd,input_features])
		
		
		
		layer = keras.layers.concatenate([train_information,graph_out])
		
		layer = Dense(2048,activation='tanh')(layer)
		layer = Dense(2048,activation='tanh')(layer)
		
		output =  Dense(1)(layer)
		#output = keras.layers.Average()([graph_out,graph_out2])
		#output=graph_out
		#layer = Dense(2048,activation='tanh')(layer)
		#output = Dense(1)(layer)
		
		#rms = keras.optimizers.RMSprop(lr=self.args.lr, rho=0.9, epsilon=None, decay=0.0)
		#adadelta = keras.optimizers.Adadelta(lr=1.0, rho=0.95, epsilon=None, decay=0.0)
		adam = keras.optimizers.Adam(lr=args.lr, beta_1=0.9, beta_2=0.999, epsilon=None, decay=args.decay, amsgrad=False)
		
		all_inputs = [input_lt, input_wd, input_hour, input_numerical, input_id, input_delays,mean,sigma]
		
		#concat_output = keras.layers.concatenate([main_output,sigma_output],axis=-1)
		model = Model(inputs=all_inputs, outputs=output)
		model.compile(loss='mse', optimizer=adam,metrics=(['mse','mae']))
		
		self.softmaxLayerModel = Model(inputs=all_inputs, outputs=attention_layer)
		self.softmaxLayerModel.compile(loss='mse', optimizer=adam,metrics=(['mse','mae']))
		
		self.softmaxLayerModel2 = Model(inputs=all_inputs, outputs=attention_layer2)
		self.softmaxLayerModel2.compile(loss='mse', optimizer=adam,metrics=(['mse','mae']))
		
		return model
		
	
	
	def reset_model(self):
		if self.train_agnostic:
			self.model_nn = self.make_model_trainNet_agnostic(self.args)
		else:
			self.model_nn = self.make_model_trainNet_aware(self.args)
			
		
		
	def fit(self,X,y,X_val=[], y_val=[],use_callbacks=False,verbose=1):
		model_nn = self.model_nn
		batch_size = self.args.batch_size
		num_epochs = self.args.num_epochs
		cpu_count = self.args.cpu_count
		
		logPath = "log/" + self.currentTime
		
		if not os.path.isdir(logPath):
			os.makedirs(logPath)
			
		if not os.path.isdir("save"):
			os.makedirs("save")
		
		validation = None
		if len(X_val)>0 and len(y_val)>0:
			validation = (X_val,y_val)
		
		callbacks = []
		if use_callbacks:
			# prepare callbacks for saving and tensorboard
			tbCallBack = TrainValTensorBoard(log_dir=logPath, histogram_freq=0, write_graph=False, write_images=False)		
			saveCallBack = keras.callbacks.ModelCheckpoint("save/" + self.currentTime + ".cp", monitor='val_loss', verbose=0, save_best_only=False, save_weights_only=False, mode='auto', period=1)
			callbacks = [tbCallBack,saveCallBack]
		
		# training loop
		return model_nn.fit(X, y, validation_data=validation, batch_size=batch_size, epochs=num_epochs,verbose=1,callbacks=callbacks)
			
		
		
	def fit_generator(self,generator_X,generator_y, length_train, length_test, use_callbacks=False):
		model_nn = self.model_nn
		num_epochs = self.args.num_epochs
		cpu_count = self.args.cpu_count
		
		callbacks = []
		if use_callbacks:
			# prepare callbacks for saving and tensorboard
			tbCallBack = TrainValTensorBoard(log_dir=logPath, histogram_freq=0, write_graph=False, write_images=False)		
			saveCallBack = keras.callbacks.ModelCheckpoint("save/" + self.currentTime + ".cp", monitor='val_loss', verbose=0, save_best_only=False, save_weights_only=False, mode='auto', period=1)
			callbacks = [tbCallBack,saveCallBack]
		
		model_nn.fit_generator(generator_X, length_train, num_epochs, #len(dataset_train)//(batch_size)
							validation_data=generator_y,
							validation_steps=length_test, #len(dataset_test)//(batch_size)
							max_queue_size=128,
							workers=1, 
							use_multiprocessing=False,
							callbacks=callbacks)
							
	def test(self, dataX, dataY):
		model_nn = self.model_nn
		data_generator_test = self.data_generator_test
		cpu_count = self.args.cpu_count
		
		print(self.model_nn.evaluate(x=dataX, y=dataY))
	
	"""def test_generator(self):
		model_nn = self.model_nn
		data_generator_test = self.data_generator_test
		cpu_count = self.args.cpu_count
		dataset_test = self.dm.dataset_test
		batch_size = self.args.batch_size
		
		print(self.model_nn.evaluate_generator(data_generator_test, steps=len(dataset_test)//(batch_size), max_queue_size=128, workers=1, use_multiprocessing=False, verbose=0))"""
		
		
	def predict(self,dataX):
		model_nn = self.model_nn
		cpu_count = self.args.cpu_count
		
		
		return model_nn.predict(dataX).flatten()
		
	def predict_sigma(self,dataX):
		sigmaModel = self.sigmaModel
		cpu_count = self.args.cpu_count
		
		return sigmaModel.predict(dataX).flatten()
		
	def predict_softmax_max(self,dataX):
		softmaxLayerModel = self.softmaxLayerModel
		cpu_count = self.args.cpu_count
		
		#softmax_argmax = np.argmax(softmaxLayerModel.predict(dataX), axis=1)
		softmax_max = softmaxLayerModel.predict(dataX).max(axis=-1)
		return softmax_max
		
	def predict_softmax_argmax(self,dataX):
		softmaxLayerModel = self.softmaxLayerModel
		cpu_count = self.args.cpu_count
		
		softmax_argmax = np.argmax(softmaxLayerModel.predict(dataX), axis=1)
		return softmax_argmax
		
	def predict_softmax(self,dataX):
		softmaxLayerModel = self.softmaxLayerModel
		cpu_count = self.args.cpu_count
		
		softmax = softmaxLayerModel.predict(dataX)
		return softmax
		
	def predict_softmax2(self,dataX):
		softmaxLayerModel = self.softmaxLayerModel2
		
		softmax = softmaxLayerModel.predict(dataX)
		return softmax
		
	def predict_graphOut(self,dataX):
		output_graphModel = self.output_graphModel
		cpu_count = self.args.cpu_count
		
		return output_graphModel.predict(dataX).flatten()
		

# found this on https://stackoverflow.com/questions/47877475/keras-tensorboard-plot-train-and-validation-scalars-in-a-same-figure?rq=1
# this enables Tensorboard to monitor training / validation losses					
class TrainValTensorBoard(keras.callbacks.TensorBoard):
    def __init__(self, log_dir='./logs', **kwargs):
        # Make the original `TensorBoard` log to a subdirectory 'training'
        training_log_dir = os.path.join(log_dir, 'training')
        super(TrainValTensorBoard, self).__init__(training_log_dir, **kwargs)

        # Log the validation metrics to a separate subdirectory
        self.val_log_dir = os.path.join(log_dir, 'validation')

    def set_model(self, model):
        # Setup writer for validation metrics
        self.val_writer = tf.summary.FileWriter(self.val_log_dir)
        super(TrainValTensorBoard, self).set_model(model)

    def on_epoch_end(self, epoch, logs=None):
        # Pop the validation logs and handle them separately with
        # `self.val_writer`. Also rename the keys so that they can
        # be plotted on the same figure with the training metrics
        logs = logs or {}
        val_logs = {k.replace('val_', ''): v for k, v in logs.items() if k.startswith('val_')}
        for name, value in val_logs.items():
            summary = tf.Summary()
            summary_value = summary.value.add()
            summary_value.simple_value = value.item()
            summary_value.tag = name
            self.val_writer.add_summary(summary, epoch)
        self.val_writer.flush()

        # Pass the remaining logs to `TensorBoard.on_epoch_end`
        logs = {k: v for k, v in logs.items() if not k.startswith('val_')}
        super(TrainValTensorBoard, self).on_epoch_end(epoch, logs)

    def on_train_end(self, logs=None):
        super(TrainValTensorBoard, self).on_train_end(logs)
        self.val_writer.close()
							
	
