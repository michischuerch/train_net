import numpy as np
import pandas as pd
from datetime import datetime
from collections import OrderedDict
import sys
import time
import data_utils
import preprocessing
import os
import pickle
from multiprocessing import Pool
import tensorflow as tf
import sklearn as sklearn
from os import listdir
from sklearn.utils import shuffle
from functools import partial
import matplotlib.pyplot as plt
import math
from sklearn.externals import joblib

# the DataManager is to make the access and transformation of the data more easy and it saves the data to hard disk such that we can reuse the preprocessing,
# 
class DataManager(object):
	
	allData = None
	allData_preprocessed = None
	dataset_train = None
	dataset_test = None
	
	def __init__(self, args, useGenerators=True):
		self.args = args
		
		if not os.path.isdir("temp"):
			os.makedirs("temp")
		
		# generate or load training dataset, check if we did it already first
		"""if os.path.isfile("temp/dataset_train_preprocessed.pkl") and not args.refreshPreprocessing:
			self.dataset_train = pd.read_pickle("temp/dataset_train_preprocessed.pkl")
		else:
			print("preprocessing training set...\n")
			self.dataset_train = self.load_data(args.datapath_train)
			self.dataset_train.to_pickle("temp/dataset_train_preprocessed.pkl")"""
		
		if self.allData!= None:
			del self.allData
		if self.allData_preprocessed != None:
			del self.allData_preprocessed
		if self.dataset_train != None:
			del self.dataset_train
		if self.dataset_test != None:
			del self.dataset_test
		
		allPaths = [(dp,f) for dp, dn, fn in os.walk(os.path.expanduser(args.data_path)) for f in fn]		
		self.allData = self.load_data(allPaths)
		#self.allData = preprocessing.remove_outliers(self.allData)
		
		
		
		
		
		self.load_vocabs(self.allData)
		
		# add edge_ids to dataset as a field
		for d in self.allData:
			d['edge_id'] = [self.edge_vocab_map[e] for e in list(zip(d['BPUIC'],d['next_BPUIC']))]
			d['lt_id'] = [self.lt_vocab_map[lt] for lt in d['LINIEN_TEXT'].astype(str)]
			d['train_id'] = [self.line_id_vocab_map[train_id] for train_id in d['LINIEN_ID']]
		
		self.amount_per_edge = pd.concat(self.allData).groupby(['edge_id']).size().to_dict()
		
		self.graph_vocab = {}
		self.graph_string = 'graph_id'
		
		counter = 0
		for k,v in list(self.amount_per_edge.items()):
			if v > self.args.min_updates:
				self.graph_vocab[k] = counter
				counter += 1
		print("graph vocab:")
		print(len(self.graph_vocab))
		
		for d in self.allData:
			d['graph_id'] = [self.graph_vocab.get(eid,-1) for eid in d['edge_id']]
			
		self.objective_string = 'running_time'
		self.name_to_bpuic_map = dict([[v,k] for k,v in self.bpuic_to_name_map.items()])
		
		self.reverseMapEdge = dict(zip(self.edge_vocab_map.values(),self.edge_vocab_map.keys()))
		self.reverseMapGraph = dict(zip(self.graph_vocab.values(),self.graph_vocab.keys()))
		#print(self.reverseMapGraph)
		#print(self.reverseMapGraph.values())
		self.graph_edges = []
		
		
		for k,v in self.reverseMapGraph.items():
			self.graph_edges.append((self.bpuic_vocab_map[self.reverseMapEdge[v][0]],self.bpuic_vocab_map[self.reverseMapEdge[v][1]]))
		
		if self.args.only_top_k > -1:
			# find top k edge
			
			all_edges = [e[1] for e in sorted(list(dict(zip(self.amount_per_edge.values(),self.amount_per_edge.keys())).items()),reverse=True)]
			
			
			"""for i, e in enumerate(all_edges):
				print(i)
				print((self.bpuic_to_name_map[str(self.reverseMapEdge[e][0])],self.bpuic_to_name_map[str(self.reverseMapEdge[e][1])]) )
				print("\n")
			sys.exit()"""
			
			self.top_k_edge = all_edges[self.args.only_top_k]
		
			print("edges:")
			print(self.top_k_edge)
			
			self.edge_name = self.bpuic_to_name_map[str(self.reverseMapEdge[self.top_k_edge][0])] + " - " + self.bpuic_to_name_map[str(self.reverseMapEdge[self.top_k_edge][1])]
			
			print(self.edge_name)
			
			#self.dataset_train = self.dataset_train[self.dataset_train["edge_id"]==self.top_k_edge]
			#self.dataset_test = self.dataset_test[self.dataset_test["edge_id"]==self.top_k_edge]
			#print(len(self.dataset_train))
			
			
			
		if self.args.only_top_k > -1 and os.path.isfile("temp/edges/edge"+str(self.args.only_top_k)+".pkl") and not self.args.refreshPreprocessing:
			with open("temp/edges/edge"+str(self.args.only_top_k)+".pkl",'rb') as f:
				allData_preprocessed = joblib.load(f)
		else:
			
			allData_preprocessed = self.compute_inputs(self.allData, allPaths)
			
			if self.args.only_top_k > -1:
				
				joblib.dump(allData_preprocessed, "temp/edges/edge"+str(self.args.only_top_k)+".pkl") 
		
		"""splitting = np.arange(len(allData_preprocessed))
		np.random.shuffle(splitting)"""
		
		"""print(np.sort(splitting[:int(len(allData_preprocessed)*args.test_ratio)]))
		print(np.sort(splitting[int(len(allData_preprocessed)*args.test_ratio):]))
		sys.exit()"""
		
		np.random.shuffle(allData_preprocessed)
		
		self.allData_preprocessed = allData_preprocessed
		
		print("all data size")
		print(len(np.concatenate([d[0][0] for d in self.allData_preprocessed])))
		
		self.dataset_test = allData_preprocessed[:int(len(allData_preprocessed)*args.test_ratio)]
		self.dataset_train = allData_preprocessed[int(len(allData_preprocessed)*args.test_ratio):]
		
		self.exampleGraph = None
		
		
		# printing sizes of the different vocabularies (amount of unique vertices / edges)
		print("num bpuic: " + str(len(self.bpuic_to_name_map)))
		print("num lineid: " + str(len(self.line_id_vocab_map)))	
		print("num edges: " + str(len(self.edge_vocab_map)))
		print("num lt: " + str(len(self.lt_vocab_map)))
		print("num graph items: " + str(len(self.graph_vocab)))
		#print("num vt: " + str(len(self.vt_vocab_map)))
	
		
	def load_data(self,allPaths):
		
		
		# do preprocessing in parallel
		p = Pool(self.args.cpu_count)
		allData = p.map(self.load_data_one_day, allPaths)		
		p.close()
		return allData	
		
	def load_data_one_day(self, datapath):
		dp,f = datapath
		
		# see if we did the preprocessing already
		
		loadPath = os.path.join(self.args.load_path, f)
		
		if os.path.isfile(loadPath) and not self.args.refreshPreprocessing:
			return pd.read_pickle(loadPath)
		
		else:
			data = data_utils.load_dataset(self.args,os.path.join(dp, f))
			data = preprocessing.extractImportantFields(data)
			
			data.to_pickle(loadPath)
			return data
	
	def compute_inputs(self,allData,allPaths):
		
		# do preprocessing in parallel
		p = Pool(self.args.cpu_count)
		allData = p.map(self.compute_inputs_one_day, zip(allData,allPaths))
		p.close()
		return allData	
		
	def compute_inputs_one_day(self, data_and_paths):
		data, datapath = data_and_paths
		dp,f = datapath
		
		# see if we did the preprocessing already
		loadPath = os.path.join(self.args.load_path, f + ".graph")
		
		if os.path.isfile(loadPath) and not self.args.refreshPreprocessing:
			data_preprocessed = joblib.load(loadPath)
		
		else:
			
			data_preprocessed = self.calculate_dataX_dataY(data)
			joblib.dump(data_preprocessed, loadPath)
			
		if self.args.only_top_k > -1:
			
			mask = [True if edge_id == self.top_k_edge else False for edge_id in data_preprocessed[0][1]]
			data_preprocessed = ([d[mask] for d in data_preprocessed[0]],data_preprocessed[1][mask])
			
			
		return data_preprocessed
		
	
	# here I build the feature vector for one row
	def extract_one_row(self,dataset, i):
		sample = dataset.iloc[i]
		
		lt = self.lt_vocab_map.get(sample['LINIEN_TEXT'])
		wd = sample['ACTUAL_TIME_DEPARTURE'].weekday()
		hour = sample['ACTUAL_TIME_DEPARTURE'].hour 
		delay = sample['DEPARTURE_DELAY']
		
		date_delta = sample['next_SCHEDULED_TIME_ARRIVAL']-sample['SCHEDULED_TIME_DEPARTURE']
		
		srt = int(date_delta.total_seconds())
		
		
		#delay = self.bin_delay(sample['delay'])
		#edge = sample['edge_train_id']
		edge = sample['edge_id']
		
		#mean = self.mean_per_edge[sample['edge_id']]
		#sigma = self.sigma_per_edge[sample['edge_id']]
		
		y = sample[self.objective_string] 
		
		# TODO change this mean
		x = [[lt,wd,hour,delay,srt],edge]
		
		return x, y
	
	# build the whole matrices, this takes a while ...
	def calculate_dataX_dataY(self, dataset):
		
		timer = time.time()
		train_information_full = np.zeros((dataset.shape[0],5), dtype=float)
		edge_information_full = np.zeros(dataset.shape[0], dtype=float)
		graph_information_full = np.zeros((dataset.shape[0], len(self.graph_vocab)), dtype=float)
		
		
		dataY = np.zeros(dataset.shape[0], dtype=int)
		
		i = 0
		for (x,y) in self.generators_dataX_dataY(dataset):
			
			start = i*self.args.batch_size
			end = (i+1)*self.args.batch_size
			
			train_information_full[start:end] = x[0]
			edge_information_full[start:end] = x[1]
			graph_information_full[start:end] = x[2]
			
			dataY[start:end] = y
			i+=1
			
			if i*self.args.batch_size >= dataset.shape[0]:
				break
		
		# print how much time we used
		elapsed = time.time() - timer
		print("done, time used: " + str(elapsed))	
		return [train_information_full, edge_information_full, graph_information_full], dataY
	
	def generators_dataX_dataY_precomputed(self,dataX,dataY):
		
		batch_size = self.args.batch_size
		while True:
			for i in range(0,len(dataX[0])//batch_size,batch_size):
				yield [dataX[0][i:i+batch_size], 
					dataX[1][i:i+batch_size],
					dataX[2][i:i+batch_size],
					dataX[3][i:i+batch_size],
					dataX[4][i:i+batch_size],
					dataX[5][i:i+batch_size],
					dataX[6][i:i+batch_size],
					dataX[7][i:i+batch_size]
					], dataY[i:i+batch_size]
			
		
	
	# this only calculates one batch at the time for efficiency reasons	
	def generators_dataX_dataY(self,data,shuffle_batch=False, use_preprocessing = False):
		
		batch_size = self.args.batch_size
		
		train_information_batch = np.zeros((batch_size,5), dtype=int)
		edge_information_batch = np.zeros(batch_size, dtype=int)
		#f1_batch = np.zeros(batch_size, dtype=float)
		#f2_batch = np.zeros(batch_size, dtype=float)	
		#f3_batch = np.zeros(batch_size, dtype=float)	
		
		graph_information_batch = np.zeros((batch_size, len(self.graph_vocab)), dtype=float)
		#inter_train_time_since_update_batch = np.zeros((batch_size, len(self.graph_vocab)), dtype=float)
		
		y_batch = np.zeros(batch_size, dtype=int)
		
		# sort data in time
		data = data.sort_values(by=['ACTUAL_TIME_DEPARTURE'])
		
		update_data = data.sort_values(by=['next_ACTUAL_TIME_ARRIVAL'])
		
		current_batch_sample = 0
		while True:
			
			# TODO changed this
			graph_information = np.zeros(len(self.graph_vocab), dtype=float)
			
			current_update_i = 0
			
			for i in range(len(data)):
				
				x,y = self.extract_one_row(data,i)
				
				train_information_batch[current_batch_sample] = np.array(x[0])
				edge_information_batch[current_batch_sample] = x[1]
				
				# update all entries until the current time
				while current_update_i<update_data.shape[0] and update_data.iloc[current_update_i]['next_ACTUAL_TIME_ARRIVAL'] <= data.iloc[i]['ACTUAL_TIME_DEPARTURE']:
					
					"""if(current_update_i>0):
						inter_train_time_since_update += (update_data.iloc[current_update_i]['next_ACTUAL_TIME_ARRIVAL'] - update_data.iloc[current_update_i-1]['next_ACTUAL_TIME_ARRIVAL']).seconds		
					"""
					iidd = update_data.iloc[current_update_i][self.graph_string]
					
					if iidd != -1 and update_data.iloc[current_update_i]["scheduled_running_time"] != 0 and update_data.iloc[current_update_i]["ARRIVAL_DELAY"] < 30*60:
						graph_information[iidd] = update_data.iloc[current_update_i]["running_time"]
						#inter_train_time_since_update[iidd] = 0
					
					current_update_i+=1
				
				graph_information_batch[current_batch_sample,:] = graph_information[:]
				
				y_batch[current_batch_sample] = y
				current_batch_sample += 1
				
				if current_batch_sample == batch_size or i == len(data)-1:
					
					# shuffle batches
					mask = np.arange(current_batch_sample)
					if shuffle_batch: 
						mask = np.random.permutation(current_batch_sample)
					
					train_information_batch_ = train_information_batch[mask]
					edge_information_batch_ = edge_information_batch[mask]
					graph_information_batch_ = graph_information_batch[mask]
					
					y_batch_ = y_batch[mask]
					
					current_batch_sample = 0
					
					yield [train_information_batch_, edge_information_batch_, graph_information_batch_], y_batch_
					
					
	def load_vocabs(self, allData):
		args = self.args
		
		# generate or load the vocabulary for LINIEN_TEXT
		if os.path.isfile("temp/vocab_lt.pkl") and not args.refreshPreprocessing:
			with open("temp/vocab_lt.pkl",'rb') as f:
				self.lt_vocab_map = pickle.load(f)
		else:
			# make a new vocabulary
			lt_vocab = np.unique(pd.concat(allData)['LINIEN_TEXT'].astype(str))
			
			self.lt_vocab_map = {}
			for i, lt in enumerate(lt_vocab):
				self.lt_vocab_map[lt] = i
				
			with open("temp/vocab_lt.pkl",'wb') as f:
				pickle.dump(self.lt_vocab_map, f)
		
		# generate or load the vocabulary for the edge (BPUIC, next_BPUIC)
		if os.path.isfile("temp/vocab_edge.pkl") and not args.refreshPreprocessing:
			with open("temp/vocab_edge.pkl",'rb') as f:
				self.edge_vocab_map = pickle.load(f)

		else:
			# make a new vocabulary	
			edge_vocab = np.unique(list(zip(pd.concat(allData)['BPUIC'],pd.concat(allData)['next_BPUIC'])),axis=0)
			self.edge_vocab_map = {}
			counter = 0
			for edge in edge_vocab:
					self.edge_vocab_map[edge[0],edge[1]] = counter
					counter += 1
			with open("temp/vocab_edge.pkl",'wb') as f:
				pickle.dump(self.edge_vocab_map, f)
		
		#generate or load the vocabulary for the particular train (LINIEN_ID)
		if os.path.isfile("temp/vocab_line_id.pkl") and not args.refreshPreprocessing:
			with open("temp/vocab_line_id.pkl",'rb') as f:
				self.line_id_vocab_map = pickle.load(f)

		else:
			# make a new vocabulary	
			line_id_vocab = np.unique(pd.concat(allData)['LINIEN_ID'])
			self.line_id_vocab_map = dict(map (lambda t: (t[1], t[0]), enumerate(line_id_vocab)))
				
			with open("temp/vocab_line_id.pkl",'wb') as f:
				pickle.dump(self.line_id_vocab_map, f)
		
		# generate or load the vocabulary for the BPUIC to names
		if os.path.isfile("temp/vocab_bpuic_to_name.pkl") and not args.refreshPreprocessing:
			with open("temp/vocab_bpuic_to_name.pkl",'rb') as f:
				self.bpuic_to_name_map = pickle.load(f)
		else:
			# this gives a vocabulary to map all operation points to their names
			self.bpuic_to_name_map = dict(np.unique(list(zip(pd.concat(allData)['BPUIC'].astype(int),pd.concat(allData)['HALTESTELLEN_NAME'])),axis=0))
			
			for bpuic, hname in np.unique(list(zip(pd.concat(allData)['next_BPUIC'].astype(int),pd.concat(allData)['next_HALTESTELLEN_NAME'])),axis=0):
				self.bpuic_to_name_map[bpuic] = hname
			
			
			with open("temp/vocab_bpuic_to_name.pkl",'wb') as f:
				pickle.dump(self.bpuic_to_name_map, f)
				
		# generate or load the vocabulary for BPUICs 
		if os.path.isfile("temp/vocab_BPUIC.pkl") and not args.refreshPreprocessing:
			with open("temp/vocab_BPUIC.pkl",'rb') as f:
				self.bpuic_vocab_map = pickle.load(f)

		else:
			# make a new vocabulary	
			bpuic_vocab = np.unique(np.append(pd.concat(allData)['BPUIC'], pd.concat(allData)['next_BPUIC']))
			self.bpuic_vocab_map = {}
			for i, bpuic in enumerate(bpuic_vocab):
				self.bpuic_vocab_map[bpuic] = i
			with open("temp/vocab_BPUIC.pkl",'wb') as f:
				pickle.dump(self.bpuic_vocab_map, f)
					
					
