import pandas as pd
import data_utils
import sys
import numpy as np
from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt
from scipy.misc import imread
import matplotlib.cm as cm

class SwissMap(object):
	
	def __init__(self, args):
		self.args = args
		
		# load dataset
		self.map_data = data_utils.load_map(args.map_path)
		
		self.name_to_coord_map = {}
		for i,row in self.map_data.iterrows():
			self.name_to_coord_map[row['BPK']] = (row['long'],row['lat'])
		
		fig = plt.figure(figsize=(8, 8))
		
		# longitude of lower left hand corner of the selected map domain
		llcrnrlon = np.min(self.map_data['long'])
		# latitude of lower left hand corner of the selected map domain
		llcrnrlat = np.min(self.map_data['lat'])
		# longitude of upper right hand corner of the selected map domain
		urcrnrlon = np.max(self.map_data['long'])
		# latitude of upper right hand corner of the selected map domain
		urcrnrlat = np.max(self.map_data['lat'])
		
		self.map = Basemap(projection='cyl', resolution='c',llcrnrlon=llcrnrlon, llcrnrlat=llcrnrlat, urcrnrlon=urcrnrlon, urcrnrlat=urcrnrlat)
		
		plt.imshow(imread("data/swiss_world_map.png"), extent=[5.95, 10.5, 45.8, 47.785])
		
	
	# each of our edges corresponds to an entry in the map, we have to find that entry..
	def print_edge_map(self,dm,edges,softmax=np.array([]), colors=np.array([])):
		
		bn_map = dm.bpuic_to_name_map
		
		current_color = 0
		
		#softmax = np.log(softmax)
		for eid in edges:
			
			s,t = dm.reverseMapEdge[eid]
			
			
			name1 = bn_map[str(s)]
			name2 = bn_map[str(t)]
			
			if len(softmax) > 0:
				alpha = (softmax[edges[eid]]-np.min(softmax))/(np.max(softmax)-np.min(softmax))
			else:
				alpha = 1
				
			if len(colors) > 0:
				self.print_edge(name1,name2,color=cm.brg(colors[current_color]),alpha=alpha)
				current_color = (current_color + 1) % len(colors)
			else:
				self.print_edge(name1,name2,color='red',alpha=alpha)
			
			
			
		
	def print_edge(self, name1, name2, color, alpha,linewidth=0.8,label=""):
		
		if name1 not in self.name_to_coord_map.keys():
			print(name1)
		elif name2 not in self.name_to_coord_map.keys():
			print(name2)
		else:
			ll1 = self.name_to_coord_map[name1]
			ll2 = self.name_to_coord_map[name2]
			
			py1,px1 = self.map([ll1[0], ll2[0]], [ll1[1],ll2[1]])
			
			if color == 'lime':
				py1 = np.add(py1,0.01)
			
			plt.plot(px1,py1, color=color,linewidth=linewidth,alpha=alpha,label=label)
		
	
	def print_map(self):
	
		for lon,lat in list(zip(self.map_data['long'],self.map_data['lat'])):
			
			y, x = self.map(lon, lat)
			plt.plot(x, y, 'ok', markersize=1.5)
	
	def remove_ticks(self):
		# remove ticks
		plt.tick_params(
			axis='x',          # changes apply to the x-axis
			which='both',      # both major and minor ticks are affected
			bottom=False,      # ticks along the bottom edge are off
			top=False,         # ticks along the top edge are off
			labelbottom=False) # labels along the bottom edge are off
			
		plt.tick_params(
			axis='y',          # changes apply to the x-axis
			which='both',      # both major and minor ticks are affected
			bottom=False,      # ticks along the bottom edge are off
			top=False,         # ticks along the top edge are off
			labelbottom=False) # labels along the bottom edge are off
	
	def save_swiss_map(self,dm):
		self.print_edge_map(dm,edges=dm.graph_vocab)
		self.print_map()
		
		plt.axis('off')
		
		plt.savefig("data/map.png")
		
	def save_swiss_map_amount_per_edge(self,dm):
		
		a_map = dm.amount_per_edge_train
		max_amount = np.max(list(a_map.values()))
		min_amount = np.min(list(a_map.values()))
		
		sorted_by_value = sorted(a_map.items(), key=lambda kv: kv[1])
		
		reverseMap = dict(zip(dm.edge_vocab_map.values(),dm.edge_vocab_map.keys()))
		
		edges = []
		colors = []
		counter = 0
		for edge_id,v in sorted_by_value:
			
			s,t = reverseMap[int(edge_id)]
			
			"""if not(counter < len(sorted_by_value)*0.1 or v < 14):
				colors.append(0.5)
				
			counter += 1"""
			
			
			colors.append((np.log(v)-np.log(min_amount))/(np.log(max_amount)-np.log(min_amount))/2 + 0.5)
			edges.append((s,t))
			
		
		self.print_edge_map(dm,colors=colors,edges=edges)
		#self.print_map()
		
		plt.savefig("data/map_amount_per_edge.png")
	
	def save_softmax_map(self,dm, softmax, edge_id, path):
		
		"""top3 = softmax.argsort()[-10:][::-1]
		
		top3Softmax = np.zeros(len(softmax))
		top3Softmax[top3] = 1"""
		
		self.print_edge_map(dm,edges=dm.graph_vocab, softmax=softmax)
		
		s,t = dm.reverseMapEdge[int(edge_id)]
		
		name1 = dm.bpuic_to_name_map[str(s)]
		name2 = dm.bpuic_to_name_map[str(t)]
		
		#plt.title(name1 + " - " + name2) 
		
		self.print_edge(name1, name2, 'lime', 1.0, linewidth=1.2)
		
		plt.axis('off')
		
		plt.savefig(path)
		
		
		
		
