\chapter{Experiments}
\label{sec:experiments}
%Single Edge Experiments

%input: time, day, train-type

%- Random Forest Experiments 
%- Gaussian Process Experiments
%- Attention Model
%- baselines

%input: graph edges array

%- Ridge
%- Lasso 
%- Attention Model

%Experiments on whole graph

%- Attention Model
%- graph net

%Reducing input size, training on whole data / on edge data, sunny vs rainy day


In this chapter we perform multiple tests with different architectures and input configurations. All models were trained with 5-fold cross-validation and we display and compare models by the mean and variance of all folds.

Our aim for the experiments is to minimize the objective function defined in \refsec{sec:problem_formulation}. The final result of the attention model can be observed in \reftable{table:final_result}. All models use the same random seed and the splits for the cross-validation are always the same.

%To start, we look at a simplified version of the task by training and testing on data that travels over the same connection in the graph. To reduce the selection bias we perform the one edge experiments on these 4 connections individually:

We decide to train and evaluate our models on individual edges in the dataset. For this we select 4 connections randomly of the 200 edges that have the most train movement. The connections can be seen in \reffig{fig:e9_observe_edges}.

\begin{figure*}[htp]
	\centering
	\includegraphics[width=1.0\linewidth]{images/e9_observe_edges.png}					
	
	\caption{The connections we will be performing our experiments with. }
	\label{fig:e9_observe_edges}
\end{figure*}

\newpage
\section{Mean per edge}
\label{subsec:experiments_mean_per_edge}

$\textbf{Input Data: }$ We train and evaluate on the data of 4 edges individually. Here we need to calculate the mean running time for each edge. We extract these quantities using the training set $\mathcal{D}_{train}$. 

$\textbf{Idea: }$ The first baseline predicts the mean running time over all vehicle movement that travels over the same edge. The results of this experiment thus will be used as a trivial baseline that doesn't require the training of a model. For an individual edge $e$ the error of this first baseline equals the \textit{mean deviation} of all running times $\mathbf{y} \in \mathbb{R}^{n_e}$ that occur on $e$:

$$ error_e = \frac{1}{n_e} \sum_{i=1}^{n_e} |\mathbf{y}_{i} - \mu_e |, \quad \mu_e = \frac{1}{n_e} \sum_{i=1}^{n_e} \mathbf{y}_{i},$$

where $n_e$ is the amount of samples traveling over the connection $e$. 

This is a measure of the variance similar to the standard deviation $\sigma_e$. In \reffig{fig:distance_vs_val_error} we show the similarities between the two quantities. We also show that there is a relation between the average travel time $\mu_e$ and variance $\sigma_e$. In general, we see that an edge with a high average running time $\mu_e$ has also a high variance $\sigma_e$. One possible explanation for this is that vehicles tend to be more affected by weather on longer travel distances.

\begin{figure*}[htp]
	\centering
	\begin{tikzpicture}[yscale=0.72,xscale=0.72]
	\begin{axis}[%
	xlabel={$\sigma_e[\text{seconds}]$},
	ylabel={$error_e\text{  [seconds]}$},
	scatter/classes={%
		a={mark=*,draw=blue}}]
	\addplot[scatter,only marks,%
	scatter src=explicit symbolic]%
	table[meta=label] {
		x y label
		28 17.16 a
		38 20.8 a
		56 38.91 a
		278 78.3 a
	};
	\end{axis}
	\end{tikzpicture}
	\begin{tikzpicture}[yscale=0.72,xscale=0.72]
	\begin{axis}[%
	xlabel={$\sigma_e[\text{seconds}]$},
	ylabel={$\mu_e[\text{seconds}]$},
	scatter/classes={%
		a={mark=*,draw=red}}]
	\addplot[scatter,only marks,%
	scatter src=explicit symbolic]%
	table[meta=label] {
		x y label
		28 270 a
		38 332 a
		56 727 a
		278 1621 a
	};
	\end{axis}
	\end{tikzpicture}
	\centering
	
	\caption{On the left the standard deviation $\sigma_e$ of the edge $e$ is compared to the validation loss of the first baseline on that edge. On the right a comparison between mean running time $\mu_e$ and the standard deviation $\sigma_e$ is displayed.}
	\label{fig:distance_vs_val_error}	
\end{figure*}

\newpage

Next we provide some statistics about each connection we will be training on individually. \textbf{Zürich Oerlikon - Zürich HB} has more than double the amount of train movement than the other edges. This can be explained by the short travel distance and the proximity to a big city. 

\begin{table}[htp]
	\begin{tabular}{|c|c|c|c|}
		\hline
		Connection & Train movement & Travel distance & Mean running time\\
		&  & [km] & $\mu_e$ [sec] \\
		\hline
		Zürich Oerlikon - Zürich HB & 27'398  & 2.6 & 270\\
		\hline
		Lenzburg - Aarau & 11'395 & 11.2 & 330 \\
		\hline
		Winterthur - Zürich Flughafen & 13'930 & 44.2 & 727 \\
		\hline
		Olten - Bern & 10'150 & 68.6 & 1621 \\
		\hline	
	\end{tabular}
	\caption{Investigate different properties of the individual edges}
	\label{table:general_stats_per_edge}
\end{table}

In the following the mean estimate $\mu_{cv}$ and the variance $\sigma_{cv}$ of the resulting running time predictions of a 5-fold cross-validation are displayed. These values can be understood as the average error in seconds when using this first trivial method. For example for the edge \textbf{Olten - Bern} we receive an average error of 78s for validation and 81s for testing.


\begin{table}[htp]
	\begin{tabular}{|c|c|c|c|}
		\hline
		Connection & $\mu_{cv}$ ($\pm$ $\sigma_{cv}$) & $\mu_{cv}$ ($\pm$ $\sigma_{cv}$) & Objective Loss \\
		& Training & Validation & Test \\
		\hline
		Zürich Oerlikon - Zürich HB &  17.15 ($\pm$ 0.12) & 17.16 ($\pm$ 0.26) & 17.02\\
		\hline
		Lenzburg - Aarau & 20.81 ($\pm$ 0.13) & 20.83 ($\pm$ 0.84) & 20.62 \\
		\hline
		Winterthur - Zürich Flughafen & 39.03 ($\pm$ 0.35) & 38.91 ($\pm$ 1.67) & 37.19 \\
		\hline
		Olten - Bern & 78.07 ($\pm$ 4.25) & 78.30 ($\pm$ 3.58) & 81.16\\
		\hline	
	\end{tabular}
	\caption{Calculating the losses on all 4 connections when predicting the mean running time per edge as defined in \refsec{subsec:mean_per_edge}. We display the average error and the standard deviation for the 5-fold cross-validation. }
	\label{table:baseline1}
\end{table}

The results of this first experiment suggest that the distribution of running times depends strongly on the corresponding edge. To equalize these different ranges of values we decide to standardize the running times as described in \refsec{subsec:feature_scaling} and train further models on the standardized values. 

\begin{comment}



\begin{table}[htp]
\begin{tabular}{|c|c|c|c|}
\hline
Connection & MSE ($\pm$ $\sigma$) & MSE ($\pm$ $\sigma$) & MSE \\
& Training & Validation & Test \\
\hline
Zürich Oerlikon - Zürich HB &  0.98 ($\pm$ 0.004) & 0.98 ($\pm$ 0.03) & 0.97\\
\hline
Lenzburg - Aarau & 0.85 ($\pm$ 0.007) & 0.84 ($\pm$ 0.02) & 0.83 \\
\hline
Winterthur - Zürich Flughafen & 0.9 ($\pm$ 0.04) & 0.89 ($\pm$ 0.09) & 0.9 \\
\hline
Olten - Bern & 0.62 ($\pm$ 0.03) & 0.64 ($\pm$ 0.12) & 0.75\\
\hline	
\end{tabular}
\caption{The mean squared error of the first baseline with standardized outputs.}
\label{table:baseline1_MSE_standardized}
\end{table}
\end{comment}
\newpage
\section{Random Forest}

\textbf{Input Data:} We use the vehicle-specific input as defined in \reftable{table:vehicle_information}. 

\textbf{Idea:} Our aim is to beat the first baseline using the vehicle-specific input only. For this we utilize the Random Forest Regressor  implementation of Scikit-learn \cite{scikit-learn} and perform 5-fold cross-validation. The amount of decision trees is set to 100 and we train the model on the standardized outputs. We use the variance reduction for optimization.

\textbf{Results:} We find that restricting the maximum depth of the decision trees improves the validation error significantly. In \reffig{fig:e2_RF_estimators} we show the effect of choosing a maximum depth of [5,10,15,20]. The model overfits to the training data if this parameter is set to more than 5. It is very likely that this problem is caused by the low amount of training data and could be prevented with more data for a single edge or with a more sophisticated feature extraction.

\begin{figure*}[htp]
	\centering
	\includegraphics[width=0.9\linewidth]{images/e2_RF_estimators.png}					
	
	\caption{Performing 5-fold cross-validation on 4 connections in the graph individually. A comparison of the resulting sum of losses when restricting the maximum depth of the decision trees is displayed.}
	\label{fig:e2_RF_estimators}
\end{figure*}

By reverting the standardization the loss of the objective function is calculated. The result of the best model found are shown in \reftable{table:RF_MAE}. 

\begin{table}[htp]
	\begin{tabular}{|c|c|c|c|}
		\hline
		Connection & $\mu_{cv}$ ($\pm$ $\sigma_{cv}$) & $\mu_{cv}$ ($\pm$ $\sigma_{cv}$) & Objective Loss \\
		& Training & Validation & Test \\
		\hline
		Zürich Oerlikon - Zürich HB &  10.02 ($\pm$ 0.03) & 10.07 ($\pm$ 0.14) & 10.51\\
		\hline
		Lenzburg - Aarau & 19.57 ($\pm$ 0.32) & 19.80 ($\pm$ 0.50) & 19.71 \\
		\hline
		Winterthur - Zürich Flughafen & 34.31 ($\pm$ 0.11) & 34.87 ($\pm$ 1.63) & 34.53 \\
		\hline
		Olten - Bern & 59.12 ($\pm$ 1.24) & 59.73 ($\pm$ 1.35) & 61.11\\
		\hline	
	\end{tabular}
	\caption{The average error $\mu_{cv}$ and the variance $\sigma_{cv}$ of the 5-fold cross-validation are displayed.}
	\label{table:RF_MAE}
\end{table}

Next we investigate \textit{why} the Random Forest Regressor overfits if we do not restrict the maximum depth of the trees. We inspect the \textit{feature importance} which estimates how much the trained model relies on each feature. In \reffig{fig:e3_feature_importance} we show and compare the resulting feature importance for all individual edges when choosing a maximal depth of 5 and 20. As categorical features are one-hot encoded, their feature importance is calculated by summing over all dimensions that correspond to that feature. We find that some features such as the \textit{weekday} and \textit{departure delay} have less feature importance in the case of smaller trees. This suggests that these features contribute to the overfitting. We can also see that the feature importance varies significantly between different edges. For example in the case of $\textbf{Zürich HB - Zürich Oerlikon}$ and $\textbf{Olten - Bern}$ the running time as given by the schedule has a high feature importance compared to the other two edges. 

\begin{figure*}[htp]
	\centering
	\includegraphics[width=0.49\linewidth]{images/e3_feature_importance_5.png}					
	\includegraphics[width=0.49\linewidth]{images/e3_feature_importance_20.png}			
	
	\caption{The feature importance of the Random Forest Regressor is displayed for a maximal depth of 5 on the left and 20 on the right. The features comprise \textit{type}, \textit{weekday}, \textit{hour}, \textit{departure delay} and \textit{running time (schedule)} of the vehicle.}
	\label{fig:e3_feature_importance}
\end{figure*}

\newpage
\section{Feedforward Neural Network}

$\textbf{Input Data: }$ Both, the vehicle-specific input $\mathbf{u}$ and the standardized graph weights  $\mathbf{w} = [\mathcal{W}(e_1)', \mathcal{W}(e_2)', \dots, \mathcal{W}(e_{1459})']^T$ are used. We combine the two features by concatenation [$\mathbf{u}$, $\mathbf{w}$].

$\textbf{Idea:}$ We investigate how well a Feedforward Neural Network works with all data concatenated to a single feature vector. For this we use the library Keras \cite{chollet2015keras} and build a model with 3 consecutive layers with 2048 neurons each. The activation function is set to the hyperbolic tangent $f(x) = \tanh(x)$ for all layers. The output of the model consists of a single neuron with no activation. The model is trained on the standardized running times \refsec{subsec:feature_scaling}, we train the model for 20 epochs, the batch size is 1, the learning rate is $10^{-5}$,  and we use the mean squared error for the optimization loss.

$\textbf{Results: }$ In \reffig{fig:e4_ffnn} we show the resulting performance of this first attempt. Similar to the Random Forest Regressor we identify a problem of overfitting to the training data. Unfortunately as neural networks are hard to interpret, it is difficult to say what caused this problem and how to handle it.

\begin{figure*}[htp]
	\centering
	\includegraphics[width=0.80\linewidth]{images/e4_ffnn.png}					
	
	\caption{Training and validation error for a 3-layer Feedforward Neural Network compared to the baselines.}
	\label{fig:e4_ffnn}
\end{figure*}

To improve the generalization error we try to add a dropout layer directly before the output neuron. The dropout rate is set to 50\% and we investigate the performance of all 20 epochs in \reffig{fig:e4_ffnn_2}. We can see that the training and validation losses are now closer. It is interesting that compared to \reffig{fig:e4_ffnn} for all edges the validation error now doesn't seem to increase. But still the model didn't benefit from this change. In case of $\textbf{Zürich HB - Zürich Oerlikon}$ the model even lost its ability to overfit to the training data.  

\begin{figure*}[htp]
	\centering
	\includegraphics[width=1.0\linewidth]{images/e4_ffnn_2.png}					
	
	\caption{Training and validation error for a Feedforward Neural Network with dropout.}
	\label{fig:e4_ffnn_2}
\end{figure*}



\newpage

\section{Graph net}

\textbf{Input Data:} We define an input graph $G = (\mathbf{u}, V,E)$ that can be used directly by Graph net. For this we use the graph of historical train movement \refsec{subsubsec:graph_features} for the vertices $V$ and the edges $E$ and we set the global attribute $\mathbf{u}$ to our vehicle-specific features \reftable{table:vehicle_information}. More specifically the node attributes $\mathbf{v}_i$ of $V$ are empty and the edge attributes $\mathbf{e}_k$ of $E$ contain the corresponding edge weights $\mathcal{W}(e_k)$ of the graph of historical train movement. 

$$ \mathbf{v}_i\ = \{\}, \quad\mathbf{e}_k = \{\mathcal{W}(e_k)\}$$

\textbf{Idea:} We want to see if a model that works directly on the structure of the graph can lead to better results than the baseline. Due to their flexibility and novelty we consider Graph nets. The open-source implementation of Battaglia et al. \cite{pbattaglia} is used and 5-fold cross-validation is performed with various different model configuration. 

For a Graph net block (GN block) we have to define each update function $\phi$ and each aggregation function $\rho$. To start, we use the same functions as provided in the examples of the implementation and that are discussed in Section 4.2 (page 15) of the paper \cite{Battaglia2018RelationalIB}.

\begin{align*} 
\phi^e(\mathbf{e}_k, \mathbf{v}_{r_k}, \mathbf{v}_{s_k}, \mathbf{u}) &:= NN_e ([\mathbf{e}_k, \mathbf{v}_{r_k}, \mathbf{v}_{s_k}, \mathbf{u}])  \\
\phi^v(\bar{\mathbf{e}}'_i, \mathbf{v}_i, \mathbf{u}) &:= NN_v ([\bar{\mathbf{e}}'_i, \mathbf{v}_i, \mathbf{u}]) \\
\phi^u (\bar{\mathbf{e}}', \mathbf{v}', \mathbf{u}) &:= NN_u ([\bar{\mathbf{e}}', \mathbf{v}', \mathbf{u}]) \\
\rho^{e \rightarrow v}(E'_i)  &:= \sum_{\{k:r_k = i\}} \mathbf{e}_k'\\
\rho^{v \rightarrow u}(V') &:= \sum_i \mathbf{v}_i' \\
\rho^{e \rightarrow u}(E') &:= \sum_k\mathbf{e}_k'
\end{align*}

Here $[\mathbf{x},\mathbf{y},\mathbf{z}]$ indicate vector concatenation and $NN_e, NN_v, NN_u$ are multilayer perceptrons (MLP).

As described in Section 4.3 (page 19) \cite{Battaglia2018RelationalIB} "[...] the information that a node
has access to after m steps of propagation is determined by the set of nodes and edges that are at
most m hops away". This means we should choose the number of GN blocks according to how far we want the information to propagate through the graph. Because we expect the most important information to be in close proximity of each edge and node we set the number of GN blocks to 1. The size of the resulting global output $\mathbf{u}'$ is reduced by an additional neural layer with a single output neuron without an activation.

\newpage

\textbf{Results:} We set the size of $NN_e, NN_v, NN_u$ to 2 layers with 16 neurons each. This equals the size of the networks used in the example code of the implementation.

The models are trained for 5 epochs, the learning rate is set to $10^{-5}$, and the batch size is 1. The models are trained on the standardized running times and we use the mean squared error for the optimization loss. We perform 5-fold cross-validation, and show the resulting objective losses in \reffig{fig:e6_graph_nets}. We can see that the training loss is close to the validation loss and both decrease similarly. This is a good sign as it indicates generalization of the model. Since the training loss is not decreasing as fast as the feedforward neural network experiment we decide to $\mathbf{increase}$ the size of the neural networks $NN_e, NN_v, NN_u$ in a next experiment.
\begin{figure*}[htp]
	\centering
	\includegraphics[width=1.0\linewidth]{images/e6_graph_nets.png}					
	
	\caption{5-fold cross-validation for graph nets with small $NN_e, NN_v, NN_u$ using 2 layers with 16 neurons each and summation for aggregation.}
	\label{fig:e6_graph_nets}
\end{figure*}
\newpage
For the next experiment the size of $NN_e, NN_v, NN_u$ is increased to 2 layers with $\mathbf{128}$ neurons each. We keep all other parameters the same and show the resulting losses in \reffig{fig:e6_graph_nets_sum}. We receive very different results for each connection. In the case of \textbf{Zürich HB - Zürich Oerlikon} we get close to the performance of Random Forest. For \textbf{Lenzburg - Aarau} we overfitted significantly and the validation error is even worse than the trivial baseline. In general we were not able to find improvement over the second baseline, and it is unclear if the model made use of the graph information. 

\begin{figure*}[htp]
	\centering
	\includegraphics[width=1.0\linewidth]{images/e6_graph_nets_sum.png}					
	
	\caption{5-fold cross-validation for graph nets with $NN_e, NN_v, NN_u$ using 2 layers with \textbf{128 neurons} each and summation for aggregation.}
	\label{fig:e6_graph_nets_sum}
\end{figure*}

\newpage

An important factor that may prevent the model of using the graph data successfully is that the aggregation function is set to be summation. As we expect the vast majority of the graph not to be important for the prediction, it could lead to the model being forced to combine important information with irrelevant data. Therefore, instead of using summation for the aggregation functions $\rho$ we now investigate if it can help to use the maximum function. This could lead to better performance as the model only propagates the hidden state with the highest values:
\begin{align*}
\rho^{e \rightarrow v}(E'_i)  &:= \max_{\{k:r_k = i\}} \mathbf{e}_k'\\
\rho^{v \rightarrow u}(V') &:= \max_i \mathbf{v}_i' \\
\rho^{e \rightarrow u}(E') &:= \max_k\mathbf{e}_k'
\end{align*}

We use 2-layer neural networks with 128 neurons for $NN_e, NN_v, NN_u$, keep all other parameters the same, and investigate the losses in \reffig{fig:e6_graph_nets_max}. Again, we have completely different results for each connection. As the losses of validation and training in most cases are higher than before, we could not verify our expectation that using the maximum function for aggregation would lead to better performance in the model. The only connection this change seems to have helped is \textbf{Lenzburg - Aarau}, although the behavior of the learning curve is very suspicious as the validation loss seems to be unaffected by the decrease of the training loss.



We conclude that the high flexibility of graph nets makes it equally hard to control them. We performed several unsuccessful tests and therefore one or more of the following are true:

\begin{enumerate}
	\item The way we define the graph input has to be reconsidered. For example a reduced version could lead to better performance. For this we would need prior knowledge about dependencies in the graph and which data is not important.
	\item We need more regularization. This could for example be achieved by adding dropout layers.
	\item The amount of data we use for training is too low. To solve this problem data augmentation could be used.
	\item Graph nets have a hard time if the majority of data is unimportant for the prediction. As graph nets process all nodes and all edges with the same update functions $\phi$ and then use $\rho$ for aggregation it is hard to focus on relevant inputs.
	\item The choice of $\phi$ and $\rho$ has to be more sophisticated. Section 4.2.2 and Section 4.2.3 (page 19) of the paper  \cite{Battaglia2018RelationalIB} discuss more sophisticated aggregation and update functions, and could be a good starting point.
\end{enumerate}
\newpage
\begin{figure*}[htp]
	\centering
	\includegraphics[width=1.0\linewidth]{images/e6_graph_nets_max.png}					
	
	\caption{Graph nets with $NN_e, NN_v, NN_u$ using 2 layers with 128 neurons each and the \textbf{maximum function} for aggregation.}
	\label{fig:e6_graph_nets_max}
\end{figure*}
\newpage
\section{Lasso Regression}
\label{subsec:lasso}

$\textbf{Input Data: }$ Only the standardized graph weights $\mathbf{w} = [\mathcal{W}(e_1)', \mathcal{W}(e_2)', \dots, \mathcal{W}(e_{1459})']^T$.

$\textbf{Idea:}$ We want to check if the graph of historical train movement can be used for train delay prediction and if regularization can lead to better results. Also, we want to investigate how much of the graph is used for the best performance. 

$\textbf{Results:}$ We train and evaluate Lasso using the implementation of Scikit-learn \cite{scikit-learn}. We add a constraint to the optimization to force positive weights to be able to interpret the prediction as a conical combination:
\begin{align*}
\mathbf{w}^* &= \arg \min_{\mathbf{w}} || \mathbf{y} - X \mathbf{w} ||_2^2 + \lambda  || \mathbf{w} ||_1, &\forall i. \mathbf{w}^*_i \geq0 \\
\mathbf{y}^* &= X \mathbf{w}^*,  &\forall i.\mathbf{w}^*_i \geq0 
\end{align*}

The behavior of the objective losses when choosing different regularization strengths $\lambda \in [0.001,0.003,\dots,0.009]$ can be seen in \reffig{fig:lasso_regularization}. 

First let us note that the trivial baseline \textit{Mean per edge} here is the same as setting all the weights to zero: $\forall i.\mathbf{w}_i^*=0$. This is achieved by setting the regularization to a high value. As the validation loss of the results is strictly lower than the trivial baseline, it must therefore be the case that at least one dimension in $\mathbf{w}^*$ is non-zero. We notice that decreasing $\lambda$ too much leads to overfitting to the training set. This indicates that using too many features of the graph can reduce the performance.

Next we investigate which features are used for \textbf{Lenzburg - Aarau} as it seems to be the most interesting case according to the learning curve. For this the vector $\mathbf{w}^*$ is normalized:

$$ \hat{\mathbf{w}}^* = \frac{\mathbf{w}^*}{|\mathbf{w}^*|}$$

We can interpret these values as a distribution over the standardized edge weights $\mathcal{W}(e_i)'$ which depict the importance of a particular edge weight for the prediction. We use the graph view \refsec{subsec:graph_view} to display the two extreme cases of our experiment $\lambda \in [0.001, 0.009]$ in \reffig{fig:e11_lasso113_graph_view}. $\hat{\mathbf{w}}^*$ is displayed by setting the alpha values of the corresponding edges to the distribution weight and we display the edge \textbf{Lenzburg - Aarau} with a green line. With a low regularization $\lambda = 0.001$ we can see now the reason the model is overfitting to the training data. The number of non-zero dimensions in $\hat{\mathbf{w}}^*$ is 206 and some of the corresponding edges are far away from the green line. A more sensible solution was found with setting $\lambda = 0.009$. Now only 25 values in $\hat{\mathbf{w}}^* $ are non-zero and most of the prediction relies on edges that are in close proximity to the green line. This constitutes to roughly 1.7\% of all edges in the graph. 



\begin{figure*}[htp]
	\centering
	\includegraphics[width=1.0\linewidth]{images/e7_LASSO_gamma.png}					
	
	\caption{Lasso Regression with different strength of regularization $\lambda$ are evaluated and compared to the baselines. High regularization strength indicates that a low amount of inputs are considered for the prediction. Contrary a low regularization strength uses more features and therefore is prone to overfitting.}
	\label{fig:lasso_regularization}
\end{figure*}

\begin{figure*}[htp]
	\centering
	\includegraphics[width=1.2\linewidth, trim={2.6cm 6cm 2cm 6cm},clip]{images/e11_lasso113_0001.png}				
	
	\includegraphics[width=1.2\linewidth, trim={2.6cm 6cm 2cm 6cm},clip]{images/e11_lasso113_0009.png}				
	
	\caption{Interpreting the learned weights of Lasso with $\lambda \in [0.001,0.009]$ on the edge \textbf{Lenzburg - Aarau} (green) leads to different amount of features being used depending on the regularization strength $\lambda$.}
	\label{fig:e11_lasso113_graph_view}
\end{figure*}

\newpage
\section{Attention model}

$\textbf{Input Data: }$ Both the vehicle-specific input $\mathbf{u}$ and the standardized graph weights as a vector $\mathbf{w} = [\mathcal{W}(e_1)', \mathcal{W}(e_2)', \dots, \mathcal{W}(e_{1459})']^T$ are used for this experiment.

$\textbf{Idea:}$ Similar to Lasso we want to have a model that is able to focus on certain parts of the graph but also have the ability to use the vehicle-specific information. We therefore develop a neural network architecture that can be seen in \refsec{subsec:attention_model} and evaluate the performance. The attention distribution will be displayed using the graph view in \refsec{subsec:graph_view}. 

$\textbf{Results: }$ The attention model is trained for 20 epochs, optimized with the mean squared error on the standardized running times, and we investigate the objective loss of a 5-fold cross-validation in \reffig{fig:e10_attention_model}. The batch size is set to 1 and the learning rate is $10^{-5}$. The learned attention distribution for all connections at epochs 1, 5 and 20 are displayed in \reffig{fig:e11_softmax_trainnet_7}, \reffig{fig:e11_softmax_trainnet_113}, \reffig{fig:e11_softmax_trainnet_35} and \reffig{fig:e11_softmax_trainnet_142}.
\begin{figure*}[htp]
	\centering
	\includegraphics[width=0.85\linewidth]{images/e10_attention_module.png}					
	
	\caption{Observing the validation and training loss of the attention model for 20 epochs.}
	\label{fig:e10_attention_model}
\end{figure*}

Although the performance of the model for $\textbf{Zürich HB - Zürich Oerlikon}$ didn't improve over the second baseline the attention mechanism focuses onto a single edge adjacent to it. This indicates that there is a dependency of these two edges. In the case of $\textbf{Lenzburg - Aarau}$ we can clearly see a worse performance of the model compared to the second baseline. In fact, by observing the graph view we can see that the model didn't learn a sensible attention distribution. One possible explanation for this finding could be that this connection simply does not have dependencies in the graph.

The results for the edge $\textbf{Winterthur - Zürich Flughafen}$ on the other hand look very promising. The small difference in validation and training loss indicate that the models for this connection are generalizing well. The attention distribution already learns to focus on a single edge in close proximity in epoch 5 and spreads to multiple edges in epoch 20. By looking at the corresponding sum of losses in \reffig{fig:e10_attention_model} we identify epoch 5 to be the starting point for improvement over the second baseline. Therefore this spreading mechanism could be attributed the good performance in this case. The improvement for the edge \textbf{Olten - Bern} compared to Random Forest Regression is 17.2\% for validation and 19.6\% for testing, see \reftable{table:final_result}. Observing the attention distribution reveals that not only edges with close proximity have a high attention, but also edges with a similar travel distance. 

In general, we observe that there seems to be a connection on how many edges the model focuses its attention on depending on the travel distance. One possible explanation for this finding is that long travel distances tend to be more effected by regional influences such as weather and therefore the model spreads the attention on multiple edges. 


\begin{figure*}[htp]
	\centering
	\centering{$\huge\textbf{Zürich HB - Zürich Oerlikon}$}
	\includegraphics[width=1.0\linewidth, trim={2.6cm 6cm 2cm 6cm},clip]{images/e11_softmax_trainnet_7_00.png}				
	
	\includegraphics[width=1.0\linewidth, trim={2.6cm 6cm 2cm 6cm},clip]{images/e11_softmax_trainnet_7_04.png}				
	
	\includegraphics[width=1.0\linewidth, trim={2.6cm 6cm 2cm 6cm},clip]{images/e11_softmax_trainnet_7_19.png}				
	
	\caption{Observing the attention distribution of \textbf{Zürich HB - Zürich Oerlikon} at 1,5 and 20 epochs of training.}
	\label{fig:e11_softmax_trainnet_7}
\end{figure*}

\begin{figure*}[htp]
	\centering
	\centering{$\huge\textbf{Lenzburg - Aarau}$}
	\includegraphics[width=1.0\linewidth, trim={2.6cm 6cm 2cm 6cm},clip]{images/e11_softmax_trainnet_113_00.png}				
	
	\includegraphics[width=1.0\linewidth, trim={2.6cm 6cm 2cm 6cm},clip]{images/e11_softmax_trainnet_113_04.png}				
	
	\includegraphics[width=1.0\linewidth, trim={2.6cm 6cm 2cm 6cm},clip]{images/e11_softmax_trainnet_113_19.png}				
	
	\caption{Observing the attention distribution of \textbf{Lenzburg - Aarau} at 1, 5 and 20 epochs of training.}
	\label{fig:e11_softmax_trainnet_113}
\end{figure*}

\begin{figure*}[htp]
	\centering
	\centering{$\huge\textbf{Winterthur - Zürich Flughafen}$}
	\includegraphics[width=1.0\linewidth, trim={2.6cm 6cm 2cm 6cm},clip]{images/e11_softmax_trainnet_35_00.png}				
	
	\includegraphics[width=1.0\linewidth, trim={2.6cm 6cm 2cm 6cm},clip]{images/e11_softmax_trainnet_35_04.png}				
	
	\includegraphics[width=1.0\linewidth, trim={2.6cm 6cm 2cm 6cm},clip]{images/e11_softmax_trainnet_35_19.png}				
	
	\caption{Observing the attention distribution of \textbf{Winterthur - Zürich Flughafen} at 1, 5 and 20 epochs of training.}
	\label{fig:e11_softmax_trainnet_35}
\end{figure*}

\begin{figure*}[htp]
	\centering
	\centering{$\huge\textbf{Olten - Bern}$}
	\includegraphics[width=1.0\linewidth, trim={2.6cm 6cm 2cm 6cm},clip]{images/e11_softmax_trainnet_142_00.png}				
	
	\includegraphics[width=1.0\linewidth, trim={2.6cm 6cm 2cm 6cm},clip]{images/e11_softmax_trainnet_142_04.png}				
	
	\includegraphics[width=1.0\linewidth, trim={2.6cm 6cm 2cm 6cm},clip]{images/e11_softmax_trainnet_142_19.png}				
	
	\caption{Observing the attention distribution of \textbf{Olten - Bern} at 1, 5 and 20 epochs of training.}
	\label{fig:e11_softmax_trainnet_142}
\end{figure*}

\begin{table}[htp]
	
	
	\makebox[\linewidth]{
		\begin{tabular}{|c|c|c|c|c|}
			\hline
			Connection & Model & $\mu_{cv}$ ($\pm$ $\sigma_{cv}$) & $\mu_{cv}$ ($\pm$ $\sigma_{cv}$) & Test\\
			& 	& Training & Validation & Error \\
			\hline
			\multirow{3}{*}{\mr{Zürich Oerlikon - Zürich HB}{(2.6 km)}} 
			& Mean Per Edge & 17.15 ($\pm$ 0.12) & 17.16 ($\pm$ 0.26) & 17.02\\
			& Random Forest &  $\mathbf{10.02}$ ($\pm$ 0.03) & $\mathbf{10.07}$ ($\pm$ 0.14) & $\mathbf{10.51}$\\
			& Attention Model & $\mathbf{10.02}$ ($\pm$ 0.12) & $\mathbf{10.11}$ ($\pm$ 0.14) & $\mathbf{9.95}$\\
			\hline
			%\addlinespace[1ex]
			%\hline
			\multirow{3}{*}{\mr{Lenzburg - Aarau}{(11.2 km)}}
			& Mean Per Edge & 20.81 ($\pm$ 0.13) & 20.83 ($\pm$ 0.84) & 20.62 \\
			& Random Forest & $\mathbf{19.57}$ ($\pm$ 0.32) & $\mathbf{19.80}$ ($\pm$ 0.50) & $\mathbf{19.71}$ \\
			& Attention Model & $\mathbf{19.67}$ ($\pm$ 0.17) & $\mathbf{19.97}$ ($\pm$ 0.78) & $\mathbf{19.46}$\\
			\hline
			%\addlinespace[1ex]
			%\hline
			\multirow{3}{*}{\mr{Winterthur - Zürich Flughafen}{(44.2 km)}} 
			& Mean Per Edge & 39.03 ($\pm$ 0.35) & 38.91 ($\pm$ 1.67) & 37.19 \\
			& Random Forest & 34.31 ($\pm$ 0.11) & 34.87 ($\pm$ 1.63) & 34.53 \\
			& Attention Model & $\mathbf{30.65}$ ($\pm$ 0.26) & $\mathbf{31.30}$ ($\pm$ 0.98) & $\mathbf{30.53}$\\
			\hline
			
			\multirow{3}{*}{\mr{Olten - Bern}{(68.6 km)}} 
			& Mean Per Edge & 78.07 ($\pm$ 4.25) & 78.30 ($\pm$ 3.58) & 81.16\\
			& Random Forest & 59.12 ($\pm$ 1.24) & 59.73 ($\pm$ 1.35) & 61.11\\
			& Attention Model & $\mathbf{49.87}$ ($\pm$ 1.03) & $\mathbf{50.03}$ ($\pm$ 3.20) & $\mathbf{49.1}$\\
			\hline
			
			%\addlinespace[1ex]
			%\hline
			%\multirow{9}{*}{\mr{Bern - Olten}{(69.4 km)}} & Ridge & 133'884.57 $\pm$ 129'757.05            & 176'755.71     \\
			%& GraphNet & 120'072.72 $\pm$ 135'723.84            & 151'312.12     \\
			%& Lasso & 102'489.11 $\pm$ 133'109.61               & 150'437.49     \\
			%& Baseline 2 & 92'742.64 $\pm$ 136'739.47               & 148'912.21     \\
			%& Gaussian Process & 80'066.13 $\pm$ 100'980.06               & 132'361.71     \\
			%& TrainNet (t.-agnostic) & 91'107.55 $\pm$ 135'125.05                        & 130'269.56 \\
			%& TrainNet (t.-aware) &  $\pm$ 136'734.36                        & 109'489.43 \\
			%& Baseline 1 & 93'263.78 $\pm$ 137'846.01               & 107'521.96     \\
			%& Random Forest & 94593.97 $\pm$ 137'588.88            & 106'705.41     \\
			
			%\hline
		\end{tabular}
	}
	
	\caption{The attention model is an improvement for long travel distances. }
	\label{table:final_result}
\end{table}


%\section{Training on multiple edges}

%\TODO{train on all data and evaluate on the 4 edges}
