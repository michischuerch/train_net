\chapter{Methodology}

In this chapter we describe the problem in more detail and present an attention-based model for predicting train delays. Additionally we depict the dataset and the pre-processing we use. 

\section{Problem formulation}
\label{sec:problem_formulation}

The goal of this thesis is to build a model that predicts train delays on the Swiss public transport system as accurately as possible and to make use of historical train movements. The task can be formulated in the following way:%Not only does this give better possibilities for dispatching but also the results can be used to improve future scheduling and customer information.

\fbox{\begin{minipage}{34em}
\centering Given all information that can be extracted from the dataset, \\ find a model that predicts the $\textbf{arrival delay}$ as accurately as possible.
\end{minipage}}

Instead of predicting the arrival delay directly we use the $\textbf{running time}$ between the stations as targets. This is a choice of preference and we can always calculate the predicted arrival time by involving the schedule and the actual departure time. 

As an objective function for measuring the performance of our model we use the mean absolute error (MAE) between the observed targets $\mathbf{y}$ and the predicted targets $\hat{\mathbf{y}}$:

$$ \mathbf{MAE}(\mathbf{y}, \hat{\mathbf{y}}) = \frac{1}{n}\sum_{i=1}^n |\mathbf{y}_i-\hat{\mathbf{y}}_i|$$

An advantage of using this objective function is that we can interpret the results as the average error of the prediction in seconds. 

\begin{comment}

\begin{figure*}[htp]
	\centering
	\includegraphics[width=1.0\linewidth]{images/running_time.png}
	
	\caption{A depiction of the targets we use. }
	\label{fig:running time}
\end{figure*}
\end{comment}

\newpage
\section{Dataset}

\begin{figure*}[htp]
	\centering
	\includegraphics[width=0.8\linewidth]{images/data_gathering.png}
	
	\caption{Several sensors at the stations gather data of the trains passing by. }
	\label{fig:data_gathering}
\end{figure*}

The dataset we use is provided by the Swiss Federal Railways (SBB) and it is an aggregation of all reported motion of the public transport system since January 2018. The data originates from sensors which are located at traffic stations \reffig{fig:data_gathering} and which record a new data sample whenever a train arrives and departs at a station. The dataset is available on the open platform of SBB \cite{SBB:open_data}.   \newline
An entry in the resulting dataset encompasses 21 fields of which we show the four most important ones in \reffig{fig:data_table}. Whenever a vehicle stops and starts at a station we are provided with an identifier for the vehicle, the name of the station and the respective timestamps of arrival and departure. An exception are final stops in which case we receive only the final arrival time. Similarly in the case of first departures we are only provided with the departure time. Other fields encompass the type of the train, the text that is displayed on the outside of the vehicle, the arrival and departure times according to the schedule, and the organization the train belongs to. The remaining 12 fields either contain duplicate entries in another format or items that are used for internal systems of SBB and therefore are not useful for us. The data is split into buckets where each bucket has size 150-200MB and represents a day of the year. The cut between the files is around 3 o'clock in the morning. We will use this natural separation to be able to process the data in parallel. 
%Unfortunately we do not have access to more details about the train like the weight of the train or the number of passengers. 

%With a resolution of seconds the sensor data gives us for examples two timestamps for both the arrival and departure of individual vehicles at the stations. 

% We will refer to this as the \textbf{actual arrival/departure time}.


\begin{figure*}[htp]

	\centering
	\includegraphics[width=0.7\linewidth]{images/Dataset_fields.png}
	
	\caption{Sample entries of the dataset}
	\label{fig:data_table}
\end{figure*}

\newpage

\subsection{Validation and test split}

For training, validation, and testing we use 181 days of data from 01.01.2018 - 31.06.2018. We split these six months in a first step by randomly selecting 27 of the days for testing. This constitutes to 15\% of all data. On the remaining 154 days we perform K-fold cross-validation. For this the dataset is split into K subsets with an equal amount of days and K different models are trained using the holdout method. Each time, one of the K subsets is used for validation and the other K-1 buckets are combined as a training set. This results in K error estimates for which the mean estimate $\mu_{cv}$ and variance $\sigma_{cv}$ is calculated and displayed according to \reffig{fig:cv_explain}. $\mu_{cv}$ will be used to tune parameters of the model. In the end the best performing model is trained on the whole training set and the test error is calculated.

As an example $\mu_{cv}$ and $\sigma_{cv}$ of \reffig{fig:cv_explain} could be received by training 5 models that result in the following losses:

\[ error_1 = 0.5,\quad
error_2 = 0.5,\quad
error_3 = 1,\quad
error_4 = 1.5,\quad
error_5 = 1.5
\] 

$$ \mu_{cv} = \frac{1}{5} \sum_{i=1}^5 error_i = 1.0\quad \sigma_{cv} =  \sqrt{\frac{1}{5} \sum_{i=1}^5 (error_i - \mu_{cv})^2} = 0.5$$ 

\begin{figure*}[htp]
	
	\centering
	\includegraphics[width=0.7\linewidth]{images/cv_explain.png}
	
	\caption{Cross-validation is displayed by the variance $\sigma_{cv}$ and average $\mu_{cv}$.}
	\label{fig:cv_explain}
\end{figure*}

\newpage

\subsection{Pre-processing}

\begin{comment}

\begin{figure*}[htp]

\centering
\includegraphics[width=0.90\linewidth]{images/preprocessing_flow_chart.png}

\caption{The preprocessing that is used in order to extract the features for our models. }
\label{fig:preprocessing_flow_chart}
\end{figure*}

\end{comment}

%In our preprocessing we first follow each train and rebuild the path it takes through the network. In a second step we remove all unimportant or missing data and finally we extract the history information $\mathcal{H}$. 

In this section we describe how the data is processed before we start training. In a first step we apply simplifications that reduce the dataset to passenger traffic. In a next step two different inputs and the targets are extracted. The first input consists of features concerning the train itself. For the second input we build a graph that captures the current state of the SBB network by using historical train movement.

\subsubsection{Simplifications}
\label{subsubsec:simplifications}
We consider only data that report "SBB" as the owner. This action removes tram or bus data and keeps all train movement. 

We ignore freight trains. They usually are given a low priority for driving on the network, have a very irregular behavior and there is not much value in predicting their movement. According to \cite{wiki:Zugnummer} freight trains have a train number of 40'000 or bigger and we simply remove all data that falls into this category. This action reduces the dataset to have passenger traffic only.

\subsubsection{Extract targets}
\label{subsubsec:target}
Since the dataset reports timestamps of travel data at the same station, but we are interested in predicting running times $\textbf{between}$ the stations we need to rebuild the path a train travels through the network. For this purpose let us consider a train $x$ and its currently reported station $S^{x}_i$. For calculating the running time we require information that can be found at the next station $S^{x}_{i+1}$.

An efficient way of accomplishing this task is to sort the data by the train identifier and the actual arrival time. For $n$ data samples this action needs time $\mathcal{O}(n\log{}n)$ and connects all entries of the same train in a sequential manner. The next station can be found by looking at the following entry. The only property we have to check is that the train identifier of the current entry matches with the train identifier of the next entry. With this action we are now able to identify the $\textbf{running time}$ for the current station to the next station. 

\subsubsection{Extract vehicle features}
\label{subsubsec:vehicle_features}
The first input vector that we build contains 5 features concerning information about the vehicle itself. We extract the weekday of departure and the train type as categorical features, and we calculate the departure delay by subtracting the actual departure time from the schedule departure time. Additionally we use the hour of departure and the running time that is given by the schedule as numerical features. Categorical features are one-hot encoded, see \refsec{subsec:one_hot_encoding}. As there are 22 different train types in total and 7 weekdays this results in a input vector of 32 dimensions.

\begin{table}
\begin{tabular}{c|c|c|c}
	\hline
	Variable name & type & example entry & extracted feature \\
	\hline
	Train type & cat. &  "IR" & class "IR"                    \\
	Weekday of departure & cat. & "Saturday" & class "Saturday"                        \\
	Hour of departure & cat. & 05.08.2018  17:15:31 & class 17                   \\
	Departure delay & num.  & 0:02:15 & 135           \\
	Running time (schedule) & num.  & 0:13:00 & 780           \\
	
	
\end{tabular}
\caption{The vehicle-specific input.}
\label{table:vehicle_information}	
\end{table}
The example for this table would correspond to a vehicle of type "IR" that travels on a Saturday at 17 o'clock with a departure delay of 135 seconds. The type "IR" signifies that the train travels between two big cities and has a higher speed. According to the schedule the planned travel time is 780 seconds.

\begin{comment}
	For an extracted data sample $x \in \mathcal{D}$ from our dataset we will refer to these values in the following way:
	
	\[
	\begin{aligned} 
	\textsc{eid} ~ &  \hat{=} \text{ Edge identifier}\\ 
	x_{type} ~& \hat{=} \text{ Train type} \\
	x_{wd} ~& \hat{=} \text{ Weekday of departure}\\
	x_{hr}  ~ &  \hat{=} \text{ Hour of departure}\\
	x_{dep delay} ~&  \hat{=} \text{ Departure delay}\end{aligned}\\
	\]
	
\end{comment}

\subsubsection{Extract graph features}

\label{subsubsec:graph_features}

To include historical train movement we build a weighted directed graph $\mathcal{G} = (V,E) $ where the set of vertices $V$ represent the stations and the set of edges $E$ represent connections between the stations that we find in the whole dataset. For this purpose let the dataset $\mathcal{D} = \{x_1,x_2, \dots, x_n\}$ consist of n train movements and let us assume we want to predict the running time for a sample $x_p \in \mathcal{D}$.

In the following we denote the station of departure for a sample $x_i$ as $A_{x_i}$, the arrival station as $B_{x_i}$ and the running time as $y_{x_i}$. Additionally we will refer to the corresponding edge that $x_i$ is traveling as $e_{x_i}$ and the  reported time points of departure as $t'_{A,x_i}$ and arrival as $t'_{B,x_i}$.

We define the \textbf{historical train movement} (HTM) for the sample $x_p$ as all train movement in the dataset $\mathcal{D}$ that arrived at a station before the departure of $x_p$:

\[
\textsc{HTM}(x_p) = \{x_1, x_2, \dots, x_k\},\quad\quad \forall i \in [1,k]: 
x_i \in \mathcal{D}, 
~t'_{B,x_i}< t'_{A,x_p}
\]

and similarly the historical train movement for the sample $x_p$ on a single edge $e$ in the graph:

\[
\textsc{HTM}_e(x_p) = \{x_1,x_2, \dots, x_q\} \quad\quad \forall i \in [1,q]: x_i \in HTM(x_p), e_{x_i} = e
\]

As these samples have an arrival time that is smaller than the departure time of $x_p$ we can assume their availability for the prediction of $y_{x_p}$. We will now use the HTM of $x_p$ to define weights in the graph that correspond to the most recent running times of all historical train movement in $HTM(x_p)$. For this purpose let $\bar{x}_{e,x_p} \in HTM(x_p)$ be the most recent sample of train movement on the edge $e$:

$$\bar{x}_{e,x_p} = \smash{\displaystyle \argmax_{x \in HTM_e(x_p)  } } t'_{B,x} $$

Using this definition we set the weights of the graph to the running time of the most recent sample of train movement on the respective edges. For the case that we cannot find any historical train movement for edge $e$ given $x_p$ we set the weight to the mean running time that is calculated by averaging all running times in the training set that occur on the edge $e_x$.

\[
\mathcal{W}(e)=
\begin{cases}
y_{\bar{x}_{e,x_p}}& |HTM_e(x_p)| > 0\\
\mu_e             & \text{otherwise}
\end{cases}
\]

\[
\mu_e = \frac{1}{|D_{train}|}\sum_{\substack{x\in D_{train} \\ e_x = e}} y_x
\]

This results in a weighted graph similar to \reffig{fig:graph_with_running_time} but with 693 vertices and 1459 edges. An important aspect of this graph is that it captures only the last train movement for each connection. We therefore assume that the running time of $x_p$ can be explained by the most recent historical train movement.

\begin{comment}
\[
\mathcal{W}(e;\mathcal{D},\mu,\sigma)= 
\begin{cases}
\frac{\overline{HTM}(e, T_A', \mathcal{D})_y - \mu}{\sigma},& \text{if } |HTM(e,t,\mathcal{D})| > 0\\
0,              & \text{otherwise}
\end{cases}
\]
\end{comment}

\begin{figure*}[htp]
	\centering
	\includegraphics[width=0.65\linewidth]{images/graph_with_running_times.png}
	
	\caption{A simple version of the graph. For a train $x_p$ that travels from $A$ to $B$ we consider the most recent train movement that happened before the departure time of $x_p$. Here the most recent running time for the edge $e=(A,B)$ is 22 seconds.}
	\label{fig:graph_with_running_time}
\end{figure*} 

\newpage

\subsection{Feature scaling}
\label{subsec:feature_scaling}

Most of the inputs and the output have different ranges of values depending on the edge they occur on. To prevent instability problems the data is scaled to have roughly the same range of values. We standardize all edge weights of the input graph, the running time as given by the schedule, and the output targets by subtracting the respective mean running time $\mu_e$ and dividing by the standard deviation $\sigma_e$. To calculate these quantities we use only data from the training set $\mathcal{D}_{train}$. For each running time $y$ that occurred on an edge $e$ we calculate the standardized running time $y'$: 

\[ \mu_e = \frac{1}{|D_{train}|}\sum_{\substack{x\in D_{train} \\ e_x = e}} y_x, \quad\quad\quad \sigma_e = \sqrt{\frac{1}{|D_{train}|}\sum_{\substack{x\in D_{train} \\ e_x = e}} (y_x-\mu_e)^2}\]


\[
y' = \frac{y -\mu_e}{\sigma_e}
\]

Here $e_x$ is the edge the train $x$ is traveling on. In \reffig{fig:histogram_running_times} we show the distribution of all running times of trains when observing the edge $\textbf{Winterthur - Zürich Flughafen}$. The average running time in this case of all 6 months is $\mu_e = 737$ and the standard deviation is $\sigma_e = 62$. 



\begin{figure*}[htp]
	\centering
	\includegraphics[width=0.75\linewidth]{images/historgram_running_time.png}
	
	\caption{A histogram for all train movement that is found in the dataset from 01.01.2018 - 31.06.2018 for a single edge. The mean of all running times is display by a vertical red line and the standard deviation by green arrows. }
	\label{fig:histogram_running_times}
\end{figure*} 

\section{Baseline}

To be able to compare the models we train and evaluate in the experiments in \refsec{sec:experiments}, we use an \textit{easy} and a \textit{hard} baseline. The first baseline constitutes a constant prediction for each edge. We will refer to it as \textbf{Mean per edge}. For the second baseline we use a \textbf{Random Forest Regressor}.



\subsection{Mean per edge}
\label{subsec:mean_per_edge}

We use a first trivial baseline that predicts the mean running time $\mu_e$. For the calculation of $\mu_e$ see \refsec{subsec:feature_scaling}. This method doesn't require the training of a model and in our first experiments we see that it can be understood as an estimate for the variance of the dataset in \refsec{subsec:experiments_mean_per_edge}. In general, trained models should never have a worse performance than this method. A model that has a validation loss worse than this method indicates overfitting to the training data.

\textbf{Comparison: "Copy function"} A trivial baseline that is used internally at SBB is referred to as "copy function". In this method the delay of the arrival station is predicted to be the delay of the departure station. This can be understood as copying the delays from the current to the next station, hence the name. In other words this means that the delay of a train \textit{doesn't change}. Therefore it assumes the running time between the stations to be exactly the running time according to the schedule. 

Now a problem with this approach is that the schedule is a rather poor candidate for prediction because it involves buffer time for the trains to catch up and its values are only given in accuracy of minutes. We therefore prefer \textbf{Mean per Edge} over this baseline.

\subsection{Random Forest Regressor}

To be able to show that our model can learn from past train movement in the network we use an additional baseline that is solely trained on the vehicle features as defined in \reftable{table:vehicle_information}. We use Random Forest Regression because former work applies this method with success on various related tasks \refsec{sec:related_work}. 

\newpage

\section{Neural Network Architectures}

\subsection{Attention mechanism}
\label{subsec:attention_mechanism}
Our experiments with Lasso indicate  that only a small portion of the graph should be used for the prediction \refsec{subsec:lasso}. For a neural network to be able to focus on parts of the graph we use an attention mechanism that can be seen in  \reffig{fig:attention_mechanism}. The main idea is that the vehicle-specific input $\mathbf{c}$ is used as a context to learn an attention distribution $p(\mathcal{W}(e_i)'| \mathbf{c})$ on the standardized weights of our graph $\mathcal{W}(e_i)'$. Here $e_i$ constitute the $i$-th edge of the graph of historical train movement. This attention distribution will then be used to calculate the arithmetic mean of the standardized edge weights:

$$ y = \sum_{i=1}^k p(\mathcal{W}(e_i)'| \mathbf{c}) \mathcal{W}(e_i)' $$ 

\begin{figure*}[htp]
	\centering
	\includegraphics[width=0.6\linewidth]{images/attention_module_abstract.png}
	
	\caption{The attention mechanism uses the vehicle-specific input as the context $\mathbf{c}$ and learns an attention on the standardized graph weights $\mathcal{W}(e_i)'$.}
	\label{fig:attention_mechanism}
\end{figure*}

\newpage
\subsection{Attention model}
\label{subsec:attention_model}

The attention model used in this work extends the attention mechanism \refsec{subsec:attention_mechanism} by an additional neural network. The main idea is that we want to give the model the ability to use both the vehicle-specific input but also the graph of historical train movement to make the prediction. As can be seen in \reffig{fig:attention_model_full} the vehicle-specific input is fed into a 2-layer neural network resulting in the attention layer. This attention layer has 1459 neurons and uses the softmax for activation. This constitutes the attention distribution $p(\mathcal{W}(\mathbf{e})'|\mathbf{c}))$ = $[p(\mathcal{W}(e_1)'|\mathbf{c})),p(\mathcal{W}(e_2)'|\mathbf{c})),\dots, p(\mathcal{W}(e_{1459})'|\mathbf{c}))]^T$. To calculate the weighted average of the attention mechanism we concatenate all standardized running times of the graph $\mathbf{w} = [\mathcal{W}(e_1)', \mathcal{W}(e_2)', \dots, \mathcal{W}(e_{1459})']^T$ and calculate the dot product $\mathbf{w}^T p(\mathcal{W}(\mathbf{e})'|\mathbf{c}))$. Together with the vehicle-specific input this weighted average is then fed into a 2-layer neural network resulting in the final output $y$.

The neural layers are all fully-connected, have 2048 neurons and use $\tanh(x)$ for activation. 


\begin{figure*}[htp]
	\centering
	\includegraphics[width=0.6\linewidth]{images/attention_model_for_train_prediction.png}
	
	\caption{The attention model for train delay prediction uses both the vehicle input and the graph input for prediction. In red the attention mechanism is highlighted.}
	\label{fig:attention_model_full}
\end{figure*}

\newpage
\section{An intuitive view of the graph}
\label{subsec:graph_view}
To be able to interpret the results of our model we extracted all GPS-coordinates of the stations with the help of google-maps and we project them on a Swiss map as can be seen in \reffig{fig:full_graph_view}. For this we use the python library Basemap \cite{jswhit}. 

\begin{figure*}[htp]
	\centering
	\includegraphics[width=0.9\linewidth]{images/full_graph_view.png}
	
	\caption{All stations are displayed by a black dot on the corresponding position on the map and the connections are displayed by red lines. This is the full graph with all edges we encounter in the dataset.}
	\label{fig:full_graph_view}
\end{figure*} 

We will use this view to display the learned attention of our model by setting the alpha value of the edge weights to the corresponding values in the attention distribution. We additionally display the edge the train is traveling on by a green line:

\begin{figure*}[htp]
	\centering
	\includegraphics[width=0.9\linewidth, trim={2.6cm 6cm 2cm 6cm},clip]{images/e11_softmax_trainnet_142_00.png}
	
	\caption{An example of a learned attention after 1 epoch of training. The green line in this case constitutes the connection \textbf{Olten - Bern}.}
	\label{fig:e11_softmax_trainnet_142_00}
\end{figure*} 

