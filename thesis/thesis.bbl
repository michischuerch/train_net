\begin{thebibliography}{10}

\bibitem{pbattaglia}
P.~Battaglia et~al.
\newblock Graph nets.
\newblock \url{https://github.com/deepmind/graph_nets}, 2018.

\bibitem{Battaglia2018RelationalIB}
Peter~W. Battaglia, Jessica~B. Hamrick, Victor Bapst, Alvaro Sanchez-Gonzalez,
  Vin{\'i}cius~Flores Zambaldi, Mateusz Malinowski, Andrea Tacchetti, David
  Raposo, Adam Santoro, Ryan Faulkner, Çaglar G{\"u}lçehre, Francis Song,
  Andrew~J. Ballard, Justin Gilmer, George~E. Dahl, Ashish Vaswani, Kelsey~R.
  Allen, Charles Nash, Victoria Langston, Chris Dyer, Nicolas Heess, Daan
  Wierstra, Pushmeet Kohli, Matthew Botvinick, Oriol Vinyals, Yujia Li, and
  Razvan Pascanu.
\newblock Relational inductive biases, deep learning, and graph networks.
\newblock {\em CoRR}, abs/1806.01261, 2018.

\bibitem{Breiman2001}
Leo Breiman.
\newblock Random forests.
\newblock {\em Machine Learning}, 45(1):5--32, Oct 2001.

\bibitem{Breiman1984ClassificationAR}
Leo Breiman, Joseph~H Friedman, R.~A. Olshen, and C.~J. Stone.
\newblock Classification and regression trees.
\newblock 1984.

\bibitem{cambriahuang2013}
Erik Cambria, G.-B Huang, Liyanaarachchi Kasun, Hongming Zhou, Chi-Man Vong,
  Jiarun Lin, Jianping Yin, Zhiping Cai, Q~Liu, Kuan Li, Victor Leung, L~Feng,
  Yew Ong, Meng-Hiot Lim, Anton Akusok, Amaury Lendasse, Francesco Corona, Rui
  Nian, Yoan Miche, and Jihao Liu.
\newblock Extreme learning machines.
\newblock {\em IEEE Intelligent Systems}, 28:30--59, 11 2013.

\bibitem{Chen2007}
Yaw J. Chien S.~I. Chen, M. and X.~Liu.
\newblock Using automatic passenger counter data in bus arrival time
  prediction.
\newblock {\em J. Adv. Transp.}, 2007.

\bibitem{chollet2015keras}
Fran\c{c}ois Chollet et~al.
\newblock Keras.
\newblock \url{https://github.com/fchollet/keras}, 2015.

\bibitem{least_angle_regression}
Trevor; Johnstone Iain; Tibshirani~Robert. Efron, Bradley;~Hastie.
\newblock Least angle regression.
\newblock {\em Ann. Statist.}, 32(2):407--499, 2004.

\bibitem{jswhit}
Jeff~Whitaker et~al.
\newblock Basemap.
\newblock \url{https://github.com/matplotlib/basemap}, 2018.

\bibitem{gaurav2018}
R.~Gaurav and B.~Srivastava.
\newblock Estimating train delays in a large rail network using a zero shot
  markov model.
\newblock {\em ArXiv e-prints}, 2018.

\bibitem{Gori2005}
M.~Gori, G.~Monfardini, and F.~Scarselli.
\newblock A new model for learning in graph domains.
\newblock In {\em Proceedings. 2005 IEEE International Joint Conference on
  Neural Networks, 2005.}, volume~2, pages 729--734 vol. 2, July 2005.

\bibitem{Gutweniger2018}
Andreas Gutweniger.
\newblock Prognose von verspätungen im öffentlichen verkehr auf basis
  empirischer daten.
\newblock Master's thesis, Berner Fachhochschule, 2018.

\bibitem{Ho_random_forest}
Tin~Kam Ho.
\newblock Random decision forests.
\newblock {\em Proceedings of the 3rd International Conference on Document
  Analysis and Recognition, Montreal}, page 278–282, 1995.

\bibitem{luong2015effective}
Minh-Thang Luong, Hieu Pham, and Christopher~D. Manning.
\newblock Effective approaches to attention-based neural machine translation,
  2015.

\bibitem{SBB_numbers}
Bernhard Meier.
\newblock Sbb facts and figures.
\newblock
  \url{https://reporting.sbb.ch/_file/344/sbb-facts-and-figures-2017.pdf},
  March 2018.

\bibitem{neelakantan2015neural}
Arvind Neelakantan, Quoc~V. Le, and Ilya Sutskever.
\newblock Neural programmer: Inducing latent programs with gradient descent,
  2015.

\bibitem{Oneto2016}
L.~Oneto, E.~Fumeo, G.~Clerico, R.~Canepa, F.~Papa, C.~Dambra, N.~Mazzino, and
  D.~Anguita.
\newblock Advanced analytics for train delay prediction systems by including
  exogenous weather data.
\newblock In {\em 2016 IEEE International Conference on Data Science and
  Advanced Analytics (DSAA)}, 2016.

\bibitem{Oneto2018}
Luca Oneto, Emanuele Fumeo, Giorgio Clerico, Renzo Canepa, Federico Papa, Carlo
  Dambra, Nadia Mazzino, and Davide Anguita.
\newblock Train delay prediction systems: A big data analytics perspective.
\newblock {\em Big Data Research}, 2018.

\bibitem{scikit-learn}
F.~Pedregosa, G.~Varoquaux, A.~Gramfort, V.~Michel, B.~Thirion, O.~Grisel,
  M.~Blondel, P.~Prettenhofer, R.~Weiss, V.~Dubourg, J.~Vanderplas, A.~Passos,
  D.~Cournapeau, M.~Brucher, M.~Perrot, and E.~Duchesnay.
\newblock Scikit-learn: Machine learning in {P}ython.
\newblock {\em Journal of Machine Learning Research}, 12:2825--2830, 2011.

\bibitem{SBB:open_data}
SBB.
\newblock Open transport data.
\newblock \url{https://opentransportdata.swiss/de/dataset/istdaten}, 2018.

\bibitem{tibshirani}
R.~Tibshirani.
\newblock Regression shrinkage and selection via the lasso.
\newblock {\em Journal of the Royal Statistical Society, Series B}, page
  267–288, 1996.

\bibitem{2008coordinate_for_lasso}
Tong {Tong Wu} and Kenneth {Lange}.
\newblock {Coordinate descent algorithms for lasso penalized regression}.
\newblock {\em arXiv e-prints}, page arXiv:0803.3876, March 2008.

\bibitem{wiki:Zugnummer}
Wikipedia.
\newblock Zugnummer --- wikipedia{,} die freie enzyklopädie.
\newblock
  \url{https://de.wikipedia.org/w/index.php?title=Zugnummer&oldid=175190774},
  2018.

\bibitem{Yaghini2013}
Khoshraftar M.~M. Yaghini, M. and M.~Seyedabadi.
\newblock Railway passenger train delay prediction via neural network model.
\newblock {\em J. Adv. Transp}, 2013.

\bibitem{Yu2010}
Yang Z. Chen~K. Yu, B. and B.~Yu.
\newblock Hybrid model for prediction of bus arrival times at next station.
\newblock {\em J. Adv. Transp.}, 2010.

\bibitem{zhang2015structured}
Wei Zhang, Yang Yu, and Bowen Zhou.
\newblock Structured memory for neural turing machines, 2015.

\end{thebibliography}
