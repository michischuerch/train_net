\changetocdepth {2}
\babel@toc {english}{}
\contentsline {chapter}{Contents}{iii}{section*.1}
\contentsline {chapter}{\chapternumberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Thesis Objective}{2}{section.1.1}
\contentsline {section}{\numberline {1.2}Related Work}{3}{section.1.2}
\contentsline {chapter}{\chapternumberline {2}Background}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Linear Least Squares Regression}{5}{section.2.1}
\contentsline {section}{\numberline {2.2}Lasso Regression}{5}{section.2.2}
\contentsline {section}{\numberline {2.3}Random Forest Regression}{6}{section.2.3}
\contentsline {section}{\numberline {2.4}Feedforward Artificial Neural Networks}{7}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Attention models}{7}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}One-hot encoding}{8}{subsection.2.4.2}
\contentsline {section}{\numberline {2.5}Graph nets}{9}{section.2.5}
\contentsline {chapter}{\chapternumberline {3}Methodology}{11}{chapter.3}
\contentsline {section}{\numberline {3.1}Problem formulation}{11}{section.3.1}
\contentsline {section}{\numberline {3.2}Dataset}{12}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Validation and test split}{13}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Pre-processing}{14}{subsection.3.2.2}
\contentsline {subsubsection}{Simplifications}{14}{section*.8}
\contentsline {subsubsection}{Extract targets}{14}{section*.9}
\contentsline {subsubsection}{Extract vehicle features}{14}{section*.10}
\contentsline {subsubsection}{Extract graph features}{15}{section*.12}
\contentsline {subsection}{\numberline {3.2.3}Feature scaling}{17}{subsection.3.2.3}
\contentsline {section}{\numberline {3.3}Baseline}{18}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Mean per edge}{18}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Random Forest Regressor}{18}{subsection.3.3.2}
\contentsline {section}{\numberline {3.4}Neural Network Architectures}{19}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Attention mechanism}{19}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Attention model}{20}{subsection.3.4.2}
\contentsline {section}{\numberline {3.5}An intuitive view of the graph}{21}{section.3.5}
\contentsline {chapter}{\chapternumberline {4}Experiments}{23}{chapter.4}
\contentsline {section}{\numberline {4.1}Mean per edge}{24}{section.4.1}
\contentsline {section}{\numberline {4.2}Random Forest}{26}{section.4.2}
\contentsline {section}{\numberline {4.3}Feedforward Neural Network}{28}{section.4.3}
\contentsline {section}{\numberline {4.4}Graph net}{30}{section.4.4}
\contentsline {section}{\numberline {4.5}Lasso Regression}{35}{section.4.5}
\contentsline {section}{\numberline {4.6}Attention model}{38}{section.4.6}
\contentsline {chapter}{\chapternumberline {5}Conclusion and Outlook}{45}{chapter.5}
\contentsline {chapter}{Bibliography}{47}{chapter*.39}
