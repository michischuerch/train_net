\chapter{Background}

In this chapter we present several different models that we use throughout the thesis to predict delays in the Swiss Railway system. 

\section{Linear Least Squares Regression}
Linear Least Squares Regression is a well-known modeling technique in which a straight line is fit to the data points $X \in \mathbb{R}^{n \times m}$ and ground truth predictions $\mathbf{y} \in \mathbb{R}^n$. For this purpose the squared difference between the predictions $\hat{\mathbf{y}} = X\mathbf{w}$ and the ground truth $\mathbf{y}$ with respect to the models parameters $\mathbf{w}$ is minimized:

$$ \mathbf{w}^* = \arg \min_{\mathbf{w}} || \mathbf{y} - X \mathbf{w} ||_2^2 $$

After calculating the derivatives w.r.t the model parameters $\mathbf{w}$ and setting them to zero the following closed form solution can be obtained:
$$ \mathbf{w}^* = (X^T X )^{-1} X^T \mathbf{y}$$
\section{Lasso Regression}

Lasso (least absolute shrinkage and selection operator)  was discussed in 1996 by Tibshirani \cite{tibshirani}. It has a similar idea as Linear Least Squares Regression but also wants to control the models parameters $\mathbf{w}$ by applying regularization. Here the residual sum of squares is minimized, but also subject to a constraint on the sum of absolute values of the regression coefficients $\mathbf{w}$: 

$$ \mathbf{w}^* = \arg \min_{\mathbf{w}} || \mathbf{y} - X \mathbf{w} ||_2^2 + \lambda  || \mathbf{w} ||_1 $$

The free parameter $\lambda$ controls the sparsity of the solution $\mathbf{w}^*$. Unfortunately, due to the fact that the Lasso penalty is not differentiable there is no closed-form solution for this problem. Optimization methods applied to Lasso include techniques from convex optimization such as coordinate descent \cite{2008coordinate_for_lasso} or least angle regression (LARS) \cite{least_angle_regression}. 



\section{Random Forest Regression}

The method of random decision forest was first discussed by Ho in 1995 \cite{Ho_random_forest} and later refined by Leo Breiman in 2001 \cite{Breiman2001}. It is an ensemble method that grows a multitude of decision trees and then averages over all results.

An example of a decision tree is shown in \reffig{fig:decision_tree}. The main idea is to build a binary tree which subsequently splits the feature space of one of the input variables at each node. The measure based on which the next feature is selected is called impurity. In the case of regression trees usually the $\textit{variance reduction}$ is used. It was introduced by Breiman and Friedman in 1984 \cite{Breiman1984ClassificationAR} and measures for a potential node $N$ the total reduction of the variance w.r.t. the feature it splits. For this let the dataset $\mathcal{D} = \{x_1, x_2, \dots x_n\}$ consist of n samples. A splitting criterion separates the data into subsets for which the criterion is either \textit{true} or \textit{false}. This results in two subsets $\mathcal{D}_{true}\subset \mathcal{D}$ and $\mathcal{D}_{false} \subset \mathcal{D}$. Note that $\mathcal{D}_{true}  \cap \mathcal{D}_{false} = \emptyset$. The variance reduction $I_V$ is defined as:

\begin{align*}
	I_V(N) = &\frac{1}{|\mathcal{D}|^2} \sum_{x_i \in \mathcal{D}} \sum_{x_j\in \mathcal{D}} \frac{1}{2}(x_i - x_j)^2 \\
	&-   \frac{1}{|\mathcal{D}_{true}|^2} \sum_{x_i\in \mathcal{D}_{true}} \sum_{x_j\in \mathcal{D}_{true}} \frac{1}{2}(x_i - x_j)^2 \\
	&-  \frac{1}{|\mathcal{D}_{false}|^2} \sum_{x_i\in \mathcal{D}_{false}} \sum_{x_j\in \mathcal{D}_{false}} \frac{1}{2}(x_i - x_j)^2 
\end{align*} 


\begin{figure*}[htp]
	\centering
	\includegraphics[width=0.6\linewidth]{images/decision_tree.png}
	\caption{An example for a decision tree that predicts the weather. When predicting an instance the model follows the tree from the root and checks the properties on the way to a leaf node.}
	\label{fig:decision_tree}
\end{figure*}

\textbf{Feature importance} A great characteristic of Random Forest is that it provides us with a measure of the relative importance of each feature on the prediction. For this the decrease of the impurity is computed at each node for a tree, summed up for each feature and then normalized. For a forest the features are then ranked according to this measurements by averaging over all trees.

\newpage

\section{Feedforward Artificial Neural Networks}

Inspired by the human brain, neural networks consist of several connected units that perform an operation. In the case of Feedforward Neural Networks the structure of these neurons can be thought of as several layers that receive information from the previous layer, perform some computation and finally produce an output for the next layer. The function that an individual Neuron $j$ in layer $l$ performs is given by
$$ o_j^l = f(b_j^l + \sum_{i=1}^{n} o_i^{l-1}w_{ij}^l)$$
where $o_j^l$ is the output of the neuron $j$ at layer $l$, $f(.)$ is the activation function, $b$ (bias) and $w$ (weights) are parameters that can be learned. For the activation function usually one chooses a non-linear function like Rectified Linear Units (ReLU) $f(x) = max(0,x)$, the hyperbolic tangent $f(x) = \tanh(x)$ or the sigmoid function $f(x) = \sigma(x) = \frac{1}{1+e^{-x}}$. %In this thesis we will be using $f(x) = \tanh(x)$ as the activation function if not specified otherwise.

One method to learn the parameters of the Neural Network is called Stochastic Gradient Descent (SGD). Given a data sample $x$ drawn at random, a loss function that computes the error of the prediction, and the true output, this method uses the first-order derivative of the loss function with respect to each parameter to update the parameters of the network by following the negative direction of the gradient. 

\subsection{Attention models}

Neural Machine Translation \cite{luong2015effective}, Neural Turing Machines \cite{zhang2015structured}, Neural Programmer \cite{neelakantan2015neural} - a few advances in deep learning in the last years that have the common idea of attention. 

In a general setting, an attention model is a method that takes $k$ arguments $\mathbf{z}_1, \dots \mathbf{z}_k \in \mathbb{R}^u$ and a context $\mathbf{c} \in \mathbb{R}^v$. It then produces an output $\mathbf{y} \in \mathbb{R}^u$ which is the weighted arithmetic mean of the $\mathbf{z}_i$, see \reffig{fig:attention_model}:

$$ \mathbf{y} = \sum_{i=1}^k p(\mathbf{z}_i| \mathbf{c}) \mathbf{z}_i $$ 

There are multiple possibilities on how to model the distribution $p(\mathbf{z}_i| \mathbf{c})$ and it depends on the task at hand. As a simple example consider the following: 

\[m_i = \mathbf{w}_c^T \mathbf{c} + \mathbf{w}_z^T \mathbf{z}_i, \quad
p(\mathbf{z}_i| \mathbf{c}) = \frac{\exp(m_i)}{\sum_j \exp(m_j)}\]

where $\mathbf{w}_z \in \mathbb{R}^u$ and $\mathbf{w}_c \in \mathbb{R}^v$ are parameters that can be learned and $\frac{\exp(x_i)}{\sum_j \exp(x_j)}$ is the $\textit{softmax}$ function. 

By design a property of attention models is that the distribution $p(\mathbf{z}_i| \mathbf{c})$ can be used to investigate the importance of $\mathbf{z}_i$. This comes from the fact that if  $p(\mathbf{z}_i|\mathbf{c}) > p(\mathbf{z}_j|\mathbf{c})$ then $\mathbf{z}_i$ is weighted more than $\mathbf{z}_j$ for the output $\mathbf{y}$. 

Usually the attention mechanism is not used on its own, for instance in the case of Neural Machine Translation a common way is to build the attention mechanism into an Encoder-Decoder model to pass information from the encoder hidden states to the decoder. 

\begin{figure*}[htp]
	\centering
	\includegraphics[width=0.38\linewidth]{images/attention_model.png}
	\caption{The idea of the attention mechanism uses a context $c$ to focus on information in the $k$ inputs $\mathbf{z}_1, \mathbf{z}_2, \dots \mathbf{z}_k$ and produces an output $\mathbf{y}$.  }
	\label{fig:attention_model}
\end{figure*}

\subsection{One-hot encoding}
\label{subsec:one_hot_encoding}

One-hot encoding is a way of transforming a categorical variable $x$ with d distinct classes to a d-dimensional binary vector $\mathbf{x}'$:

\[   
\mathbf{x}_i' =
\begin{cases}
1 &\quad\text{if }  i = \text{class(x)}\\ 
0 &\quad\text{else } \\
\end{cases}
\]

\newpage

\section{Graph nets}

Sets and graphs are a very natural and flexible way of representing data of the real world. Neural networks that operate on such a structure have been explored for more than a decade \cite{Gori2005} and are usually called "graph neural networks". 

Graph nets (GN) were presented and discussed in a paper by Battaglia et al. in October 2018 \cite{Battaglia2018RelationalIB}. They are a specific neural network architecture that operate on a directed graph that can be represented by a 3-tuple $G = (\mathbf{u}, V,E)$. The $\mathbf{u}$ is the global attribute of the graph, $V$ is the set of nodes and $E$ is the set of edges. Each node and each edge can have a set of features associated with them:

$$ V = \{\mathbf{v}_i\}_{i=1:N^v}, E = \{(\mathbf{e}_k, r_k, s_k)\}_{k = 1:N^e}$$

where $\mathbf{v}_i$ are the node attributes and $\mathbf{e}_k$ are the edge attributes, $r_k$ is the receiver node and $s_k$ is the sender node. $V$ has cardinality $N^v$ and $E$ has cardinality $N^e$.

The algorithm that processes the graph is shown in \refalgo{GN_block}. A graph net block (GN block) contains three update functions $\phi$ and three aggregation functions $\rho$.  This block can be applied multiple times resulting in the outputs $(E', V', \mathbf{u}')$.

\begin{algorithm}
	\caption{Steps of computation in a full GN block}\label{GN_block}
	\begin{algorithmic}[1]
		\Function{\textsc{GraphNetwork}}{$E,V,\mathbf{u}$}
		\For{\texttt{$k \in \{1\dots N^e\}$}}
		\State $\mathbf{e}'_k \leftarrow \phi^e(\mathbf{e}_k, \mathbf{v}_{r_k}, \mathbf{v}_{s_k}, \mathbf{u})$ \Comment{Compute updated edge attribute}
		\EndFor

		\For{\texttt{$i \in \{1\dots N^v\}$}}
		\State \textbf{let} $E'_i = \{(\mathbf{e}'_k, r_k, s_k)\}_{r_k=i, k=1:N^e}$
		\State $\bar{\mathbf{e}}'_i \leftarrow \rho^{e \rightarrow v}(E'_i)$ \Comment{Aggregate edge attributes per node}
		\State $\mathbf{v}'_i \leftarrow \phi^v(\bar{\mathbf{e}}'_i, \mathbf{v}_i, \mathbf{u})$ \Comment{Compute updated node attributes}
		\EndFor
		\State \textbf{let} $V' = \{\mathbf{v}'\}_{i=1:N^v}$
		\State \textbf{let} $E' = \{(\mathbf{e}'_k, r_k, s_k)\}_{i=1:N^e}$
		\State $\bar{\mathbf{e}}' \leftarrow \rho^{e \rightarrow u}(E')$ \Comment{Aggregate edge attributes globally}
		\State $\bar{\mathbf{v}}' \leftarrow \rho^{v \rightarrow u}(V')$ \Comment{Aggregate node attributes globally}
		\State $\mathbf{u}' \leftarrow \phi^u (\bar{\mathbf{e}}', \mathbf{v}', \mathbf{u})$ \Comment{Compute updated global attributes}
		
		\State \textbf{return} $(E', V', \mathbf{u}')$
		\EndFunction
	\end{algorithmic}
\end{algorithm}