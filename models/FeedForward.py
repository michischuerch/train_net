import numpy as np
import matplotlib.pyplot as plt
import pandas
import math
import keras
import os
import sys
from keras.models import Sequential, Model, load_model
from keras.layers.wrappers import TimeDistributed
from keras.layers import Dense, Input, LSTM, Embedding, Flatten, Lambda,Activation
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error
import tensorflow as tf
from time import localtime, strftime
from keras import backend as K
import losses
from keras.models import model_from_json
import pickle
from sklearn.linear_model import LinearRegression
import data_utils

class FeedForwardNN:
	
	currentTime = strftime("%Y-%m-%d %H:%M:%S", localtime())
	
	def __init__(self, args, inputLengths,num_layers):
		self.args = args
		self.inputLengths = inputLengths
		self.num_layers = num_layers
	
		
	
	def make_model(self):
		args = self.args
		
		# line_id input
		input_id = Input(shape=(1,), dtype='float32', name='input_id')
		
		
		input_delays = Input(shape=(self.inputLengths[1],), dtype='float32', name='input_delays')
		input_lt = Input(shape=(1,), dtype='float32', name='input_lt')
		input_wd = Input(shape=(1,), dtype='float32', name='input_wd')
		input_hour = Input(shape=(1,), dtype='float32', name='input_hour')
		mean = Input(shape=(1,), dtype='float32', name='mean')
		sigma = Input(shape=(1,), dtype='float32', name='sigma')
		input_numerical = Input(shape=(2,), dtype='float32', name='input_num')
		#input_srt = Input(shape=(1,), dtype='float32', name='input_srt')
		
		def _one_hot(x, num_classes):
			return K.one_hot(K.cast(x, 'uint8'),
                          num_classes=num_classes)
		
		
		id_ohe = Lambda(_one_hot,
			arguments={'num_classes': self.inputLengths[0]},
			input_shape=(1,))(input_id)
		id_ohe = Flatten()(id_ohe)
		lt_ohe = Lambda(_one_hot,
			arguments={'num_classes': self.inputLengths[2]},
			input_shape=(1,))(input_lt)
		lt_ohe = Flatten()(lt_ohe)
		wd_ohe = Lambda(_one_hot,
			arguments={'num_classes': 7},
			input_shape=(1,))(input_wd)
		wd_ohe = Flatten()(wd_ohe)
		hour_ohe = Lambda(_one_hot,
			arguments={'num_classes': 24},
			input_shape=(1,))(input_hour)
		hour_ohe = Flatten()(hour_ohe)
		
		layer = keras.layers.concatenate([id_ohe,lt_ohe,hour_ohe, wd_ohe,input_numerical,input_delays])
		
		#layer = emb_type
		#layer = keras.layers.concatenate([emb_type,emb_wd])
		
		for i in range(self.num_layers):
			layer = Dense(2048,activation='tanh')(layer)
		output = Dense(1)(layer)
		
		output = keras.layers.Dropout(0.5)(output)
		
		all_inputs = [input_lt, input_wd, input_hour, input_numerical, input_id, input_delays,mean,sigma]
		model = Model(inputs=all_inputs, outputs=output)
		
		adam = keras.optimizers.Adam(lr=args.lr)
		model.compile(loss='mse', optimizer=adam,metrics=(['mse','mae']))
		
		self.model_nn = model
	
	def reset_model(self):
		print("resetting")
		self.make_model()
	
	def fit(self,X,y,X_val=[],y_val=[],use_callbacks=False,verbose=1):
		model_nn = self.model_nn
		batch_size = self.args.batch_size
		num_epochs = self.args.num_epochs
		cpu_count = self.args.cpu_count
		
		validation = None
		if len(X_val)>0 and len(y_val)>0:
			validation = (X_val,y_val)
		
		
		# training loop
		return model_nn.fit(X, y, validation_data=validation, batch_size=batch_size, epochs=num_epochs,verbose=1)
		
		
	def predict(self,dataX):
		model_nn = self.model_nn
		cpu_count = self.args.cpu_count
		
		
		return model_nn.predict(dataX).flatten()
							
	
