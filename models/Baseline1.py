from sklearn.linear_model import LinearRegression,Lasso,Ridge
from keras.wrappers.scikit_learn import KerasRegressor

class Baseline1(LinearRegression):
	
	
	def fit(self, X, y, sample_weight=None):
		return self
		
	def predict(self, X):
		return X
		
	def reset_model(self):
		return

from sklearn.linear_model import LinearRegression

class Lasso_standardized(Lasso):
	
		
	def fit(self,X,y):
		mean = X[:,1]
		std = X[:,2]
		
		return super().fit(X[:,3:],(y - mean) / std)
		
		
	def predict(self, X):
		mean = X[:,1]
		std = X[:,2]
		
		
		yPred = super().predict(X[:,3:])
		return (yPred*std)+mean
		
class Ridge_standardized(Ridge):
	
		
	def fit(self,X,y):
		mean = X[:,1]
		std = X[:,2]
		
		return super().fit(X[:,3:],(y - mean) / std)
		
		
	def predict(self, X):
		mean = X[:,1]
		std = X[:,2]
		
		
		yPred = super().predict(X[:,3:])
		return (yPred*std)+mean
