# HOW TO USE
1. For training start by downloading some data from https://opentransportdata.swiss/de/dataset/istdaten and add it to a folder data/train for training and data/validate for validation

2. then run the following:

	python main.py

To see all options run the following:

	python main.py -h

Note: I save the preprocessed data into temp/ folder and reuse it for the next run. If you want 
to 
refresh the preprocessing use the `--refreshPreprocessing`

# DIARY

current progress and TODOs can be found in my diary: https://www.overleaf.com/read/sbrvvxtrdqvt

# DATA

data is expected in data/* folder

datasets can be found at:
https://opentransportdata.swiss/de/dataset/istdaten

The full schedule can be found at (not used at the moment):
https://opentransportdata.swiss/de/dataset/timetable-2018-hrdf

# REQUIREMENTS

python: tensorflow, keras, sklearn

