# this was build by looking at the shortest path example at
# https://colab.research.google.com/github/deepmind/graph_nets/blob/master/graph_nets/demos/shortest_path.ipynb#scrollTo=TrGithqWUML7


from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import collections
import itertools
import time

from graph_nets import graphs
from graph_nets import utils_np
from graph_nets import utils_tf
from demos import models
import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
from scipy import spatial
import tensorflow as tf
import sys
from sklearn.metrics import mean_squared_error,mean_absolute_error
import threading
from multiprocessing import Queue
import time



DISTANCE_WEIGHT_NAME = "d"  # The name for the distance edge attribute.

def set_diff(seq0, seq1):
  """Return the set difference between 2 sequences as a list."""
  return list(set(seq0) - set(seq1))

def get_weighted_edges(edges, weights):
	
	return [(edges[i][1], edges[i][0], np.array([weights[i]], dtype = np.float)) for i in range(len(edges))]
		

def generate_graph(e, num_nodes, weighted_edges):
	"""Creates a graph from a weighted list of connected edges
	
	Args:
	num_nodes: the amount of nodes of the graph
	weighted_edges: a list representation with origin, destination and weight of an edge, format: (from, to, weight) 
	
	"""
	
	graph = nx.DiGraph()
	graph.add_nodes_from(range(0,num_nodes))
	graph.add_weighted_edges_from(weighted_edges, weight="features")
	
	graph.add_nodes_from(graph.nodes(), features=np.array([0,0,1], dtype = np.float))
	graph.add_node(e[0], features=np.array([1,0,0], dtype = np.float))
	graph.add_node(e[1], features=np.array([0,1,0], dtype = np.float))
	
	
	"""graph.add_node(e[0], start=True)
	graph.add_node(e[1], end=True)
	graph.add_nodes_from(set_diff(graph.nodes(), [e[0]]), start=False)
	graph.add_nodes_from(set_diff(graph.nodes(), [e[1]]), end=False)"""
	
	return graph

def get_next_graph_batch(dm,data_generator):
	
	num_nodes = len(dm.bpuic_vocab_map)
	
	# get next sample
	x,y = next(data_generator)
	
	x1,x2,x22,x3,x_edge,x_graph, data_mean, data_sigma = x
	
	graph_batch = []
	for i in range(len(x_edge)):
		# find corresponding nodes
		from_ = dm.bpuic_vocab_map[dm.reverseMapEdge[int(x_edge[i])][0]]
		to_ = dm.bpuic_vocab_map[dm.reverseMapEdge[int(x_edge[i])][1]]
		e = (from_,to_)
		
		#print(data_graph[i])
		
		# calculate edges
		weighted_edges = get_weighted_edges(dm.graph_edges, x_graph[i])
		
		G = generate_graph(e, num_nodes,weighted_edges)
		
		# extract features
		features = []
		
		# add one hot encodings
		for j in range(len(dm.lt_vocab_map)):
			if j == x1[i]:
				features.append(1)
			else:
				features.append(0)
		
		for j in range(7):
			if j == x2[i]:
				features.append(1)
			else:
				features.append(0)
				
		for j in range(24):
			if j == x22[i]:
				features.append(1)
			else:
				features.append(0)
		
		# add numerical features
		features.append(x3[i,0])
		features.append(x3[i,1])
		
		# the graph needs to have features as well
		G.graph["features"] = np.array(features, dtype=np.float)
		
		graph_batch.append(G)
	
	
	return graph_batch, y, data_mean, data_sigma


def create_placeholders(dm, rand, batch_size):
	"""Creates placeholders for the model training and evaluation.

	Args:
		rand: A random seed (np.RandomState instance).
		batch_size: Total number of graphs per batch.
    """
	"""input_graphs, target_graphs, _ = generate_networkx_graphs(
		rand, batch_size, num_nodes_min_max, theta)"""
		
	# generate a sample graph
	graph_batch, y_batch, mean, sigma = get_next_graph_batch(dm,dm.generators_dataX_dataY_precomputed([np.zeros(dm.args.batch_size),
		np.zeros(dm.args.batch_size), 
		np.zeros(dm.args.batch_size), 
		np.zeros((dm.args.batch_size, 2)),
		np.zeros(dm.args.batch_size),
		np.zeros((dm.args.batch_size, len(dm.graph_vocab))),
		np.zeros(dm.args.batch_size),
		np.zeros(dm.args.batch_size)
		], 
		np.zeros(dm.args.batch_size)))
	
	
	input_ph = utils_tf.placeholders_from_networkxs(
		graph_batch, force_dynamic_num_graphs=True)
	target_ph = tf.placeholder(tf.float32, shape=(len(y_batch)), name="target")
	#mean_ph = tf.placeholder(tf.float64, shape=(len(y_batch)), name="mean")
	#sigma_ph = tf.placeholder(tf.float64, shape=(len(y_batch)), name="sigma")

	return input_ph, target_ph

def create_loss_ops(target_op, output_ops):
	
	batch_size = target_op.shape[0]
	
	loss_ops = tf.losses.mean_squared_error(target_op, output_ops)
	return loss_ops

class GraphNet:
	
	def __init__(self, args,dm):
		self.args = args
		self.dm = dm
		
		self.sess = tf.Session()
	
	def reset_model(self):
		tf.reset_default_graph()

		rand = np.random.RandomState(seed=2)
		
		# Model parameters.
		# Number of processing (message-passing) steps.
		num_processing_steps_tr = 1
		num_processing_steps_ge = 1
		
		self.input_ph, self.target_ph = create_placeholders(self.dm, rand, self.args.batch_size)
		
		model = models.EncodeProcessDecode(edge_output_size=0, node_output_size=0,global_output_size=1)
		# A list of outputs, one per processing step.
		output_ops_tr = model(self.input_ph, num_processing_steps_tr)
		output_ops_ge = model(self.input_ph, num_processing_steps_ge)
		
		self.output_ops_tr = tf.squeeze(output_ops_tr[-1].globals)
		self.output_ops_ge = tf.squeeze(output_ops_ge[-1].globals)
		
		#output_ops_tr = tf.reshape(tf.reduce_mean(output_ops_tr[-1].edges),[1])
		#output_ops_ge = tf.reshape(tf.reduce_mean(output_ops_ge[-1].edges),[1])
		
		#self.output_ops_tr_seconds = output_ops_tr * self.sigma_ph + self.mean_ph
		#self.output_ops_ge_seconds = output_ops_ge * self.sigma_ph + self.mean_ph
		
		
		self.loss_op_tr = create_loss_ops(self.target_ph, self.output_ops_tr)
		self.loss_op_ge = create_loss_ops(self.target_ph, self.output_ops_ge)
		
		# Optimizer.
		optimizer = tf.train.AdamOptimizer(self.args.lr)
		self.step_op = optimizer.minimize(self.loss_op_tr)

		# reset session
		try:
		  self.sess.close()
		except NameError:
		  pass
		self.sess = tf.Session()
		self.sess.run(tf.global_variables_initializer())
		
	def fit_generator(self,generatorTrain,generatorVal, dataSize, valSize):
		last_iteration = 0
		logged_iterations = []
		losses_tr = []
		corrects_tr = []
		solveds_tr = []
		losses_ge = []
		corrects_ge = []
		solveds_ge = []
		
		# run training loop

		# How much time between logging and printing the current results.
		log_every_seconds = 20

		print("# (iteration number), T (elapsed seconds), "
		  "Ltr (training loss), Lge (test/generalization loss), "
		  "Ctr (training fraction nodes/edges labeled correctly), "
		  "Str (training fraction examples solved correctly), "
		  "Cge (test/generalization fraction nodes/edges labeled correctly), "
		  "Sge (test/generalization fraction examples solved correctly)")
		print("")
		start_time = time.time()
		last_log_time = start_time
		
		summary = {}
		summary["loss"] = []
		summary["val_loss"] = []
		summary["real_mean_absolute_error"] = []
		summary["real_val_mean_absolute_error"] = []
		
		queue_train = Queue(64)
		queue_val = Queue(64)
		
		def add_training_sample():
			for item in get_next_graph_batch(self.dm,generatorTrain):
				queue_train.put(item)
				
		def add_validation_sample():
			for item in get_next_graph_batch(self.dm,generatorVal):
				queue_val.put(item)
				
		generator_graph_train = get_next_graph_batch(self.dm,generatorTrain)
		generator_graph_val = get_next_graph_batch(self.dm,generatorVal)
		
		#threading.Thread(target=add_training_sample).start()
		#threading.Thread(target=add_validation_sample).start()
		
		for epoch in range(self.args.num_epochs):
		
			losses = []
			losses_abs = []
		
			the_time = time.time()
			print("NEXT EPOCH")
			for iteration in range((dataSize+self.args.batch_size-1)//self.args.batch_size):
				last_iteration = iteration
				
				x,y,mean_train,sigma_train = get_next_graph_batch(self.dm,generatorTrain)
				#x,y = next(generator_graph_train)
				#x,y = queue_train.get()
				
				#print(utils_np.networkxs_to_graphs_tuple(x), end="\r")
				
				# batch_size should be always same
				#if len(y)!=args.batch_size: continue
				
				train_values = self.sess.run({
				  "step": self.step_op,
				  "target": self.target_ph,
				  "loss": self.loss_op_tr,
				  "outputs": self.output_ops_tr
				}, feed_dict={self.input_ph: utils_np.networkxs_to_graphs_tuple(x), self.target_ph: y})
				
				
				elapsed_since_last_log = the_time - last_log_time
				
				losses.append(train_values['loss'])
				
				real_values_target = np.add(mean_train,np.multiply(np.array(train_values['target']).flatten(),sigma_train))
				real_values_outputs = np.add(mean_train,np.multiply(np.array(train_values['outputs']).flatten(),sigma_train))
				
				
				losses_abs.append(mean_absolute_error(real_values_target,real_values_outputs))
				
				
				#print(loss_values)
				
				elapsed = time.time() - start_time
				
				print("# {:05d}, T {:.1f}, train_loss {:.4f}, real_absolute_loss {:.4f}".format(
							  iteration, elapsed, np.mean(losses), np.mean(losses_abs),
							), end="\r")
			summary["loss"].append(np.mean(losses))
			summary["real_mean_absolute_error"].append(np.mean(losses_abs))
			
			#summary["real_mean_absolute_error"].append(mean_absolute_error(output_values_real,target_values_real))			
			print("# {:05d}, T {:.1f}, train_loss {:.4f}, real_absolute_loss {:.4f}".format(
							  iteration, elapsed, np.mean(losses), np.mean(losses_abs),
							))
			losses = []
			losses_abs = []
			print("VALIDATION")
			
			for iteration in range((valSize+self.args.batch_size-1)//self.args.batch_size):
				last_iteration = iteration
				
				x,y,mean_val,sigma_val = get_next_graph_batch(self.dm,generatorVal)
				#x,y = next(generator_graph_val)
				
				#x,y = queue_val.get()
				
				
				#print(utils_np.networkxs_to_graphs_tuple(x), end="\r")
				
				# batch_size should be always same
				#if len(y)!=args.batch_size: continue
				
				train_values = self.sess.run({
				  "loss": self.loss_op_tr,
				  "target": self.target_ph,
				  "outputs": self.output_ops_tr
				}, feed_dict={self.input_ph: utils_np.networkxs_to_graphs_tuple(x), self.target_ph: y})
				
				losses.append(train_values['loss'])
				
				real_values_target = np.add(mean_val,np.multiply(np.array(train_values['target']).flatten(),sigma_val))
				real_values_outputs = np.add(mean_val,np.multiply(np.array(train_values['outputs']).flatten(),sigma_val))
				
				
				losses_abs.append(mean_absolute_error(real_values_target,real_values_outputs))
				
				#print(loss_values)
				
				elapsed = time.time() - start_time
				
				print("# {:05d}, T {:.1f}, val_loss {:.4f}, real_absolute_loss {:.4f}".format(
							  iteration, elapsed, np.mean(losses), np.mean(losses_abs),
							), end="\r")
				
			summary["val_loss"].append(np.mean(losses))
			summary["real_val_mean_absolute_error"].append(np.mean(losses_abs))
			print("# {:05d}, T {:.1f}, val_loss {:.4f}, real_absolute_loss {:.4f}".format(
							  iteration, elapsed, np.mean(losses), np.mean(losses_abs),
							), end="\r")
			
			
			
		return summary
			
	def predict(self,generatorTest,dataSize):
		
		values_ge = np.array([])
		
		#generator_graph_test = get_next_graph_batch(self.dm,generatorTest)
		
		for iteration in range((dataSize+self.args.batch_size-1)//self.args.batch_size + 1):
			
			#last_log_time = the_time
			
			#x,y = get_next_graph_batch(self.dm,generatorTest)
			#x,y = next(generator_graph_test)
			x,y,mean,sigma = get_next_graph_batch(self.dm,generatorTest)
			
			test_values = self.sess.run({
				"target": self.target_ph,
				"loss": self.loss_op_ge,
				"outputs": self.output_ops_ge
			}, feed_dict={self.input_ph: utils_np.networkxs_to_graphs_tuple(x), self.target_ph: y})
			
			#elapsed = time.time() - start_time
			#losses_tr.append(train_values["loss"])
			#corrects_tr.append(correct_tr)
			#solveds_tr.append(solved_tr)
			values_ge = np.append(values_ge,test_values["outputs"])
			
			#corrects_ge.append(correct_ge)
			#solveds_ge.append(solved_ge)
			#logged_iterations.append(iteration)
			#print("# {:05d}, T {:.1f}, test error {:.4f}".format(
			#		  iteration, elapsed, np.average(losses_ge),
			#		), end="\r")
					
		#print("# {:05d}, T {:.1f}, test error {:.4f}".format(
		#			  iteration, elapsed, np.average(losses_ge),
		#			))
		
		return values_ge[0:dataSize]






