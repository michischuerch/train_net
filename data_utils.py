import numpy as np
import pandas as pd
from datetime import datetime
from collections import OrderedDict
import sys
import time
from thinkbayes import Cdf,MakeCdfFromList,GaussianPdf,GaussianCdfInverse
import matplotlib.pyplot as plt
from matplotlib.legend_handler import HandlerLine2D
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import mean_squared_error,mean_absolute_error
from sklearn.model_selection import KFold,cross_val_score
from sklearn.metrics import make_scorer
from operator import itemgetter
from preprocessing import prepare_X_y
import swiss_map

from joblib import Parallel, delayed
import multiprocessing

# dataset from https://opentransportdata.swiss/de/dataset/istdaten
# format given at https://opentransportdata.swiss/de/cookbook/ist-daten/
def load_dataset(args,path):
	
	nrows = None
	# for debugging reasons I only take the first few samples if this value is set (since building the whole dataset can take some time)
	if args.useSmallData:
		nrows = 1000
	
	data = pd.read_csv(path, sep=';',header=0,nrows=nrows,
		dtype={
		'BETRIEBSTAG': np.unicode, 
		'FAHRT_BEZEICHNER': np.unicode, 
		'BETREIBER_ID': np.unicode, 
		'BETREIBER_ABK': np.unicode, 
		'BETREIBER_NAME': np.unicode,
		'PRODUKT_ID': np.unicode, 
		'LINIEN_ID': np.unicode, 
		'LINIEN_TEXT': np.unicode, 
		'UMLAUF_ID': np.unicode, 
		'VERKEHRSMITTEL_TEXT': np.unicode, 
		'ZUSATZFAHRT_TF': np.unicode,
		'FAELLT_AUS_TF': np.unicode, 
		'BPUIC': np.int64, 	# unique ID of operation point
		'HALTESTELLEN_NAME': np.unicode, 
		'ANKUNFTSZEIT': np.unicode, # scheduled arrival time
		'AN_PROGNOSE': np.unicode, # actual arrival time 
		'AN_PROGNOSE_STATUS': np.unicode, 
		'ABFAHRTSZEIT': np.unicode, # scheduled departure time
		'AB_PROGNOSE': np.unicode, # actual departure time
		'AB_PROGNOSE_STATUS': np.unicode, 
		'DURCHFAHRT_TF': np.unicode})
	
	# see https://docs.python.org/3/library/datetime.html#datetime.datetime.strptime
	# convert datetime
	data['ANKUNFTSZEIT'] = pd.to_datetime(data['ANKUNFTSZEIT'], format='%d.%m.%Y %H:%M')
	data['AN_PROGNOSE'] = pd.to_datetime(data['AN_PROGNOSE'], format='%d.%m.%Y %H:%M:%S')
	
	data['ABFAHRTSZEIT'] = pd.to_datetime(data['ABFAHRTSZEIT'], format='%d.%m.%Y %H:%M')
	data['AB_PROGNOSE'] = pd.to_datetime(data['AB_PROGNOSE'], format='%d.%m.%Y %H:%M:%S')	
		
	return data


def load_map(path):
	
	data = pd.read_csv(path, sep=',',header=0,
		dtype={
		'BPK': np.unicode, 
		'long': np.double, 
		'lat': np.double })
		
	return data
	
	
	

# calculates the delay from the target time (from schedule) 
# and the actual time the train arrived
def calc_delay(times):
	
	time_actual,time_schedule = times
	
	# see https://docs.python.org/3/library/datetime.html#datetime.datetime.strptime
	#date_schedule = datetime.strptime(time_schedule, '%d.%m.%Y %H:%M')
	#date_actual = datetime.strptime(time_actual, '%d.%m.%Y %H:%M:%S')
	
	date_delta = time_actual-time_schedule
	
	#print(time_schedule, time_actual, date_delta,np.timedelta64(date_delta, 's'))
	#sys.exit(0)
	#print(time_schedule, date_schedule, time_actual, date_actual, date_delta)
	return int(np.timedelta64(date_delta, 's').item().total_seconds())

def QQPlot(cdf, fit):
	"""Makes a QQPlot of the values from actual and fitted distributions.

	cdf: actual Cdf
	fit: model Cdf
	"""
	ps = cdf.ps
	actual = cdf.xs
	
	fitted = [fit(p) for p in ps]

	
	
	plt.xlabel("Model")
	plt.ylabel("Actual")
	
	mini = np.min(fitted)
	maxi = np.max(np.array(fitted)[np.isfinite(fitted)])
	
	line1, = plt.plot([mini,maxi],[mini,maxi], color='red',label='Identity line')
	
	plt.scatter(fitted, actual,s=0.7,label='QQ-plot')
	
	plt.legend()
	
	plt.show()
	
	plt.savefig("data/is_my_data_normal.png")
	
	
def make_QQ_plot(data):
	
	cdf = MakeCdfFromList(data)
	
	mean = np.average(data)
	sigma = np.sqrt(np.average(np.square(data-mean)))
	
	#gaussian = lambda x: ((2*np.pi*sigma**2))**(-1/2)*np.exp(-0.5 * (x-mean)**2 / sigma**2) 
	
	"""prob = [GaussianCdf(x,mean,sigma) for x in data]
	prob = prob/np.sum(prob)"""
	
	print(mean)
	print(sigma)
	
	#gpdf = [GaussianPdf(mean,sigma).Density(x) for x in np.arange(-100+mean,100+mean,0.01)]
	
	"""plt.plot(np.arange(-100+mean,100+mean,0.01), gpdf)
	plt.show()"""
	
	cdfInverse = lambda p: GaussianCdfInverse(p,mean,sigma)
	
	#fit = Cdf(np.arange(-100+mean,100+mean,0.0001), [GaussianCdf(x,mean,sigma) for x in np.arange(-100+mean,100+mean,0.0001)])
	
	QQPlot(cdf,cdfInverse)

def rmse_loss(y_true, y_pred): 
	
	return np.sqrt(mean_squared_error(y_true, y_pred))

"""def train_validate_test(args, model, X_train, y_train, X_test, y_test):
	# prepare data
	#model = GridSearchCV(model, param_grid=param_grid, n_jobs=args.cpu_count, cv=5,scoring="neg_mean_squared_error")
	#model.fit(X_train, y_train)
	
	seed = args.random_seed
	scoring = make_scorer(mean_squared_error, greater_is_better=False)
	
	# cross validation score
	kfold = KFold(n_splits=args.num_cv, shuffle=True, random_state=seed)
	results = cross_val_score(model, X_train, y_train, cv=kfold, scoring=scoring,n_jobs=args.cpu_count)
	print("Accuracy: %0.2f (+/- %0.2f)" % (-1*results.mean(), results.std()))
	
	# train model on whole data
	model.fit(X_train, y_train)

	# test data with trained model
	y_pred = model.predict(X_test)

	print("test_error MAE: " + str(mean_absolute_error(y_pred, y_test)))
	#print("test_error MAE: " + str(mean_absolute_error(y_pred,y_test)))
	print("\n")
	
	return -1*results.mean(), results.std(), mean_squared_error(y_pred, y_test)"""
	
def train_validate_test(args,dm,model, objective_function=mean_squared_error, dataformat="allData", predictionFormat="values", isKerasModel=True, useGenerators=False, multi_input=False,has_attention=True):
	
	data_train = dm.dataset_train
	data_test = dm.dataset_test
	
	seed = args.random_seed
	scoring = make_scorer(mean_absolute_error, greater_is_better=False)
	
	kfold = KFold(n_splits=args.num_cv, shuffle=True, random_state=seed)
	objective_function_error_per_fold_train = np.array([])
	objective_function_error_per_fold_val = np.array([])
	all_histories_val = np.array([])
	
	for i, (train_split, val_split) in enumerate(kfold.split(data_train)):
		if isKerasModel:
			model.reset_model()
		
		# aggregate training data
		training_data_cv = itemgetter(*train_split)(data_train)
		validation_data_cv = itemgetter(*val_split)(data_train)		
		
		# we need the mean and sigma for each edge for the Baseline and for standardization
		#print(np.unique(pd.concat(dm.allData_preprocessed)['edge_id']))
		
		allData_train = pd.concat(itemgetter(*train_split)(dm.allData))
		
		#print(i)
		#print(train_split)
		#print(len(training_data))
		
		X_train, y_train, X_val, y_val, y_mean_train, y_mean_val, y_sigma_train, y_sigma_val = prepare_X_y(dm, allData_train, training_data_cv, validation_data_cv, dataformat,predictionFormat,multi_input)
		
		if isKerasModel:
			
			history = {}
			history["loss"] = []
			history["val_loss"] = []
			history["mean_absolute_error"] = []
			history["val_mean_absolute_error"] = []
			history["mean_squared_error"] = []
			history["val_mean_squared_error"] = []
			
			history["real_mean_absolute_error"] = []
			history["real_mean_squared_error"] = []
			history["real_val_mean_absolute_error"] = []
			history["real_val_mean_squared_error"] = []
			
			for j in range(args.num_epochs):
				saveEpochs = args.num_epochs
				args.num_epochs=1	
				
				if useGenerators:
					generator_train = dm.generators_dataX_dataY_precomputed(X_train,y_train)
					generator_val = dm.generators_dataX_dataY_precomputed(X_val,y_val)
				
					h = model.fit_generator(generator_train, generator_val, len(X_train[0]), len(X_val[0])) 
					
					
					history["loss"].append(h["loss"])
					history["val_loss"].append(h["val_loss"])
					history["mean_absolute_error"].append(h["real_mean_absolute_error"])
					history["val_mean_absolute_error"].append(h["real_val_mean_absolute_error"])
				else:
					h = model.fit(X_train, y_train,X_val,y_val)
				
					history["mean_squared_error"].append(h.history["mean_squared_error"])
					history["val_mean_squared_error"].append(h.history["val_mean_squared_error"])
					history["mean_absolute_error"].append(h.history["mean_absolute_error"])
					history["val_mean_absolute_error"].append(h.history["val_mean_absolute_error"])
				
					history["loss"].append(h.history["loss"])
					history["val_loss"].append(h.history["val_loss"])
				
				args.num_epochs=saveEpochs
				
				y_train_ = y_train
				
				# y_pred = model.predict(dm.generators_dataX_dataY_precomputed(X_train,y_train), len(X_train[0]))
				if not useGenerators:
					
					y_pred = model.predict(X_train)
					if(predictionFormat == "standardized"):
						y_pred = np.add(y_mean_train,np.multiply(y_pred,y_sigma_train))
						y_train_ = np.add(y_mean_train,np.multiply(y_train,y_sigma_train))
					
					history["real_mean_absolute_error"].append(mean_absolute_error(y_pred, y_train_))
					history["real_mean_squared_error"].append(mean_squared_error(y_pred, y_train_))
					
					y_val_ = y_val
					if useGenerators:
						y_pred = model.predict(dm.generators_dataX_dataY_precomputed(X_val,y_val), len(X_val[0]))
					else:
						y_pred = model.predict(X_val)
					if(predictionFormat == "standardized"):
						y_pred = np.add(y_mean_val,np.multiply(y_pred,y_sigma_val))
						y_val_ = np.add(y_mean_val,np.multiply(y_val,y_sigma_val))
					
					history["real_val_mean_absolute_error"].append(mean_absolute_error(y_pred, y_val_))
					history["real_val_mean_squared_error"].append(mean_squared_error(y_pred, y_val_))
				
				print(history)
					
			all_histories_val = np.append(all_histories_val, history)
		else:
			
			print(X_train.shape, y_train.shape)
			
			model.fit(X_train, y_train)
		
		# training loss
		#if not useGenerators:
		#	y_pred = model.predict(dm.generators_dataX_dataY_precomputed(X_train,y_train), len(X_train[0]))
		if not useGenerators:
			y_pred = model.predict(X_train)
		
			y_train_ = y_train
			if(predictionFormat == "standardized"):
				y_pred = np.add(y_mean_train,np.multiply(y_pred,y_sigma_train))
				y_train_ = np.add(y_mean_train,np.multiply(y_train,y_sigma_train))
			objective_function_error_per_fold_train = np.append(objective_function_error_per_fold_train,objective_function(y_pred, y_train_))
			
			y_val_ = y_val
			# validation loss
			if useGenerators:
				y_pred = model.predict(dm.generators_dataX_dataY_precomputed(X_val,y_val), len(X_val[0]))
			else:
				y_pred = model.predict(X_val)
			if(predictionFormat == "standardized"):
				y_pred = np.add(y_mean_val,np.multiply(y_pred,y_sigma_val))
				y_val_ = np.add(y_mean_val,np.multiply(y_val,y_sigma_val))
			objective_function_error_per_fold_val = np.append(objective_function_error_per_fold_val,objective_function(y_pred, y_val_))
			
			#print(model.model_nn.evaluate(x=[data[val_split] for data in X_train], y=y_train[val_split],batch_size=args.batch_size))
		
			print("Average_error val %d: %0.2f" % (i,objective_function_error_per_fold_val[-1]))
			print("Average_error train %d: %0.2f" % (i,objective_function_error_per_fold_train[-1]))
	
	#print(objective_function_error_per_fold_val)
	
	error_mean_train = np.mean(objective_function_error_per_fold_train)
	error_std_train = np.std(objective_function_error_per_fold_train)
	error_mean_val = np.mean(objective_function_error_per_fold_val)
	error_std_val = np.std(objective_function_error_per_fold_val)
	
	print("errors average/std val: %0.2f (+/- %0.2f)" % (error_mean_val, error_std_val))
	print("errors average/std train: %0.2f (+/- %0.2f)" % (error_mean_train, error_std_train))
	
	if isKerasModel:
		model.reset_model()
	
	X_train, y_train, X_test, y_test,y_mean_train, y_mean_test, y_sigma_train, y_sigma_test  = prepare_X_y(dm,pd.concat(dm.allData), data_train, data_test,dataformat,predictionFormat,multi_input)
	
	history_test = {}
	history_test["loss"] = []
	history_test["val_loss"] = []
	history_test["mean_absolute_error"] = []
	history_test["val_mean_absolute_error"] = []
	history_test["mean_squared_error"] = []
	history_test["val_mean_squared_error"] = []
	
	history_test["real_mean_absolute_error"] = []
	history_test["real_mean_squared_error"] = []
	history_test["real_val_mean_absolute_error"] = []
	history_test["real_val_mean_squared_error"] = []
	history_test["softmax"] = []
	
	saveEpochs = args.num_epochs
	args.num_epochs = 1
	if isKerasModel:
		for i in range(saveEpochs):
			
			if useGenerators:
				generator_train = dm.generators_dataX_dataY_precomputed(X_train,y_train)
				generator_test = dm.generators_dataX_dataY_precomputed(X_test,y_test)
			
				h = model.fit_generator(generator_train, generator_test, len(X_train[0]), len(X_test[0]))
			
				history_test["loss"].append(h["loss"])
				history_test["val_loss"].append(h["val_loss"])
				history_test["mean_absolute_error"].append(h["real_mean_absolute_error"])
				history_test["val_mean_absolute_error"].append(h["real_val_mean_absolute_error"])
			
			else:
				h = model.fit(X_train, y_train, X_test, y_test, verbose=0,use_callbacks=True)
				
				history_test["mean_squared_error"].append(h.history["mean_squared_error"])
				history_test["val_mean_squared_error"].append(h.history["val_mean_squared_error"])
				history_test["mean_absolute_error"].append(h.history["mean_absolute_error"])
				history_test["val_mean_absolute_error"].append(h.history["val_mean_absolute_error"])
				
				if has_attention:
					softmax = model.predict_softmax(X_test)
					#history_test["softmax"].append(softmax)
					sm = swiss_map.SwissMap(args,dm)
					
					#edge_id = args.egde_to_observe #mm.dm.dataX_test[0][i]
					
					plt.title("Epoch " + str(i+1))
					
					if i < 10:
						sm.save_softmax_map(dm,softmax[0], dm.top_k_edge, "data/e11_softmax_trainnet_"+str(args.only_top_k)+"_0" + str(i) + ".png")
					else:
						sm.save_softmax_map(dm,softmax[0], dm.top_k_edge, "data/e11_softmax_trainnet_"+str(args.only_top_k)+"_" + str(i) + ".png")
						
					plt.clf()

				history_test["loss"].append(h.history["loss"])
				history_test["val_loss"].append(h.history["val_loss"])
			
			y_train_ = y_train
				#y_pred = model.predict(dm.generators_dataX_dataY_precomputed(X_train,y_train), len(X_train[0]))
			if not useGenerators:
				y_pred = model.predict(X_train)
				if(predictionFormat == "standardized"):
					y_pred = np.add(y_mean_train,np.multiply(y_pred,y_sigma_train))
					y_train_ = np.add(y_mean_train,np.multiply(y_train,y_sigma_train))
				
				history_test["real_mean_absolute_error"].append(mean_absolute_error(y_pred, y_train_))
				history_test["real_mean_squared_error"].append(mean_squared_error(y_pred, y_train_))
				
				
				y_test_ = y_test
				if useGenerators:
					y_pred = model.predict(dm.generators_dataX_dataY_precomputed(X_test,y_test), len(X_test[0]))
				else:
					y_pred = model.predict(X_test)
				if(predictionFormat == "standardized"):
					y_pred = np.add(y_mean_test,np.multiply(y_pred,y_sigma_test))
					y_test_ = np.add(y_mean_test,np.multiply(y_test,y_sigma_test))
				
				history_test["real_val_mean_absolute_error"].append(mean_absolute_error(y_pred, y_test_))
				history_test["real_val_mean_squared_error"].append(mean_squared_error(y_pred, y_test_))	
	else:
		model.fit(X_train, y_train)
	args.num_epochs = saveEpochs
	
	
	y_test_ = y_test
	# testing
	if useGenerators:
		y_pred = model.predict(dm.generators_dataX_dataY_precomputed(X_test,y_test), len(X_test[0]))
	else:
		y_pred = model.predict(X_test)
	if(predictionFormat == "standardized"):
		y_pred = np.add(y_mean_test,np.multiply(y_pred,y_sigma_test))
		y_test_ = np.add(y_mean_test,np.multiply(y_test,y_sigma_test))
	"""if(predictionFormat == "standardized"):
		y_pred = np.add(np.multiply(y_pred,y_sigma),y_mean)
	elif(predictionFormat == "standardizedEverything"):
		y_pred = np.add(np.multiply(y_pred,y_sigma),y_mean)
		y_test = np.add(np.multiply(y_test,y_sigma),y_mean)"""
	
	
	
	if isKerasModel:
		if useGenerators:
			return all_histories_val, history_test
		else:
			print("max error validation: " + str(np.max(objective_function_error_per_fold_val)))

			print("test_error: " + str(objective_function(y_pred, y_test_)))
			print("\n")
			
			return error_mean_train, error_std_train, error_mean_val, error_std_val, objective_function(y_pred, y_test_), all_histories_val, history_test
	else:
		print("max error validation: " + str(np.max(objective_function_error_per_fold_val)))

		print("test_error: " + str(objective_function(y_pred, y_test_)))
		print("\n")
		
		return error_mean_train, error_std_train, error_mean_val, error_std_val, objective_function(y_pred, y_test_)
	
def train_validate_test_generators(args, dm, model, X_train, y_train, X_test, y_test):
	
	seed = args.random_seed
	scoring = make_scorer(rmse_loss, greater_is_better=False)
	
	kfold = KFold(n_splits=args.num_cv, shuffle=True, random_state=seed)
	
	mean_error_per_fold = np.array([])
	
	for i, (train_split, val_split) in enumerate(kfold.split(y_train)):
		
		model.reset_model()
		
		generator_train = dm.generators_dataX_dataY_precomputed([data[train_split] for data in X_train],y_train[train_split])
		generator_val = dm.generators_dataX_dataY_precomputed([data[val_split] for data in X_train],y_train[val_split])
		
		model.fit_generator(generator_train, len(train_split))
		
		y_pred = model.predict(generator_val, len(val_split))[0:len(val_split)]
		e = np.square(y_pred - y_train[val_split])
		
		
		
		
		mean = np.mean(e)
		std = np.std(e)
		
		mean_error_per_fold = np.append(mean_error_per_fold,mean)
	
		print("Average_error %d: %0.2f (+/- %0.2f)" % (i,mean, std))
	
	print(mean_error_per_fold)
	
	mean = np.mean(mean_error_per_fold)
	std = np.std(mean_error_per_fold)
	
	print("validation errors average/std : %0.2f (+/- %0.2f)" % (mean, std))
	
	generator_train = dm.generators_dataX_dataY_precomputed(X_train,y_train)
	generator_test = dm.generators_dataX_dataY_precomputed(X_test,y_test)
	
	model.reset_model()
	model.fit_generator(generator_train, len(X_train[0]))
	
	y_pred = model.predict(generator_test,len(X_test[0]))[0:len(X_test[0])]

	print("max error: " + str(np.max(mean_error_per_fold)))
	
	print(np.max(y_pred))
	print(np.min(y_pred))
	
	print("test_error MSE: " + str(mean_squared_error(y_pred, y_test)))
	print("test_error MAE: " + str(mean_absolute_error(y_pred,y_test)))
	print("\n")	

	


"""def mean_squared_error_standardized(y_true, y_pred, sample_weight=None, multioutput='uniform_average'):
		
	mean = y_true[:,1]
	sigma = y_true[:,2]
	
	
	output_errors = np.average((y_true - (y_pred*sigma)-mean) ** 2, axis=0, weights=sample_weight)
	return np.average(output_errors, weights=multioutput)"""

	
 


