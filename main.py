import argparse
import data_utils
import swiss_map
from collections import OrderedDict
import numpy as np
from model import TrainNet
from DataManager import DataManager
import sys
from sklearn.metrics import mean_absolute_error, mean_squared_error
import matplotlib.pyplot as plt
plt.switch_backend('agg')
import matplotlib.dates as mdates
import pandas as pd
import losses
import os
from keras import backend as K
from sklearn.linear_model import LinearRegression, Ridge
from my_graph_nets import GraphNet
import time
from sklearn.ensemble import RandomForestRegressor
import preprocessing
import data_utils as du 
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import Kernel, CompoundKernel, ExpSineSquared, RBF, Product
from models.Baseline1 import Baseline1,Lasso_standardized, Ridge_standardized
import tensorflow as tf
from models.FeedForward import FeedForwardNN




DATAPATH_DEFAULT = 'data/raw_data'
LOADPATH_DEFAULT = 'temp/'
MAP_PATH_DEFAULT = 'data/swiss_map.csv'
TEMP_PATH_DEFAULT = 'temp'
NUM_EPOCHS_DEFAULT = 10
BATCH_SIZE_DEFAULT = 1
CPU_COUNT_DEFAULT = 8
CROSS_VALIDATION_SPLIT_AMOUNT_DEFAULT = 5
LR_DEFAULT = 0.001
RANDOM_SEED_DEFAULT = 42
TEST_RATIO_DEFAULT = 0.15

""" params """
parser = argparse.ArgumentParser(description='')
parser.add_argument('--useSmallData', dest='useSmallData', action='store_true', help='use only a small fraction of the dataset')
parser.add_argument('--refreshPreprocessing', dest='refreshPreprocessing', action='store_true', help='force preprocessing to be done again. Note: you can also remove the files generated in temp/ for this')
parser.add_argument('--data_path', dest='data_path', default=DATAPATH_DEFAULT, help='path of dataset (default: ' + DATAPATH_DEFAULT + ')')
parser.add_argument('--load_path', dest='load_path', default=LOADPATH_DEFAULT, help='where to save the preprocessed dataset (default: ' + LOADPATH_DEFAULT + ')')
parser.add_argument('--map_path', dest='map_path', default=MAP_PATH_DEFAULT, help='path of all the lines to make a nice drawing')
parser.add_argument('--epochs', dest='num_epochs', type=int, default=NUM_EPOCHS_DEFAULT, help='how many epochs to train (default: ' + str(NUM_EPOCHS_DEFAULT) + ')')
parser.add_argument('--batch_size', dest='batch_size', type=int, default=BATCH_SIZE_DEFAULT, help='batch size to use (default: ' + str(BATCH_SIZE_DEFAULT) + ')')
parser.add_argument('--cpu_count', dest='cpu_count', type=int, default=CPU_COUNT_DEFAULT, help='number of default cpus on current system (default: ' + str(CPU_COUNT_DEFAULT) + ')')
parser.add_argument('--load_model', dest='load_model', default=None, help='Path to model to load')
parser.add_argument('--onlyValidate', dest='onlyValidate', action='store_true', help='no training, only prediction on validation data')
parser.add_argument('--lr', dest='lr', type=float, default=LR_DEFAULT, help='learning rate when training (default: ' + str(LR_DEFAULT) + ')')
parser.add_argument('--cudaDevice', dest='cudaDevice', default="0", help='cuda device to use, only use one, if using all devices type \"all\"')
parser.add_argument('--mode', dest='mode', default="train", help='cuda device to use, only use one, if using all devices type \"all\"')
parser.add_argument('--experiment_num', dest='experiment_num', type=int, default=2, help='which experiment to perform')
parser.add_argument('--decay', type=float, dest='decay', default=0.0, help='decay parameter of training')
parser.add_argument('--only_top_k', type=int, dest='only_top_k', default=-1, help='train and test only on top k edges of the graph, edges with more updates are preferred first')
parser.add_argument('--min_updates', type=int, dest='min_updates', default=62, help='minimum amount of updates for a edge to be valid')
parser.add_argument('--egde_to_observe', type=int, dest='egde_to_observe', default=503, help='minimum amount of updates for a edge to be valid')
parser.add_argument('--num_cv', type=int, dest='num_cv', default=CROSS_VALIDATION_SPLIT_AMOUNT_DEFAULT, help='the number of splits for the cross validation')
parser.add_argument('--random_seed', type=int, dest='random_seed', default=RANDOM_SEED_DEFAULT, help='random seed of calculations (default: ' + str(RANDOM_SEED_DEFAULT) + ')')
parser.add_argument('--test_ratio', type=float, dest='test_ratio', default=TEST_RATIO_DEFAULT, help='ratio of test splitting (default: ' + str(TEST_RATIO_DEFAULT) + ')')


args = parser.parse_args()
# only allocate 1 GPU 
os.environ["CUDA_VISIBLE_DEVICES"]=args.cudaDevice

np.random.seed(args.random_seed)
tf.set_random_seed(args.random_seed)

#mm = ModelManager(args)


if args.mode == "train":
	
	dm = DataManager(args)
	model = TrainNet(args,len(dm.edge_vocab_map),len(dm.graph_vocab))
	model.fit_generator(dm.generators_dataX_dataY(dm.dataset_train,shuffle_batch=True), dm.generators_dataX_dataY(dm.dataset_test,shuffle_batch=False), len(dm.dataset_train)//(args.batch_size), len(dm.dataset_test)//(args.batch_size))
	
elif args.mode == "train_aware":
	
	dm = DataManager(args,useGenerators=False)
	
	X_train_0,y_train_0 = preprocessing.extractFeatures(dm,dm.dataset_train)
	X_test_0,y_test_0 = preprocessing.extractFeatures(dm,dm.dataset_test)
	
	print("TrainNet train-aware: \n")
	start_time = time.time()
	
	args.lr = 0.0001
	
	X_train = dm.dataX_train
	X_train.append(X_train_0[:,1])
	X_train.append(X_train_0[:,2])
	X_train.append(X_train_0[:,3])
	X_train.append(X_train_0[:,4])
	X_train.append(X_train_0[:,5])
	X_test = dm.dataX_test
	X_test.append(X_test_0[:,1])
	X_test.append(X_test_0[:,2])
	X_test.append(X_test_0[:,3])
	X_test.append(X_test_0[:,4])
	X_test.append(X_test_0[:,5])
	
	inputLengths = [len(dm.edge_vocab_map),len(dm.graph_vocab), len(dm.lt_vocab_map) ]
	model = TrainNet(args,inputLengths,train_agnostic=False)
	model.fit(X_train, dm.dataY_train,verbose=0,use_callbacks = True)
	
	y_pred = model.predict([X_test])
	
	e = np.square(y_pred - dm.dataY_test)
		
	mean = np.mean(e)
	std = np.std(e)

	print("Average_error %d: %0.2f (+/- %0.2f)" % (i,mean, std))
	
	#du.train_validate_test_keras(args, TrainNet(args,inputLengths,train_agnostic=False), "neg_mean_squared_error", X_train, dm.dataY_train, X_test, dm.dataY_test)
	print("time:" + str(time.time() - start_time))

elif(args.mode == "test"):
	
	dm = DataManager(args,useGenerators=False)
	
	X_train_0,y_train_0 = preprocessing.extractFeatures(dm,dm.dataset_train)
	X_test_0,y_test_0 = preprocessing.extractFeatures(dm,dm.dataset_test)
	
	print("TrainNet train-aware: \n")
	start_time = time.time()
	
	args.lr = 0.0001
	
	X_train = dm.dataX_train
	X_train.append(X_train_0[:,1])
	X_train.append(X_train_0[:,2])
	X_train.append(X_train_0[:,3])
	X_train.append(X_train_0[:,4])
	X_train.append(X_train_0[:,5])
	X_test = dm.dataX_test
	X_test.append(X_test_0[:,1])
	X_test.append(X_test_0[:,2])
	X_test.append(X_test_0[:,3])
	X_test.append(X_test_0[:,4])
	X_test.append(X_test_0[:,5])
	
	inputLengths = [len(dm.edge_vocab_map),len(dm.graph_vocab), len(dm.lt_vocab_map) ]
	model = TrainNet(args,inputLengths,train_agnostic=False)
	#model.fit(X_train, dm.dataY_train,verbose=0,use_callbacks = True)
	
	y_pred = model.predict(X_test)
	
	e = np.square(y_pred - dm.dataY_test)
		
	mean = np.mean(e)
	std = np.std(e)

	print("Average_error: %0.2f (+/- %0.2f)" % (mean, std))
	
	#du.train_validate_test_keras(args, TrainNet(args,inputLengths,train_agnostic=False), "neg_mean_squared_error", X_train, dm.dataY_train, X_test, dm.dataY_test)
	print("time:" + str(time.time() - start_time))


	#dataX, dataY = mm.dm.calculate_dataX_dataY(mm.dm.dataset_test)
	
	#mm.test(mm.dm.dataX_test,mm.dm.dataY_test)
	#mm.test_generator()
	sys.exit()
	
	#sigma2 = mm.predict_sigma(dataX)
	yPred = mm.predict(mm.dm.dataX_test)
	print(yPred)
	#graph_out = mm.predict_graphOut(dataX)
	"""softmax_max_out = mm.predict_softmax_max(dataX)
	softmax_argmax_out = mm.predict_softmax_max(dataX)"""
	
	#mask = softmax_argmax_out
	#mask[softmax_argmax_out==len(mm.dm.graph_vocab)] = 1
	
	#print(np.sum(graph_out != 0))
	
	
	mask = mm.dm.dataset_test['delay'] >60
	
	#err = np.multiply((1/sigma2),np.square(yPred - dataY)) + np.log(sigma2)
	err = np.abs(yPred - mm.dm.dataY_test)
	
	print("printing")
	
	
	"""plt.plot(np.sqrt(sigma2[graph_out!=0]),np.arange(len(sigma2[graph_out!=0])))
	plt.savefig("data/0_sigmaGraph.png")"""
	
	#plt.clf()
	
	#plt.scatter(np.arange(len(graph_out))[mask==1], graph_out[mask==1],c='red',s=0.5)
	"""plt.scatter(np.arange(len(graph_out))[mask==0], graph_out[mask==0],c='blue',s=0.5)
	plt.savefig("data/0_graphOutGraph.png")
	
	plt.clf()"""
	
	#plt.scatter(np.arange(len(err))[mask==1], err[mask==1],c='red',s=0.5)
	plt.scatter(np.arange(len(err))[mask==0], err[mask==0],c='blue',s=0.5)
	plt.savefig("data/0_errGraph.png")
	
	plt.clf()
	
	#plt.scatter(np.arange(len(softmax_max_out))[mask==1], softmax_max_out[mask==1], c='red',s=0.5)
	"""plt.scatter(np.arange(len(softmax_max_out))[mask==0], softmax_max_out[mask==0], c='blue',s=0.5)
	plt.savefig("data/0_softmax.png")"""
	
	
elif(args.mode == "baseline1"):
	
	date_delta = (mm.dm.dataset_test['next_SCHEDULED_TIME']-mm.dm.dataset_test['SCHEDULED_TIME']).astype('timedelta64[s]').astype(int)
	
	print("Baseline 1 MAE: " + str(mean_absolute_error(mm.dm.dataset_test['running_time'], date_delta)))
	print("Baseline 1 MSE: " + str(mean_squared_error(mm.dm.dataset_test['running_time'], date_delta)))
	


elif(args.mode == "baseline2"):
	dataY = mm.dm.dataset_test[mm.dm.objective_string]
	
	yPred = [mm.dm.mean_per_edge[t] for t in mm.dm.dataset_test['edge_id']]
	
	print("Baseline 2 MAE: " + str(mean_absolute_error(yPred,dataY)))
	print("Baseline 2 MSE: " + str(mean_squared_error(yPred,dataY)))	
	
elif(args.mode == "is_my_data_normal"):
	
	# check if data is normal

	amounts = [mm.dm.amount_per_edge[e] for e in mm.dm.dataset_train['edge_id']]

	print(np.max(amounts))
	print(mm.dm.dataset_train[amounts == np.max(amounts)]['BPUIC'].iloc[0])
	print(mm.dm.dataset_train[amounts == np.max(amounts)]['next_BPUIC'].iloc[0])

	reverseMap = dict(zip(mm.dm.edge_vocab_map.values(),mm.dm.edge_vocab_map.keys()))
	u,v = reverseMap[args.egde_to_observe]


	print(mm.dm.dataset_train[mm.dm.dataset_train['BPUIC']==u]['next_HALTESTELLEN_NAME'])

	subData = mm.dm.dataset_test[np.logical_and(mm.dm.dataset_test['BPUIC']==u,mm.dm.dataset_test['next_BPUIC']==v)]
	print(len(subData))
	
	print(np.unique(subData['HALTESTELLEN_NAME']))
	print(np.unique(subData['next_HALTESTELLEN_NAME']))
	
	plt.title(np.unique(subData['HALTESTELLEN_NAME']) + "-" + np.unique(subData['next_HALTESTELLEN_NAME']))
	
	#print(subData['delta_delay'].values)
	data_utils.make_QQ_plot(subData['running_time'].values)
	
elif(args.mode == "investigate_edges"):
	
	numbers = mm.dm.amount_per_edge
		
	numbers = np.sort(list(numbers.values()))
	
	plt.plot(np.arange(len(numbers)),numbers)
	
	plt.xlabel("edge")
	plt.ylabel("amount")
	
	plt.savefig("data/investigate_edges.png")

#elif(args.mode == "plot_stuff"):

elif(args.mode == "cv_explain"):
	#plt.plot(np.arange(len(train_e))+1,train_e,linewidth=0.5,color=colors[2])
	plt.errorbar([1], [1], yerr= [0.5], fmt='o', linewidth=0.5, markersize=3, capsize=3,color='rebeccapurple')
	plt.xlabel('Parameter')
	plt.ylabel('Sum of losses')
	
	
	plt.show()
	plt.savefig("data/cv_explain.png")

elif(args.mode == "map"):
	
	args.only_top_k = 113
	
	dm = DataManager(args,useGenerators=False)
	
	sm = swiss_map.SwissMap(args)
	sm.save_swiss_map(dm)
	
elif(args.mode == "map_amount_per_edge"):
	
	sm = swiss_map.SwissMap(args,mm.dm)
	sm.save_swiss_map_amount_per_edge()
	
elif(args.mode == "map_softmax"):
	
	softmax = mm.predict_softmax(mm.dm.dataX_test)
	
	
	sm = swiss_map.SwissMap(args,mm.dm)
	
	"""for i in range(len(softmax)):
		if np.max(softmax[i]) == np.max(softmax):
			break"""
			
	#i = 123
	
	
	edge_id = args.egde_to_observe #mm.dm.dataX_test[0][i]

	#reverseMap = dict(zip(mm.dm.edge_train_vocab_map.values(),mm.dm.edge_train_vocab_map.keys()))
	
	
	sm.save_softmax_map(softmax[0], edge_id)
	
elif(args.mode == "lasso" or args.mode == "ridge"):
	# linear regression, intercept centers the data
	#model = LinearRegression(n_jobs=args.cpu_count,fit_intercept=True)
	#model = Ridge(alpha=500)
	if args.mode == "lasso":
		model = Lasso(alpha=0.1)
	elif args.mode == "ridge":
		model = Ridge(alpha=500)
	X = mm.dm.dataX_train[3]
	print("predicting")
	model.fit(X, (mm.dm.dataY_train - mm.dm.dataX_train[1]) / mm.dm.dataX_train[2])
	
	# transform categorical features to onehot encoding
	#X = np.zeros((len(mm.dm.dataX_train[0]),len(mm.dm.edge_vocab_map)), dtype=int)
	#X = np.ones((len(mm.dm.dataX_train[0]),1), dtype=int)
	
	"""for i in range(len(mm.dm.edge_vocab_map)):
		mask = mm.dm.dataX_train[0] == i
		
		X[mask,i] = 1"""
	
	#print(np.reshape(np.average(mm.dm.dataX_train[3],axis=1),[-1,1]))
	

	#model.fit(X*np.reshape(mm.dm.dataX_train[2],[-1,1])+np.reshape(mm.dm.dataX_train[1],[-1,1]), (mm.dm.dataY_train))
	
	#X = np.zeros((len(mm.dm.dataX_test[0]),len(mm.dm.edge_vocab_map)), dtype=int)
	#X = np.ones((len(mm.dm.dataX_test[0]),1), dtype=int)
	
	"""for i in range(len(mm.dm.edge_vocab_map)):
		mask = mm.dm.dataX_test[0] == i
		
		X[mask,i] = 1"""
	
	X = mm.dm.dataX_test[3]
	yPred = model.predict(X)

	print((yPred* mm.dm.dataX_test[2])+mm.dm.dataX_test[1] - mm.dm.dataY_test)

	print("lasso MAE: " + str(mean_absolute_error((yPred* mm.dm.dataX_test[2])+mm.dm.dataX_test[1],mm.dm.dataY_test)))
	print("lasso MSE: " + str(mean_squared_error((yPred* mm.dm.dataX_test[2])+mm.dm.dataX_test[1],mm.dm.dataY_test)))
	
	#print("linear regression MAE: " + str(mean_absolute_error(yPred,mm.dm.dataY_test )))
	#print("linear regression MSE: " + str(mean_squared_error(yPred,mm.dm.dataY_test)))
	print(model.coef_)
	print(model.intercept_)
	
	sm = swiss_map.SwissMap(args,mm.dm)
	sm.save_softmax_map(model.coef_, args.egde_to_observe)

elif args.mode == "MLP":
	dm = DataManager(args,useGenerators=False)
	
	ve_b, sig_b, te_b = du.train_validate_test(args, dm,FeedForwardNN(args,[len(dm.lt_vocab_map)],2),dataformat="feedforwardNN_with_embedding")
	#ve_b, sig_b, te_b = du.train_validate_test(args,dm,RandomForestRegressor(n_estimators=50, criterion='mse', random_state=args.random_seed), dataformat="onlyTrainInformation", isKerasModel = False)

	print(ve_b, sig_b, te_b)

elif args.mode == "graph_net":
	import my_graph_nets
	
	my_graph_nets.train_generator(args, mm, mm.dm.generators_dataX_dataY_precomputed(mm.dm.dataX_train,mm.dm.dataY_train), mm.dm.generators_dataX_dataY_precomputed(mm.dm.dataX_test,mm.dm.dataY_test))
	
	#graph_data = my_graph_nets.generate_networkx_graphs(mm, mm.dm.dataX_train, mm.dm.dataY_train)
	
	#G = my_graph_nets.graph_to_input_target(G)
elif args.mode == "RF":
	from sklearn.ensemble import RandomForestRegressor
	
	RF.train_validate_test(RandomForestRegressor(criterion='mse', random_state=1), {'n_estimators':[256], 'max_depth':[256]}, mm,mm.dm.dataset_train, mm.dm.dataset_test)
	
elif args.mode == "plotting":
	import plotting
	
	plotting.plot_all_delays_over_time(dm.dataset_test, "data/plot.png")
	
elif args.mode == "experiment":
	import experiments as expe
	
	lines = [7,113, 35, 142]
	
	if args.experiment_num == 1:
		expe.experiment1(args,lines)
	elif args.experiment_num == 2:
		expe.experiment2(args,lines)
	elif args.experiment_num == 3:
		expe.experiment3(args,lines)
	elif args.experiment_num == 4:
		expe.experiment4(args,lines)
	elif args.experiment_num == 5:
		expe.experiment5(args,lines)
	elif args.experiment_num == 6:
		expe.experiment6(args,lines)
	elif args.experiment_num == 7:
		expe.experiment7(args,lines)
	elif args.experiment_num == 8:
		expe.experiment8(args,lines)
	elif args.experiment_num == 9:
		expe.experiment9(args,lines)
	elif args.experiment_num == 10:
		expe.experiment10(args,lines)
	elif args.experiment_num == 11:
		expe.experiment11(args,lines)	
	elif args.experiment_num == 12:
		expe.experiment12(args,lines)	
	elif args.experiment_num == 13:
		expe.experiment13(args,lines)	
