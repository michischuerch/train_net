import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import sys
import data_utils


def plot_all_delays_over_time(data, outpath):
	
	plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%a'))
	plt.gca().xaxis.set_major_locator(mdates.DayLocator())	
	
	data['ACTUAL_TIME'] = data['ACTUAL_TIME'].map(lambda x: x.replace(second=0))
	data['ACTUAL_TIME'] = data['ACTUAL_TIME'].map(lambda x: x.replace(minute=0))
	print(data['ACTUAL_TIME'])
	amount_per_timeslot = data.groupby(['ACTUAL_TIME']).size().to_dict()
	
	#x = data['ACTUAL_TIME']
	#y = data['running_time']
	
	print("start plotting...")
	
	plt.plot(amount_per_timeslot.keys(),amount_per_timeslot.values(), 'b',markersize=0.1,linewidth=0.5)
	
	plt.ylabel('Amount')
	plt.xlabel('Weekday')
	
	plt.title('30.07.18 - 05.08.18')
	
	plt.show()
	plt.savefig(outpath) 	
	
