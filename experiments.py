import swiss_map
from collections import OrderedDict
import numpy as np
from model import TrainNet
from DataManager import DataManager
import sys
from sklearn.metrics import mean_absolute_error, mean_squared_error
import matplotlib.pyplot as plt
plt.switch_backend('agg')
import matplotlib.dates as mdates
import pandas as pd
import losses
import os
from keras import backend as K
from sklearn.linear_model import LinearRegression, Ridge
from my_graph_nets import GraphNet
import time
from sklearn.ensemble import RandomForestRegressor
import preprocessing
import data_utils as du 
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import Kernel, CompoundKernel, ExpSineSquared, RBF, Product
from models.Baseline1 import Baseline1,Lasso_standardized, Ridge_standardized
import matplotlib.pyplot as plt
from models.FeedForward import FeedForwardNN
from preprocessing import prepare_X_y
import pickle
from pylab import *
import swiss_map
from sklearn.linear_model import Lasso
from matplotlib import rc

"""def experiment0(args, lines):
	
	for line in lines:
		
		args.only_top_k = line
		
		dm = DataManager(args,useGenerators=False)
		
		val_error_delay = []
		sigma_delay = []
		test_error_delay = []
		estimators = range(1,100,10)
		
		for e_count in estimators:
			ve, sig, te = du.train_validate_test(args,dm,RandomForestRegressor(n_estimators=e_count, criterion='mse', random_state=1), dm.dataset_test['next_delay'])
			val_error_delay.append(ve)
			sigma_delay.append(sig)
			test_error_delay.append(te)
		
		
		
		#plt.errorbar(estimators, val_error, yerr=2*np.array(sigma), fmt='o', linewidth=0.5, markersize=3, capsize=3)
		#plt.plot(estimators,test_error, 'turquoise',linewidth=0.5,label='Testing pred. delay')
		
		val_error = []
		sigma = []
		test_error = []
		
		for e_count in estimators:
			ve, sig, te = du.train_validate_test(args,dm,RandomForestRegressor(n_estimators=e_count, criterion='mse', random_state=1), X_train_0, y_train_0, X_test_0, y_test_0)
			val_error.append(ve)
			sigma.append(sig)
			test_error.append(te)
		
		plt.plot(estimators,val_error_delay, 'b',linewidth=0.5,label='predict arrival delay')
		plt.plot(estimators,val_error, 'r',linewidth=0.5,label='predict running time')
		
		#plt.errorbar(estimators, val_error, yerr=2*np.array(sigma), fmt='o', linewidth=0.5, markersize=3, capsize=3)
		#
		
		plt.legend()
		plt.title(dm.edge_name + " (Validation)")
		
		plt.ylabel('Sum of losses')
		plt.xlabel('Estimators')
		
		plt.show()
		plt.savefig("data/e0_RF_estimator"+str(args.only_top_k)+".png") 	
		
		plt.clf()
		
		plt.plot(estimators,test_error_delay, 'b',linewidth=0.5,label='predict arrival delay')
		plt.plot(estimators,test_error, 'r',linewidth=0.5,label='predict running time')
		
		plt.legend()
		plt.title(dm.edge_name + " (Testing)")
		
		plt.ylabel('Sum of losses')
		plt.xlabel('Estimators')
		
		plt.show()
		plt.savefig("data/e0_RF_estimator"+str(args.only_top_k)+"_test.png") 	"""

def experiment1(args,lines):
	
	colors = ['brown','b','g','orange']
	i = 0
	
	plt.title("(01.01.2018 - 31.05.2018)")
	
	f, axarr = plt.subplots(1, 1)
	
	args.only_top_k = lines[2]
	dm = DataManager(args,useGenerators=False)
	
	# display mean and mean srt
	mean_per_edge = pd.concat(dm.allData).groupby(['edge_id'])['running_time'].mean().to_dict()
	sigma_per_edge = pd.concat(dm.allData).groupby(['edge_id'])['running_time'].std().to_dict()
	
	mean_srt_per_edge = pd.concat(dm.allData).groupby(['edge_id'])['scheduled_running_time'].mean().to_dict()
	sigma_srt_per_edge = pd.concat(dm.allData).groupby(['edge_id'])['scheduled_running_time'].std().to_dict()
	
	lines = [lines[2]]
	
	for line in lines:
		args.only_top_k = line
		
		dm = DataManager(args,useGenerators=False)
		data = np.concatenate([y[1] for y in dm.allData_preprocessed])
		
		
		#plt.title("Zürich Oerlikon - Zürich HB (17.09.2018 - 30.09.2018) ")
		
		print(len(data))
		print(mean_per_edge[dm.top_k_edge])
		print(sigma_per_edge[dm.top_k_edge])
		
		
		plt.plot([mean_per_edge[dm.top_k_edge],mean_per_edge[dm.top_k_edge]], [0,1],linewidth=1.0, c='r')
		#plt.plot([mean_per_edge[dm.top_k_edge]-sigma_per_edge[dm.top_k_edge],mean_per_edge[dm.top_k_edge]+sigma_per_edge[dm.top_k_edge]], [0.006,0.006],linewidth=1.0, c='g')
		#plt.arrow(mean_per_edge[dm.top_k_edge]-sigma_per_edge[dm.top_k_edge], sigma_per_edge[dm.top_k_edge]*2, 0, head_width=0.05, head_length=0.1, arrowprops=dict(arrowstyle='-',color='g',), ec='k')
		#plt.arrow(mean_per_edge[dm.top_k_edge]+sigma_per_edge[dm.top_k_edge], sigma_per_edge[-dm.top_k_edge]*2, 0, head_width=0.05, head_length=0.1, arrowprops=dict(arrowstyle='-',color='g',), ec='k')
		plt.annotate(s='Mean', xy=(mean_per_edge[dm.top_k_edge]+20,0.002),bbox=dict(boxstyle='round,pad=0.2',fc='white', alpha=1),color='r')
		plt.annotate(s='', xy=(mean_per_edge[dm.top_k_edge]-sigma_per_edge[dm.top_k_edge],0.006),xytext=(mean_per_edge[dm.top_k_edge]+sigma_per_edge[dm.top_k_edge],0.006), arrowprops=dict(arrowstyle='<->',color='g'),color='g')
		plt.annotate(s='Standard deviation', xy=(mean_per_edge[dm.top_k_edge]+sigma_per_edge[dm.top_k_edge]*1.6,0.006),bbox=dict(boxstyle='round,pad=0.2',fc='white', alpha=1,lw=1),color='g')
		#plt.plot([mean_srt_per_edge[dm.top_k_edge],mean_srt_per_edge[dm.top_k_edge]],[0,1], c='b')
		
		
		#plt.gca().set_yscale("log")
		axarr.hist(data, 80, density=True, facecolor='orange', alpha=0.60,label=dm.edge_name) #bins=np.square(np.arange(0,80,0.3))
		#axarr.hist(pd.concat(dm.allData)['running_time'])
		axarr.set_xlim(0, 1500)
		axarr.set_ylim(0, 0.015)
		
		i += 1
	
	#f.title("(01.07.2018 - 30.09.2018)")	
	axarr.set_ylabel("Frequency")
	axarr.set_xlabel("Running time (seconds)")
	axarr.grid(True)
	#plt.savefig("data/e1_histogram_running_time.png") 
	#plt.clf()
	
	"""i = 0
	for line in lines:
		args.only_top_k = line
		
		dm = DataManager(args,useGenerators=False)
		data = pd.concat(dm.allData)
		
		
		#plt.title("Zürich Oerlikon - Zürich HB (17.09.2018 - 30.09.2018) ")
		
		#plt.gca().set_yscale("log")
		axarr[1].hist(pd.concat(dm.allData)['ARRIVAL_DELAY'], 50, density=True, facecolor=colors[i], alpha=0.60,label=dm.edge_name, histtype='stepfilled') #bins=np.square(np.arange(0,80,0.3))
		axarr[1].set_xlim(-1000, 1000)
		axarr[1].set_ylim(0, 0.025)
		i += 1"""
	
	plt.legend(bbox_to_anchor = (0.55, 1))
		
	#axarr[1].set_ylabel("Frequency")
	"""axarr[1].set_xlabel("Arrival delay")
	axarr[1].grid(True)"""
	#f.subplots_adjust(wspace=0.5)
	plt.savefig("data/e1_histogram.png") 
	plt.clf()

	

def experiment2(args,lines):
	
	f, axarr = plt.subplots(2, 2)
	f.subplots_adjust(hspace=0.45)
	f.subplots_adjust(wspace=0.3)
	
	subplot_array = [(0,0),(0,1),(1,0),(1,1)]
	current_subplot_i = 0
	
	edge_names = ["Zürich HB - Zürich Oerlikon", "Lenzburg - Aarau", "Winterthur - Zürich Flughafen", "Olten - Bern"]
	
	estimators = [5,10,15,20]
	colors = ['g', 'darkorange', 'rebeccapurple']
	
	for i,line in enumerate(lines):
		args.only_top_k = line
		
		print("Baseline1: \n")
		start_time = time.time()
		
		if os.path.isfile("data/results/e2_result"+str(line)+"B1.pkl"):
			with open("data/results/e2_result"+str(line)+"B1.pkl",'rb') as f:
				results = pickle.load(f)
				print(results)
				
		else:
			dm = DataManager(args,useGenerators=False)
			
			results  = du.train_validate_test(args,dm,Baseline1(), dataformat="baseline1", objective_function=mean_absolute_error, isKerasModel = False)
			print("time:" + str(time.time() - start_time))
			
			with open("data/results/e2_result"+str(line)+"B1.pkl",'wb') as f:
				pickle.dump(results, f)
			
		te_, tsig_, ve_, vsig_, teste_ = results
		
		l1 = axarr[subplot_array[current_subplot_i]].plot([-1000,1000], [ve_,ve_], 'r', linewidth=1.2, label='Baseline Historical Average')
		axarr[subplot_array[current_subplot_i]].set_title(edge_names[i])
		
		
		train_error = []
		train_sig = []
		val_error = []
		sigma = []
		test_error = []
		
		results = []
		
		if os.path.isfile("data/results/e2_result"+str(line)+".pkl"):
			with open("data/results/e2_result"+str(line)+".pkl",'rb') as f:
				a = pickle.load(f)
				train_error,train_sig,val_error,sigma,test_error = a
				print(a)
		else:
			dm = DataManager(args,useGenerators=False)
			
			for e_count in estimators:
				te_, tsig_, ve_, vsig_, teste_  = du.train_validate_test(args,dm,RandomForestRegressor(max_depth=e_count,n_estimators=100, criterion='mse', random_state=1), dataformat="onlyTrainInformation", objective_function=mean_absolute_error, isKerasModel = False)
				train_error.append(te_)
				train_sig.append(tsig_)
				val_error.append(ve_)
				sigma.append(vsig_)
				test_error.append(teste_)
				
			with open("data/results/e2_result"+str(line)+".pkl",'wb') as f:
				pickle.dump((train_error,train_sig,val_error,sigma,test_error), f)
		
		
		
		#l2 = axarr[subplot_array[current_subplot_i]].plot(estimators,train_error, 'b--',linewidth=0.5,label='Training Random Forest',color=colors[0])
		
		l2 = axarr[subplot_array[current_subplot_i]].plot(estimators,np.array(val_error), 'b--',linewidth=0.5,label='Validation Random Forest',color=colors[1])
		#l3 = axarr[subplot_array[current_subplot_i]].plot(estimators,test_error, 'b',linewidth=1,label='Testing Random Forest')
		
		
		l4 = axarr[subplot_array[current_subplot_i]].plot(estimators,np.array(train_error), 'b--',linewidth=0.5,label='Validation Random Forest',color=colors[2])
		
		axarr[subplot_array[current_subplot_i]].errorbar(estimators, np.array(val_error), yerr=2*np.array(sigma), fmt='o', linewidth=0.5, markersize=3, capsize=3,color=colors[1])
		axarr[subplot_array[current_subplot_i]].errorbar(estimators, np.array(train_error), yerr=2*np.array(train_sig), fmt='o', linewidth=0.5, markersize=3, capsize=3,color=colors[2])
		
		#axarr[subplot_array[current_subplot_i]].set_xlabel('Estimators')
		#axarr[subplot_array[current_subplot_i]].set_ylim(-500, 12500)
		axarr[subplot_array[current_subplot_i]].set_xlim(np.min(estimators) - 5, np.max(estimators) + 5)
		#axarr[subplot_array[current_subplot_i]].set_ylim(0, 100)
		#axarr[subplot_array[current_subplot_i]].set_yscale("log", nonposy='clip')
		#axarr[subplot_array[current_subplot_i]].yaxis.set_ticks(10**np.arange(1,5))
		#axarr[subplot_array[current_subplot_i]].grid(True)
		
		axarr[subplot_array[current_subplot_i]].grid()
		
		#plt.plot(estimators, te_b*np.ones(len(estimators)), 'r', linewidth=0.5, label='Baseline Historical Average')
		
		#plt.plot(estimators,val_error, 'b--',linewidth=0.5,label='Validation Random Forest')
		
		#plt.errorbar(estimators, val_error, yerr=2*np.array(sigma), fmt='o', linewidth=0.5, markersize=3, capsize=3)
		#plt.plot(estimators,test_error, 'b',linewidth=1,label='Testing Random Forest')
		#for cap in caps:
		#	cap.set_markeredgewidth(1)
		#plt.plot(estimators,np.ones(len(test_error))*ve_b, 'r',linewidth=0.5,label='Historical Average Validation')
		#plt.plot(estimators,np.ones(len(test_error))*te_b, 'r',linewidth=0.5,label='Historical Average Testing')
		current_subplot_i += 1
	print("printing")
	axarr[0,0].set_ylabel('Sum of Losses')
	axarr[1,0].set_ylabel('Sum of Losses')
	
	axarr[1,0].set_xlabel('Max Depth')
	axarr[1,1].set_xlabel('Max Depth')
		
	#plt.ylabel('MAE')
	#plt.xlabel('Estimators')

	
	lgd = plt.legend( ('Baseline Mean Per Edge','Validation Error Random Forest', 'Training Error Random Forest'), bbox_to_anchor = (0.9, -0.35), ncol=1 )
	
	#plt.title('Random Forest on different edges')
	#
	
	plt.show()
	plt.savefig("data/e2_RF_estimators.png",bbox_extra_artists=(lgd,),bbox_inches='tight') 
	plt.clf()	
		
		

def experiment3(args,lines):
	
	width = 0.15
	slack = 0.02
	
	all_feature_importance = []
	all_names = []
	
	for line in lines:
		args.only_top_k = line
		# train random forest and look at feature importance
		dm = DataManager(args,useGenerators=False)
		
		model = RandomForestRegressor(max_depth=20, n_estimators=100, criterion='mse', random_state=1)
		
		X_train, y_train, X_test, y_test,y_mean_train, y_mean_test, y_sigma_train, y_sigma_test = prepare_X_y(dm, pd.concat(dm.allData), dm.dataset_train, dm.dataset_test, dataformat="onlyTrainInformation",multi_input=False,predictionFormat="standardized")
		#X_train, y_train, X_val, y_val = prepare_X_y(dm.dataset_train, dm.dataset_test,dataformat="onlyTrainInformation")
		
		model.fit(X_train, y_train)
		
		all_feature_importance.append(model.feature_importances_)
		all_names.append(dm.edge_name)
	
	all_feature_importance = np.array(all_feature_importance)	
		
	fig, ax = plt.subplots()
	feature = ('Type', 'Weekday', 'Hour', 'Dep. delay', 'R.t. (schedule)')
	x_pos = np.arange(len(feature))
	ax.set_xticks(x_pos)
	ax.set_xticklabels(feature)
	#ax.invert_yaxis()  # labels read top-to-bottom
	ax.set_ylabel('Feature importance')
	ax.set_title("Max Depth = 20")
	
	
	colors = ['brown','b','g','orange']
	
	all_bars = []
	
	for i in range(len(all_feature_importance)):
		# Example data
		
		performance = all_feature_importance[i,:]
		#error = np.random.rand(len(feature))

		b = ax.bar(x_pos+(i-1.5)*(width+slack), performance,width,align='center',
				color=colors[i], ecolor='black')
		all_bars.append(b)	
	
	ax.legend( all_bars,  all_names)
		
	
	plt.show()
	plt.savefig("data/e3_feature_importance.png") 	

def experiment4(args,lines):
	
	f, axarr = plt.subplots(2, 2)
	f.subplots_adjust(hspace=0.45)
	f.subplots_adjust(wspace=0.3)
	
	subplot_array = [(0,0),(0,1),(1,0),(1,1)]
	current_subplot_i = 0
	
	edge_names = ["Zürich HB - Zürich Oerlikon", "Lenzburg - Aarau", "Winterthur - Zürich Flughafen", "Olten - Bern"]
	
	estimators = [3]
	colors = ['g', 'darkorange', 'rebeccapurple']
	
	for current_subplot_i,line in enumerate(lines):
		args.only_top_k = line
		
		legend_plots = []
		
		axarr[subplot_array[current_subplot_i]].grid()
		
		print("Baseline1: \n")
		start_time = time.time()
		
		if os.path.isfile("data/results/e6_result"+str(line)+"B1.pkl"):
			with open("data/results/e6_result"+str(line)+"B1.pkl",'rb') as f:
				results = pickle.load(f)
				print(results)
				
		else:
			dm = DataManager(args,useGenerators=False)
			
			results  = du.train_validate_test(args,dm,Baseline1(), dataformat="baseline1", objective_function=mean_absolute_error, isKerasModel = False)
			print("time:" + str(time.time() - start_time))
			
			with open("data/results/e6_result"+str(line)+"B1.pkl",'wb') as f:
				pickle.dump(results, f)
			
		te_, tsig_, ve_, vsig_, teste_ = results
		
		l1 = axarr[subplot_array[current_subplot_i]].plot([-1000,1000], [ve_,ve_], 'r', linewidth=1.2)
		axarr[subplot_array[current_subplot_i]].set_title(edge_names[current_subplot_i])
		legend_plots.append(l1[0])
		
		if os.path.isfile("data/results/e6_result"+str(line)+"RF.pkl"):
			with open("data/results/e6_result"+str(line)+"RF.pkl",'rb') as f:
				results = pickle.load(f)
				print(results)
				
		else:
			dm = DataManager(args,useGenerators=False)
			
			te_, tsig_, ve_, vsig_, teste_  = du.train_validate_test(args,dm,RandomForestRegressor(max_depth=5, n_estimators=e_count, criterion='mse', random_state=1), dataformat="onlyTrainInformation", objective_function=mean_absolute_error, isKerasModel = False)
			print("time:" + str(time.time() - start_time))
			
			with open("data/results/e6_result"+str(line)+"RF.pkl",'wb') as f:
				pickle.dump(results, f)
			
		te_, tsig_, ve_, vsig_, teste_ = results
		
		l1 = axarr[subplot_array[current_subplot_i]].plot([-1000,1000], [ve_,ve_], 'g', linewidth=1.2)
		legend_plots.append(l1[0])
		
		if os.path.isfile("data/results/e4_result"+str(line)+".pkl"):
			with open("data/results/e4_result"+str(line)+".pkl",'rb') as f:
				results = pickle.load(f)
		else:
			
			dm = DataManager(args,useGenerators=False)
			results = []
			
			inputLengths = [len(dm.edge_vocab_map),len(dm.graph_vocab), len(dm.lt_vocab_map) ]
			te_, tsig_, ve_, vsig_, teste_, history_val, history_test = du.train_validate_test(args, dm,FeedForwardNN(args,inputLengths,3),isKerasModel = True,multi_input = True, dataformat="allData",predictionFormat="standardized",has_attention=False)
				
			results.append((history_val,history_test))
				
			
			with open("data/results/e4_result"+str(line)+".pkl",'wb') as f:
				pickle.dump(results, f)
		

		for i,result in enumerate(results):
			history_val, history_test = result
			
			
			
			cutoff = 20
			
			train_e = np.mean([h['real_mean_absolute_error'] for h in history_val],axis=0)[0:cutoff]
			train_sig = np.std([h['real_mean_absolute_error'] for h in history_val],axis=0)[0:cutoff]
			ve = np.mean([h['real_val_mean_absolute_error'] for h in history_val],axis=0)[0:cutoff]
			sig = np.std([h['real_val_mean_absolute_error'] for h in history_val],axis=0)[0:cutoff]
			
			"""train_e = np.mean([h['real_mean_absolute_error'] for h in history_val],axis=0)[0:cutoff]
			train_sig = np.std([h['real_mean_absolute_error'] for h in history_val],axis=0)[0:cutoff]
			ve = np.mean([h['real_val_mean_absolute_error'] for h in history_val],axis=0)[0:cutoff]
			sig = np.std([h['real_val_mean_absolute_error'] for h in history_val],axis=0)[0:cutoff]"""
			
			ld = axarr[subplot_array[current_subplot_i]].plot(np.arange(len(ve))+1,ve,linewidth=0.5,color=colors[1])
			axarr[subplot_array[current_subplot_i]].errorbar(np.arange(len(ve))+1, ve, yerr=np.array(sig), fmt='o', linewidth=0.5, markersize=3, capsize=3,color=colors[1])
			legend_plots.append(ld[0])
			
			ld = axarr[subplot_array[current_subplot_i]].plot(np.arange(len(train_e))+1,train_e,linewidth=0.5,color=colors[2])
			axarr[subplot_array[current_subplot_i]].errorbar(np.arange(len(train_sig))+1, train_e, yerr=np.array(train_sig), fmt='o', linewidth=0.5, markersize=3, capsize=3,color=colors[2])
			legend_plots.append(ld[0])
			
			
			axarr[subplot_array[current_subplot_i]].set_xlim(0, cutoff+1)
	
	
	
	
	
	
	#axarr[1].grid()
	
	
	axarr[(0,0)].set_ylabel('Sum of losses')
	axarr[(1,0)].set_ylabel('Sum of losses')
	
	
	axarr[(1,0)].set_xlabel('Epochs')
	axarr[(1,1)].set_xlabel('Epochs')
	
	plt.subplots_adjust(hspace=0.45)
	plt.subplots_adjust(wspace=0.3)
	
	lgd = plt.legend( legend_plots, ('Baseline Mean Per Edge','Baseline Random Forest','Validation Error Feedforward NN + Dropout','Training Error Feedforward NN + Dropout'), bbox_to_anchor = (0.9, -0.26), ncol=1 )
	
	#fig = gcf()
	#fig.tight_layout()
	#plt.grid()
	
	print("aha")
	
	plt.show()
	plt.savefig("data/e4_ffnn.png",bbox_extra_artists=(lgd,),bbox_inches='tight') # bbox_extra_artists=(lgd,)
	
	plt.clf()
	
		
		
		


def experiment5(args,lines):
	# train feed forward neural network 
	
	f, axarr = plt.subplots(1,2)
	subplot_array = [0,1]
	current_subplot_i = 0
	
	
	layers = range(1,6)
	modes = ["values","standardized"]
	line = lines[1]
	
	
	args.lr = 0.001
	
	#args.num_epochs = 2
	
	
	
	args.only_top_k = line
	
	dm = DataManager(args,useGenerators=False)
	
	fig = gcf()
	fig.suptitle(dm.edge_name, fontsize=14)
	
	legend_plots = []
	
	te_, tsig_, ve_, vsig_, teste_ = du.train_validate_test(args,dm,Baseline1(), objective_function=mean_squared_error, dataformat="baseline1", isKerasModel = False)
	
	#axarr[subplot_array[current_subplot_i]].plot([0,6], [ve_b,ve_b], 'r--', linewidth=0.5, label='Baseline Historical Average')
	axarr[0].plot([0,6], [teste_,teste_], 'r', linewidth=1, label='Baseline Historical Average')
	ld = axarr[1].plot([0,6], [teste_,teste_], 'r', linewidth=1, label='Baseline Historical Average')
	legend_plots.append(ld[0])
	
	te_, tsig_, ve_, vsig_, teste_ = du.train_validate_test(args,dm,RandomForestRegressor(max_depth=15, n_estimators=50, criterion='mse', random_state=args.random_seed), objective_function=mean_squared_error, dataformat="onlyTrainInformation", isKerasModel = False, predictionFormat="standardized")

	#axarr[subplot_array[current_subplot_i]].plot([1,7], [ve_b,ve_b], 'g--', linewidth=0.5)
	axarr[0].plot([0,6], [teste_,teste_], 'g', linewidth=1)
	ld = axarr[1].plot([0,6], [teste_,teste_], 'g', linewidth=1)
	legend_plots.append(ld[0])
	
	colors = ['y', 'c', 'rebeccapurple']
	
	if os.path.isfile("data/results/e5_result"+str(line)+".pkl"):
		with open("data/results/e5_result"+str(line)+".pkl",'rb') as f:
			results = pickle.load(f)
	else:
		
		results = []
		
		for i,mode in enumerate(modes):

			training_error = []
			training_std = []
			validation_error = []
			validation_std = []
			
			
			for layer_count in layers:
				te_, tsig_, ve_, vsig_, teste_, history_val, history_test = du.train_validate_test(args, dm,FeedForwardNN(args,[len(dm.lt_vocab_map)],layer_count),isKerasModel = True,dataformat="feedforwardNN_with_embedding",predictionFormat=mode)
				
				training_error.append(te_)
				training_std.append(tsig_)
				validation_error.append(ve_)
				validation_std.append(vsig_)
				
			results.append((training_error, training_std, validation_error, validation_std))
			#results.append((val_error,sigma,test_error))
			
		
		with open("data/results/e5_result"+str(line)+".pkl",'wb') as f:
			pickle.dump(results, f)
	

	
	training_error, training_std, validation_error, validation_std = results[0]
	
	ld = axarr[0].plot(layers,training_error, 'b--',linewidth=0.5,color=colors[0])
	axarr[0].errorbar(layers, training_error, yerr=2*np.array(training_std), fmt='o', linewidth=0.5, markersize=3, capsize=3,color=colors[0])
	legend_plots.append(ld[0])
	ld = axarr[1].plot(layers,validation_error, 'b--',linewidth=0.5,color=colors[0])
	axarr[1].errorbar(layers, validation_error, yerr=2*np.array(validation_std), fmt='o', linewidth=0.5, markersize=3, capsize=3,color=colors[0])
	#axarr[1].plot(layers,test_error, 'b',linewidth=1,label='Testing Random Forest',color=colors[1])
	legend_plots.append(ld[0])
	
	
	training_error, training_std, validation_error, validation_std = results[1]
	
	axarr[0].plot(layers,training_error, 'b--',linewidth=0.5,color=colors[1])
	axarr[0].errorbar(layers, training_error, yerr=2*np.array(training_std), fmt='o', linewidth=0.5, markersize=3, capsize=3,color=colors[1])
	axarr[1].plot(layers,validation_error, 'b--',linewidth=0.5,color=colors[1])
	axarr[1].errorbar(layers, validation_error, yerr=2*np.array(validation_std), fmt='o', linewidth=0.5, markersize=3, capsize=3,color=colors[1])
	#axarr[0].plot(layers,test_error, 'b',linewidth=1,label='Testing Random Forest',color=colors[0])
	
	
	
	
	
	
	#l1 = axarr[subplot_array[current_subplot_i]].plot([-1000,1000], [te_b,te_b], 'r', linewidth=0.5, label='Baseline Historical Average')
	#axarr[subplot_array[current_subplot_i]].set_title(dm.edge_name)
	
	
	"""axarr[0].set_ylim(20, 60)
	axarr[0].set_xlim(0, 6)
	axarr[1].set_ylim(20, 60)
	axarr[1].set_xlim(0, 6)"""
	axarr[0].set_yscale("log", nonposy='clip')
	axarr[0].yaxis.set_ticks(10**np.arange(3,5))
	axarr[1].set_yscale("log", nonposy='clip')
	axarr[1].yaxis.set_ticks(10**np.arange(3,5))
	axarr[0].set_title("Training")
	axarr[1].set_title("Validation")
	
	current_subplot_i += 1
	
	
	axarr[0].set_ylabel('Sum of losses')
	#axarr[1].set_ylabel('MAE')
	
	axarr[0].set_xlabel('Layers')
	axarr[1].set_xlabel('Layers')
	
	plt.subplots_adjust(hspace=0.45)
	plt.subplots_adjust(wspace=0.3)
	
	lgd = plt.legend( legend_plots, ('Baseline Historical Average','Baseline Random Forest','MLP, normal output','MLP, standardized output'), bbox_to_anchor = (0.95, -0.2), ncol=1 )
	
	fig.tight_layout()
	#st.set_y(-1)
	fig.subplots_adjust(top=0.85)
	#plt.title('Random Forest on different edges')
	#axarr[0].grid()
	#axarr[1].grid()
	
	
	plt.show()
	plt.savefig("data/e5_FeedforwardNN.png",bbox_extra_artists=(lgd,),bbox_inches='tight') # bbox_extra_artists=(lgd,)
		

def experiment6(args, lines):
	# graph net
	
	f, axarr = plt.subplots(2, 2)
	subplot_array = [(0,0),(0,1),(1,0),(1,1)]
	
	#layers = range(1,6)
	edge_names = ["Zürich HB - Zürich Oerlikon", "Lenzburg - Aarau", "Winterthur - Zürich Flughafen", "Olten - Bern"]
	
	for current_subplot_i, line in enumerate(lines):
		
		args.only_top_k = line
		
		

		legend_plots = []

		print("Baseline1: \n")
		start_time = time.time()
		
		if os.path.isfile("data/results/e6_result"+str(line)+"B1.pkl"):
			with open("data/results/e6_result"+str(line)+"B1.pkl",'rb') as f:
				results = pickle.load(f)
				print(results)
				
		else:
			dm = DataManager(args,useGenerators=False)
			
			results  = du.train_validate_test(args,dm,Baseline1(), dataformat="baseline1", objective_function=mean_absolute_error, isKerasModel = False)
			print("time:" + str(time.time() - start_time))
			
			with open("data/results/e6_result"+str(line)+"B1.pkl",'wb') as f:
				pickle.dump(results, f)
		te_, tsig_, ve_, vsig_, teste_ = results

		l1 = axarr[subplot_array[current_subplot_i]].plot([-1000,1000], [ve_,ve_], 'r', linewidth=1.2, label='Baseline Historical Average')
		legend_plots.append(l1[0])
		#axarr[subplot_array[current_subplot_i]].set_title(edge_names[i])
		
		if os.path.isfile("data/results/e6_result"+str(line)+"RF.pkl"):
			with open("data/results/e6_result"+str(line)+"RF.pkl",'rb') as f:
				results = pickle.load(f)
		else:
			dm = DataManager(args,useGenerators=False)
			results = du.train_validate_test(args,dm,RandomForestRegressor(max_depth=5, n_estimators=50, criterion='mse', random_state=args.random_seed),objective_function=mean_absolute_error, dataformat="onlyTrainInformation", isKerasModel = False)
			with open("data/results/e6_result"+str(line)+"RF.pkl",'wb') as f:
				pickle.dump(results, f)
				
		te_, tsig_, ve_, vsig_, teste_ = results
		
		#axarr[subplot_array[current_subplot_i]].plot([0,args.num_epochs+1], [ve_,ve_], 'r', linewidth=1)
		ld = axarr[subplot_array[current_subplot_i]].plot([0,args.num_epochs+1], [ve_,ve_], 'g', linewidth=1.2)
		legend_plots.append(ld[0])
		
		axarr[subplot_array[current_subplot_i]].grid()
		
		colors = ['y', 'c', 'rebeccapurple']
		axarr[subplot_array[current_subplot_i]].set_title(edge_names[current_subplot_i])
		
		if os.path.isfile("data/results/e6_result"+str(line)+".pkl"):
			with open("data/results/e6_result"+str(line)+".pkl",'rb') as f:
				results = pickle.load(f)
		else:
			
			dm = DataManager(args,useGenerators=False)
			
			results = []
				
			history_val, history_test = du.train_validate_test(args, dm,GraphNet(args,dm), isKerasModel = True,multi_input = True, dataformat="allData",predictionFormat="standardized",useGenerators=True)
			#te_, tsig_, ve_, vsig_, teste_, history_val, history_test = du.train_validate_test(args, dm,GraphNet(args,dm),objective_function=mean_squared_error, isKerasModel = True,dataformat="onlyGraphInformation",predictionFormat="standardizedEverything",useGenerators=True)
				
			results.append((history_val,history_test))
				
			
			with open("data/results/e6_result"+str(line)+".pkl",'wb') as f:
				pickle.dump(results, f)
		

		for i,result in enumerate(results):
			history_val, history_test = result
			
			print(history_val)
			
			train_e = np.mean([h['mean_absolute_error'] for h in history_val],axis=0)
			train_sig = np.std([h['mean_absolute_error'] for h in history_val],axis=0)
			ve = np.mean([h['val_mean_absolute_error'] for h in history_val],axis=0)
			sig = np.std([h['val_mean_absolute_error'] for h in history_val],axis=0)
			
			ld = axarr[subplot_array[current_subplot_i]].plot(np.arange(len(ve))+1,ve,linewidth=0.5,color='darkorange')
			axarr[subplot_array[current_subplot_i]].errorbar(np.arange(len(ve))+1, ve, yerr=np.array(sig), fmt='o', linewidth=0.5, markersize=3, capsize=3,color='darkorange')
			legend_plots.append(ld[0])
			
			ld = axarr[subplot_array[current_subplot_i]].plot(np.arange(len(train_e))+1,train_e,linewidth=0.5,color='rebeccapurple')
			axarr[subplot_array[current_subplot_i]].errorbar(np.arange(len(train_e))+1, train_e, yerr=np.array(train_sig), fmt='o', linewidth=0.5, markersize=3, capsize=3,color='rebeccapurple')
			legend_plots.append(ld[0])
			
			
			
			"""for losses in history_val:
				
				ve = losses['val_loss']		
				ld = axarr[subplot_array[current_subplot_i]].plot(np.arange(len(ve))+1,ve,linewidth=0.5,color='c')
				#axarr[subplot_array[current_subplot_i]].errorbar(np.arange(len(ve))+1, ve, yerr=2*np.array(sig), fmt='o', linewidth=0.5, markersize=3, capsize=3,color=colors[i])
			
			legend_plots.append(ld[0])
			
			for losses in history_val:
				
				ve = losses['loss']
				ld = axarr[subplot_array[current_subplot_i]].plot(np.arange(len(ve))+1,ve,linewidth=0.5,color='y')
				#axarr[subplot_array[current_subplot_i]].errorbar(np.arange(len(ve))+1, ve, yerr=2*np.array(sig), fmt='o', linewidth=0.5, markersize=3, capsize=3,color=colors[i])
			
			legend_plots.append(ld[0])"""
				
			axarr[subplot_array[current_subplot_i]].set_xlim(0, args.num_epochs+1)
			#l1 = axarr[subplot_array[current_subplot_i]].plot([-1000,1000], [te_b,te_b], 'r', linewidth=0.5, label='Baseline Historical Average')
			#axarr[0].set_title(dm.edge_name)
			
			"""axarr[subplot_array[i]].set_yscale("log", nonposy='clip')
			axarr[subplot_array[i]].yaxis.set_ticks([1,10,100])
			axarr[subplot_array[i]].set_yscale("log", nonposy='clip')
			axarr[subplot_array[i]].yaxis.set_ticks([1,10,100])"""
	#axarr[0].set_title("Training")
	#axarr[1].set_title("Validation")
	
	#axarr[0].set_xlim(0, args.num_epochs)
	
	
	
	
	#axarr[1].grid()
	
	
	axarr[(0,0)].set_ylabel('Sum of losses')
	axarr[(1,0)].set_ylabel('Sum of losses')
	
	
	axarr[(1,0)].set_xlabel('Epochs')
	axarr[(1,1)].set_xlabel('Epochs')
	
	plt.subplots_adjust(hspace=0.45)
	plt.subplots_adjust(wspace=0.3)
	
	lgd = plt.legend( legend_plots, ('Baseline Historical Average','Baseline Random Forest','Validation Error Graph Net', 'Training Error Graph Net'), bbox_to_anchor = (0.9, -0.35), ncol=1 )
	
	#fig = gcf()
	#fig.tight_layout()
	#plt.grid()
	
	plt.show()
	plt.savefig("data/e6_graph_nets.png",bbox_extra_artists=(lgd,),bbox_inches='tight') # bbox_extra_artists=(lgd,)
		
def experiment7(args, lines):
	
	f, axarr = plt.subplots(2, 2)
	f.subplots_adjust(hspace=0.45)
	f.subplots_adjust(wspace=0.3)
	
	subplot_array = [(0,0),(0,1),(1,0),(1,1)]
	current_subplot_i = 0
	
	edge_names = ["Zürich HB - Zürich Oerlikon", "Lenzburg - Aarau", "Winterthur - Zürich Flughafen", "Olten - Bern"]
	
	estimators = [0.001,0.003,0.005,0.007,0.009]
	colors = ['g', 'darkorange', 'rebeccapurple']
	
	for i,line in enumerate(lines):
		args.only_top_k = line
		
		print("Baseline1: \n")
		start_time = time.time()
		
		if os.path.isfile("data/results/e2_result"+str(line)+"B1.pkl"):
			with open("data/results/e2_result"+str(line)+"B1.pkl",'rb') as f:
				results = pickle.load(f)
				print(results)
				
		else:
			dm = DataManager(args,useGenerators=False)
			
			results  = du.train_validate_test(args,dm,Baseline1(), dataformat="baseline1", objective_function=mean_absolute_error, isKerasModel = False)
			print("time:" + str(time.time() - start_time))
			
			with open("data/results/e2_result"+str(line)+"B1.pkl",'wb') as f:
				pickle.dump(results, f)
			
		te_, tsig_, ve_, vsig_, teste_ = results
		
		l1 = axarr[subplot_array[current_subplot_i]].plot([-1000,1000], [ve_,ve_], 'r', linewidth=1.2, label='Baseline Historical Average')
		axarr[subplot_array[current_subplot_i]].set_title(edge_names[i])
		
		
		if os.path.isfile("data/results/e6_result"+str(line)+"RF.pkl"):
			with open("data/results/e6_result"+str(line)+"RF.pkl",'rb') as f:
				results = pickle.load(f)
		else:
			dm = DataManager(args,useGenerators=False)
			results = du.train_validate_test(args,dm,RandomForestRegressor(max_depth=5, n_estimators=50, criterion='mse', random_state=args.random_seed),objective_function=mean_absolute_error, dataformat="onlyTrainInformation", isKerasModel = False)
			with open("data/results/e6_result"+str(line)+"RF.pkl",'wb') as f:
				pickle.dump(results, f)
				
		te_, tsig_, ve_, vsig_, teste_ = results
		
		#axarr[subplot_array[current_subplot_i]].plot([0,args.num_epochs+1], [ve_,ve_], 'r', linewidth=1)
		ld = axarr[subplot_array[current_subplot_i]].plot([0,args.num_epochs+1], [ve_,ve_], 'g', linewidth=1.2)
		#legend_plots.append(ld[0])
		
		train_error = []
		train_sig = []
		val_error = []
		sigma = []
		test_error = []
		
		results = []
		
		if os.path.isfile("data/results/e7_result"+str(line)+".pkl"):
			with open("data/results/e7_result"+str(line)+".pkl",'rb') as f:
				a = pickle.load(f)
				train_error,train_sig,val_error,sigma,test_error = a
				print(a)
		else:
			dm = DataManager(args,useGenerators=False)
			
			for e_count in estimators:
				te_, tsig_, ve_, vsig_, teste_  = du.train_validate_test(args,dm, Lasso(alpha=e_count,positive=True), dataformat="onlyGraphInformation", objective_function=mean_absolute_error, isKerasModel = False, predictionFormat="standardized")
				train_error.append(te_)
				train_sig.append(tsig_)
				val_error.append(ve_)
				sigma.append(vsig_)
				test_error.append(teste_)
				
			with open("data/results/e7_result"+str(line)+".pkl",'wb') as f:
				pickle.dump((train_error,train_sig,val_error,sigma,test_error), f)
		
		
		
		#l2 = axarr[subplot_array[current_subplot_i]].plot(estimators,train_error, 'b--',linewidth=0.5,label='Training Random Forest',color=colors[0])
		
		l2 = axarr[subplot_array[current_subplot_i]].plot(estimators,np.array(val_error), 'b--',linewidth=0.5,label='Validation Random Forest',color=colors[1])
		#l3 = axarr[subplot_array[current_subplot_i]].plot(estimators,test_error, 'b',linewidth=1,label='Testing Random Forest')
		
		
		l4 = axarr[subplot_array[current_subplot_i]].plot(estimators,np.array(train_error), 'b--',linewidth=0.5,label='Validation Random Forest',color=colors[2])
		
		axarr[subplot_array[current_subplot_i]].errorbar(estimators, np.array(val_error), yerr=np.array(sigma), fmt='o', linewidth=0.5, markersize=3, capsize=3,color=colors[1])
		axarr[subplot_array[current_subplot_i]].errorbar(estimators, np.array(train_error), yerr=np.array(train_sig), fmt='o', linewidth=0.5, markersize=3, capsize=3,color=colors[2])
		
		#axarr[subplot_array[current_subplot_i]].set_xlabel('Estimators')
		#axarr[subplot_array[current_subplot_i]].set_ylim(-500, 12500)
		axarr[subplot_array[current_subplot_i]].set_xlim(0, (np.max(estimators)+0.001))
		#axarr[subplot_array[current_subplot_i]].set_ylim(0, 100)
		#axarr[subplot_array[current_subplot_i]].set_yscale("log", nonposy='clip')
		#axarr[subplot_array[current_subplot_i]].yaxis.set_ticks(10**np.arange(1,5))
		#axarr[subplot_array[current_subplot_i]].grid(True)
		
		axarr[subplot_array[current_subplot_i]].grid()
		
		axarr[subplot_array[current_subplot_i]].ticklabel_format(axis="x",style='sci',scilimits=(0,1))
		#plt.plot(estimators, te_b*np.ones(len(estimators)), 'r', linewidth=0.5, label='Baseline Historical Average')
		
		#plt.plot(estimators,val_error, 'b--',linewidth=0.5,label='Validation Random Forest')
		
		#plt.errorbar(estimators, val_error, yerr=2*np.array(sigma), fmt='o', linewidth=0.5, markersize=3, capsize=3)
		#plt.plot(estimators,test_error, 'b',linewidth=1,label='Testing Random Forest')
		#for cap in caps:
		#	cap.set_markeredgewidth(1)
		#plt.plot(estimators,np.ones(len(test_error))*ve_b, 'r',linewidth=0.5,label='Historical Average Validation')
		#plt.plot(estimators,np.ones(len(test_error))*te_b, 'r',linewidth=0.5,label='Historical Average Testing')
		current_subplot_i += 1
	print("printing")
	axarr[0,0].set_ylabel('Sum of Losses')
	axarr[1,0].set_ylabel('Sum of Losses')
	
	axarr[1,0].set_xlabel(r'$\lambda$')
	axarr[1,1].set_xlabel(r'$\lambda$')
		
	#plt.ylabel('MAE')
	#plt.xlabel('Estimators')

	
	lgd = plt.legend( ('Baseline Mean Per Edge','Baseline Random Forest','Validation Error LASSO', 'Training Error LASSO'), bbox_to_anchor = (0.9, -0.35), ncol=1 )
	
	#plt.title('Random Forest on different edges')
	#
	
	plt.show()
	plt.savefig("data/e7_LASSO_gamma.png",bbox_extra_artists=(lgd,),bbox_inches='tight') 
	plt.clf()	
		
		
		
def experiment8(args, lines):
	# attention model
	
	f, axarr = plt.subplots(2, 2)
	subplot_array = [(0,0),(0,1),(1,0),(1,1)]
	
	#layers = range(1,6)
	
	
	
	#args.num_epochs = 2
	
	#for line in lines:
	args.num_epochs = 20
	args.batch_size = 1
	
	learning_rates = [0.001]
	
	
	for current_subplot_i, line in enumerate(lines):
		
		args.only_top_k = line
		
		dm = DataManager(args,useGenerators=False)

		legend_plots = []

		te_, tsig_, ve_, vsig_, teste_ = du.train_validate_test(args,dm,Baseline1(), objective_function=mean_squared_error, dataformat="baseline1", isKerasModel = False,predictionFormat="standardizedEverything")

		#axarr[subplot_array[current_subplot_i]].plot([0,6], [ve_b,ve_b], 'r--', linewidth=0.5, label='Baseline Historical Average')
		axarr[subplot_array[current_subplot_i]].plot([0,args.num_epochs+1], [teste_,teste_], 'r', linewidth=1, label='Baseline Historical Average')
		ld = axarr[subplot_array[current_subplot_i]].plot([0,args.num_epochs+1], [teste_,teste_], 'r', linewidth=1, label='Baseline Historical Average')
		legend_plots.append(ld[0])

		te_, tsig_, ve_, vsig_, teste_ = du.train_validate_test(args,dm,RandomForestRegressor(n_estimators=50, criterion='mse', random_state=args.random_seed), objective_function=mean_squared_error, dataformat="onlyTrainInformation", isKerasModel = False,predictionFormat="standardizedEverything")

		axarr[subplot_array[current_subplot_i]].plot([0,args.num_epochs+1], [teste_,teste_], 'g', linewidth=1)
		ld = axarr[subplot_array[current_subplot_i]].plot([0,args.num_epochs+1], [teste_,teste_], 'g', linewidth=1)
		legend_plots.append(ld[0])
		
		axarr[subplot_array[current_subplot_i]].grid()
		
		colors = ['y', 'c', 'rebeccapurple']
		axarr[subplot_array[current_subplot_i]].set_title(dm.edge_name)
		
		if os.path.isfile("data/results/e8_result"+str(line)+".pkl"):
			with open("data/results/e8_result"+str(line)+".pkl",'rb') as f:
				results = pickle.load(f)
		else:
			
			results = []
			
			for i,lr in enumerate(learning_rates):
				args.lr = lr
				
				inputLengths = [len(dm.edge_vocab_map),len(dm.graph_vocab), len(dm.lt_vocab_map) ]
				te_, tsig_, ve_, vsig_, teste_, history_val, history_test = du.train_validate_test(args, dm,TrainNet(args,inputLengths,train_agnostic=True),objective_function=mean_squared_error, isKerasModel = True,dataformat="onlyGraphInformation",predictionFormat="standardizedEverything")
				
				results.append((history_val,history_test))
				
			
			with open("data/results/e8_result"+str(line)+".pkl",'wb') as f:
				pickle.dump(results, f)
		

		for i,result in enumerate(results):
			history_val, history_test = result
			
			train_e = np.mean([h['loss'] for h in history_val],axis=0)
			train_sig = np.std([h['loss'] for h in history_val],axis=0)
			ve = np.mean([h['val_loss'] for h in history_val],axis=0)
			sig = np.std([h['val_loss'] for h in history_val],axis=0)
			
			
			"""ld = axarr[0].plot(np.arange(len(train_e))+1,train_e,linewidth=0.5,color=colors[i])
			axarr[0].errorbar(np.arange(len(train_e))+1, train_e, yerr=2*np.array(train_sig), fmt='o', linewidth=0.5, markersize=3, capsize=3,color=colors[i])
			legend_plots.append(ld[0])"""
			ld = axarr[subplot_array[current_subplot_i]].plot(np.arange(len(train_e))+1,ve,linewidth=0.5,color=colors[i])
			axarr[subplot_array[current_subplot_i]].errorbar(np.arange(len(train_e))+1, ve, yerr=np.array(sig), fmt='o', linewidth=0.5, markersize=3, capsize=3,color=colors[i])
			legend_plots.append(ld[0])
		
			#l1 = axarr[subplot_array[current_subplot_i]].plot([-1000,1000], [te_b,te_b], 'r', linewidth=0.5, label='Baseline Historical Average')
			#axarr[0].set_title(dm.edge_name)
			
			"""axarr[subplot_array[i]].set_yscale("log", nonposy='clip')
			axarr[subplot_array[i]].yaxis.set_ticks([1,10,100])
			axarr[subplot_array[i]].set_yscale("log", nonposy='clip')
			axarr[subplot_array[i]].yaxis.set_ticks([1,10,100])"""
	#axarr[0].set_title("Training")
	#axarr[1].set_title("Validation")
	
	#axarr[0].set_xlim(0, args.num_epochs)
	
	
	
	
	#axarr[1].grid()
	
	
	axarr[(0,0)].set_ylabel('Sum of losses')
	axarr[(1,0)].set_ylabel('Sum of losses')
	
	
	axarr[(1,0)].set_xlabel('Epochs')
	axarr[(1,1)].set_xlabel('Epochs')
	
	plt.subplots_adjust(hspace=0.45)
	plt.subplots_adjust(wspace=0.3)
	
	lgd = plt.legend( legend_plots, ('Baseline Historical Average','Baseline Random Forest','Validation Attention-Module'), bbox_to_anchor = (0.9, -0.13), ncol=1 )
	
	#fig = gcf()
	#fig.tight_layout()
	#plt.grid()
	
	plt.show()
	plt.savefig("data/e8_attention_module.png",bbox_extra_artists=(lgd,),bbox_inches='tight') # bbox_extra_artists=(lgd,)
		
		
		

def experiment9(args, lines):
	
	sm = swiss_map.SwissMap(args)
	
	colors = ['r','b','lime','orange']
	
	for i,line in enumerate(lines):
		args.only_top_k = line
		
		dm = DataManager(args,useGenerators=False)
		
		#edge_id = args.egde_to_observe #mm.dm.dataX_test[0][i]
		
		#plt.title("Observed Edges")
		
		s,t = dm.reverseMapEdge[dm.top_k_edge]
			
		name1 = dm.bpuic_to_name_map[str(s)]
		name2 = dm.bpuic_to_name_map[str(t)]
		
		sm.print_edge(name1, name2, colors[i], 1,linewidth=1.5,label=dm.edge_name)
		
		"""if i < 10:
			sm.save_softmax_map(dm,softmax[0], dm.top_k_edge, "data/e11_softmax_trainnet0" + str(i) + ".png")
		else:
			sm.save_softmax_map(dm,softmax[0], dm.top_k_edge, "data/e11_softmax_trainnet" + str(i) + ".png")"""
	
	plt.legend()
	
	plt.axis('off')
	
	plt.show()
	plt.savefig("data/e9_observe_edges.png",bbox_inches='tight') # bbox_extra_artists=(lgd,)

def experiment10(args,lines):
	# attention model with train information
	
	f, axarr = plt.subplots(2, 2)
	subplot_array = [(0,0),(0,1),(1,0),(1,1)]
	
	edge_names = ["Zürich HB - Zürich Oerlikon", "Lenzburg - Aarau", "Winterthur - Zürich Flughafen", "Olten - Bern"]
	
	#layers = range(1,6)
	
	#args.num_epochs = 2
	
	#for line in lines:
	#args.batch_size = 1
	
	#lines=[lines[1]]
	#lines = [lines[0]]
	
	for current_subplot_i, line in enumerate(lines):
		
		
		
		args.only_top_k = line
		legend_plots = []
		colors = ['b', 'darkorange', 'rebeccapurple']
		
		if os.path.isfile("data/results/e2_result"+str(line)+"B1.pkl"):
			with open("data/results/e2_result"+str(line)+"B1.pkl",'rb') as f:
				results = pickle.load(f)
				print(results)
				
		else:
			dm = DataManager(args,useGenerators=False)
			
			results  = du.train_validate_test(args,dm,Baseline1(), dataformat="baseline1", objective_function=mean_absolute_error, isKerasModel = False)
			print("time:" + str(time.time() - start_time))
			
			with open("data/results/e2_result"+str(line)+"B1.pkl",'wb') as f:
				pickle.dump(results, f)
			
		te_, tsig_, ve_, vsig_, teste_ = results
		
		l1 = axarr[subplot_array[current_subplot_i]].plot([-1000,1000], [ve_,ve_], 'r', linewidth=1.2, label='Baseline Historical Average')
		#axarr[subplot_array[current_subplot_i]].set_title(edge_names[current_subplot_i])
		legend_plots.append(l1[0])
		
		"""

		te_, tsig_, ve_, vsig_, teste_ = du.train_validate_test(args,dm,Baseline1(), dataformat="baseline1", isKerasModel = False,objective_function=mean_absolute_error)

		#axarr[subplot_array[current_subplot_i]].plot([0,6], [ve_b,ve_b], 'r--', linewidth=0.5, label='Baseline Historical Average')
		#axarr[subplot_array[current_subplot_i]].plot([0,args.num_epochs+1], [ve_,ve_], 'orange', linewidth=1, label='Baseline Historical Average')
		ld = axarr[subplot_array[current_subplot_i]].plot([0,args.num_epochs+1], [ve_,ve_], color='r', linewidth=1, label='Baseline Historical Average')
		legend_plots.append(ld[0])"""
		
		if os.path.isfile("data/results/e10_result"+str(line)+"RF.pkl"):
			with open("data/results/e10_result"+str(line)+"RF.pkl",'rb') as f:
				results = pickle.load(f)
		else:
			dm = DataManager(args,useGenerators=False)
			results = du.train_validate_test(args,dm,RandomForestRegressor(max_depth=5, n_estimators=50, criterion='mse', random_state=args.random_seed),objective_function=mean_absolute_error, dataformat="onlyTrainInformation", isKerasModel = False)
			with open("data/results/e10_result"+str(line)+"RF.pkl",'wb') as f:
				pickle.dump(results, f)
				
		te_, tsig_, ve_, vsig_, teste_ = results
		
		#axarr[subplot_array[current_subplot_i]].plot([0,args.num_epochs+1], [ve_,ve_], 'r', linewidth=1)
		ld = axarr[subplot_array[current_subplot_i]].plot([0,args.num_epochs+1], [ve_,ve_], 'g', linewidth=1.2)
		legend_plots.append(ld[0])
		
		axarr[subplot_array[current_subplot_i]].grid()
		
		
		axarr[subplot_array[current_subplot_i]].set_title(edge_names[current_subplot_i])
		
		
		
		if os.path.isfile("data/results/e10_result"+str(line)+".pkl"):
			with open("data/results/e10_result"+str(line)+".pkl",'rb') as f:
				results = pickle.load(f)
		else:
			
			dm = DataManager(args,useGenerators=False)
			results = []
			
			inputLengths = [len(dm.edge_vocab_map),len(dm.graph_vocab), len(dm.lt_vocab_map) ]
			te_, tsig_, ve_, vsig_, teste_, history_val, history_test = du.train_validate_test(args, dm,TrainNet(args,inputLengths,train_agnostic=False), isKerasModel = True,multi_input = True, dataformat="allData", predictionFormat="standardized")
			
			results.append((history_val,history_test))
				
			
			with open("data/results/e10_result"+str(line)+".pkl",'wb') as f:
				pickle.dump(results, f)
		

		for i,result in enumerate(results):
			history_val, history_test = result
			
			
			
			cutoff = 20
			
			train_e = np.mean([h['real_mean_absolute_error'] for h in history_val],axis=0)[0:cutoff]
			train_sig = np.std([h['real_mean_absolute_error'] for h in history_val],axis=0)[0:cutoff]
			ve = np.mean([h['real_val_mean_absolute_error'] for h in history_val],axis=0)[0:cutoff]
			sig = np.std([h['real_val_mean_absolute_error'] for h in history_val],axis=0)[0:cutoff]
			
			print(train_e[-1],train_sig[-1],ve[-1],sig[-1],history_test['real_mean_absolute_error'][-1])
			
			
			if current_subplot_i == 1:
				axarr[subplot_array[current_subplot_i]].set_ylim(16, 241)
			
			"""train_e = np.mean([h['real_mean_absolute_error'] for h in history_val],axis=0)[0:cutoff]
			train_sig = np.std([h['real_mean_absolute_error'] for h in history_val],axis=0)[0:cutoff]
			ve = np.mean([h['real_val_mean_absolute_error'] for h in history_val],axis=0)[0:cutoff]
			sig = np.std([h['real_val_mean_absolute_error'] for h in history_val],axis=0)[0:cutoff]"""
			
			ld = axarr[subplot_array[current_subplot_i]].plot(np.arange(len(ve))+1,ve,linewidth=0.5,color=colors[1])
			axarr[subplot_array[current_subplot_i]].errorbar(np.arange(len(ve))+1, ve, yerr=np.array(sig), fmt='o', linewidth=0.5, markersize=3, capsize=3,color=colors[1])
			legend_plots.append(ld[0])
			
			ld = axarr[subplot_array[current_subplot_i]].plot(np.arange(len(train_e))+1,train_e,linewidth=0.5,color=colors[2])
			axarr[subplot_array[current_subplot_i]].errorbar(np.arange(len(train_sig))+1, train_e, yerr=np.array(train_sig), fmt='o', linewidth=0.5, markersize=3, capsize=3,color=colors[2])
			legend_plots.append(ld[0])
			
			
			axarr[subplot_array[current_subplot_i]].set_xlim(0, cutoff+1)
	
	
	
	
	
	
	#axarr[1].grid()
	
	
	axarr[(0,0)].set_ylabel('Sum of losses')
	axarr[(1,0)].set_ylabel('Sum of losses')
	
	
	axarr[(1,0)].set_xlabel('Epochs')
	axarr[(1,1)].set_xlabel('Epochs')
	
	plt.subplots_adjust(hspace=0.45)
	plt.subplots_adjust(wspace=0.3)
	
	lgd = plt.legend( legend_plots, ('Baseline Mean Per Edge','Baseline Random Forest','Validation Error Attention-model','Training Error Attention-model'), bbox_to_anchor = (0.9, -0.26), ncol=1 )
	
	#fig = gcf()
	#fig.tight_layout()
	#plt.grid()
	
	print("aha")
	
	plt.show()
	plt.savefig("data/e10_attention_module.png",bbox_extra_artists=(lgd,),bbox_inches='tight') # bbox_extra_artists=(lgd,)
	
	plt.clf()
	
	with open("data/results/e10_result142.pkl",'rb') as f:
		results = pickle.load(f)
	
	f, axarr = plt.subplots(1)
	
	# show baseline error
	
	legend_plots = []
	colors = ['b', 'c', 'rebeccapurple']

	
	with open("data/results/e10_result142RF.pkl",'rb') as f:
		results_baseline = pickle.load(f)
			
	te_, tsig_, ve_, vsig_, teste_ = results_baseline
	
	cutoff = 15
	
	ld = axarr.plot([0,cutoff+1], [ve_,ve_], 'g', linewidth=1)
	legend_plots.append(ld[0])
	
	for i,result in enumerate(results):
		history_val, history_test = result
		
		
		
		train_e = np.mean([h['loss'] for h in history_val],axis=0)[0:cutoff]
		train_sig = np.std([h['loss'] for h in history_val],axis=0)[0:cutoff]
		ve = np.mean([h['val_loss'] for h in history_val],axis=0)[0:cutoff]
		sig = np.std([h['val_loss'] for h in history_val],axis=0)[0:cutoff]
		
		"""train_e = np.mean([h['real_mean_absolute_error'] for h in history_val],axis=0)[0:cutoff]
		train_sig = np.std([h['real_mean_absolute_error'] for h in history_val],axis=0)[0:cutoff]
		ve = np.mean([h['real_val_mean_absolute_error'] for h in history_val],axis=0)[0:cutoff]
		sig = np.std([h['real_val_mean_absolute_error'] for h in history_val],axis=0)[0:cutoff]"""
		
		
		
		ld = axarr.plot(np.arange(len(ve))+1,ve,linewidth=0.5,color=colors[i])
		axarr.errorbar(np.arange(len(ve))+1, ve, yerr=np.array(sig), fmt='o', linewidth=0.5, markersize=3, capsize=3,color=colors[i])
		legend_plots.append(ld[0])
		"""ld = axarr[subplot_array[current_subplot_i]].plot(np.arange(len(train_e))+1,train_e,linewidth=0.5,color='g')
		axarr[subplot_array[current_subplot_i]].errorbar(np.arange(len(train_sig))+1, ve, yerr=np.array(train_sig), fmt='o', linewidth=0.5, markersize=3, capsize=3,color='g')
		legend_plots.append(ld[0])"""
		
		
		axarr.set_xlim(0, cutoff+1)
	
	axarr.set_ylabel('Sum of losses')
	axarr.set_xlabel('Epochs')
	
	lgd = plt.legend( legend_plots, ('Baseline Random Forest','Validation Attention-model'), bbox_to_anchor = (0.9, -0.1), ncol=1 )
	
	axarr.set_title("Olten - Bern")
	
	plt.show()
	plt.savefig("data/e10_attention_module_2.png",bbox_extra_artists=(lgd,),bbox_inches='tight') # bbox_extra_artists=(lgd,)
	

def experiment11(args,lines):
	# show softmax
	
	args.only_top_k = lines[1]
		
	dm = DataManager(args,useGenerators=False)
	
	"""X_train, y_train, X_test, y_test,y_mean_train, y_mean_test, y_sigma_train, y_sigma_test = prepare_X_y(dm, pd.concat(dm.allData), dm.dataset_train, dm.dataset_test, dataformat="onlyGraphInformation",multi_input=False,predictionFormat="standardized")
	
	model = Lasso(alpha=0.001,positive=True)
	model.fit(X_train,y_train)
	
	print("num non-zero")
	print(np.count_nonzero(model.coef_))
	
	
	sm = swiss_map.SwissMap(args)
	plt.title(r'$\lambda = 0.001$ ')
	sm.save_softmax_map(dm,model.coef_, dm.top_k_edge, "data/e11_lasso" + str(args.only_top_k) + ".png")
	
	
	
	sys.exit()"""
	
	"""X_train, y_train, X_test, y_test, y_mean, y_sigma, y_mean_train, y_sigma_train = prepare_X_y(dm, pd.concat(dm.allData), dm.dataset_train, dm.dataset_test, dataformat="onlyGraphInformation",multi_input=False,predictionFormat="standardizedEverything")
	model.fit(X_train, y_train)"""
	
	
	"""print("Lasso: \n")
	start_time = time.time()
	te_, tsig_, ve_, vsig_, teste_ = du.train_validate_test(args, dm,model, isKerasModel = False,multi_input = False, dataformat="onlyGraphInformation",predictionFormat="standardizedEverything")
	print("time:" + str(time.time() - start_time))
	
	sm = swiss_map.SwissMap(args,dm)
	sm.save_softmax_map(model.coef_, dm.top_k_edge, "data/e11_softmax_lasso.png")"""
	
	#te_, tsig_, ve_, vsig_, teste_ = du.train_validate_test(args,dm,RandomForestRegressor(n_estimators=50, criterion='mse', random_state=args.random_seed), dataformat="onlyTrainInformation", isKerasModel = False)
	
	X_train, y_train, X_test, y_test,y_mean_train, y_mean_test, y_sigma_train, y_sigma_test = prepare_X_y(dm, pd.concat(dm.allData), dm.dataset_train, dm.dataset_test, dataformat="allData",multi_input=True,predictionFormat="standardized")
	
	print(y_mean_train)
	print(y_sigma_train)
	
	
	inputLengths = [len(dm.edge_vocab_map),len(dm.graph_vocab), len(dm.lt_vocab_map) ]
	model = TrainNet(args,inputLengths,train_agnostic=False)
	
	saveEpochs = args.num_epochs
	args.num_epochs = 1
	for i in range(saveEpochs):
		history = model.fit(X_train, y_train, X_test, y_test, verbose=0).history
		softmax = model.predict_softmax(X_test)
		softmax2 = model.predict_softmax2(X_test)
		
		print(np.max(softmax),np.min(softmax))
		print(np.max(softmax2),np.min(softmax2))
		
		
		y_pred = model.predict(X_test)
		y_pred = np.add(np.multiply(y_pred,y_sigma_test),y_mean_test)
		y_test_ = np.add(np.multiply(y_test,y_sigma_test),y_mean_test)
		
		print(np.max(y_train), np.max(y_test))
		print(np.mean(y_pred),np.std(y_pred),np.min(y_pred), np.max(y_pred))
		print("test error")
		print(mean_squared_error(y_pred, y_test_))
		print(mean_absolute_error(y_pred, y_test_))
		
		y_pred = model.predict(X_train)
		y_pred = np.add(np.multiply(y_pred,y_sigma_train),y_mean_train)
		y_train_ = np.add(np.multiply(y_train,y_sigma_train),y_mean_train)
		print(np.mean(y_pred),np.std(y_pred),np.min(y_pred), np.max(y_pred))
		
		print("train error")
		print(mean_squared_error(y_pred, y_train_))
		print(mean_absolute_error(y_pred, y_train_))
		
		sm = swiss_map.SwissMap(args)
		
		#edge_id = args.egde_to_observe #mm.dm.dataX_test[0][i]
		
		plt.title("Epoch " + str(i+1))
		
		if i < 10:
			sm.save_softmax_map(dm, softmax[0], dm.top_k_edge, "data/e11_softmax_trainnet0" + str(i) + ".png")
		else:
			sm.save_softmax_map(dm, softmax[0], dm.top_k_edge, "data/e11_softmax_trainnet" + str(i) + ".png")
		
		plt.clf()
		
def experiment12(args,lines):
	
	for current_subplot_i, line in enumerate(lines):
		args.only_top_k = line
		
		
	
	
	
	
	
def experiment13(args, lines):
	# training on whole graph, single edge evaluation
	
	for line in lines:
		args.only_top_k = line
		
		dm = DataManager(args,useGenerators=False)
		
def experimentAll(args):

	dm = DataManager(args,useGenerators=False)
	
	X_train_0,y_train_0 = preprocessing.extractFeatures(dm,dm.dataset_train)
	X_test_0,y_test_0 = preprocessing.extractFeatures(dm,dm.dataset_test)
	
	print("Baseline1: \n")
	start_time = time.time()
	scheduled_rt_train = (dm.dataset_train['next_SCHEDULED_TIME']-dm.dataset_train['SCHEDULED_TIME']).astype('timedelta64[s]').astype(int)
	scheduled_rt_test = (dm.dataset_test['next_SCHEDULED_TIME']-dm.dataset_test['SCHEDULED_TIME']).astype('timedelta64[s]').astype(int)
	
	du.train_validate_test(args,Baseline1(), scheduled_rt_train, y_train_0, scheduled_rt_test, y_test_0)
	print("time:" + str(time.time() - start_time))
	
	print("Baseline2: \n")
	start_time = time.time()
	mean_rt_train = [dm.mean_per_edge[t] for t in dm.dataset_train['edge_id']]
	mean_rt_test = [dm.mean_per_edge[t] for t in dm.dataset_test['edge_id']]
	
	du.train_validate_test(args,Baseline1(), mean_rt_train, y_train_0, mean_rt_test, y_test_0)
	print("time:" + str(time.time() - start_time))
	
	"""print("Gaussian Process: \n")
	start_time = time.time()
	c = np.random.choice(len(X_train_0), 1000, replace=False)
	du.train_validate_test(args,GaussianProcessRegressor(kernel=RBF(length_scale=1),alpha=1), "neg_mean_squared_error", np.array(X_train_0)[c,:], np.array(y_train_0)[c], X_test_0, y_test_0)
	print("time:" + str(time.time() - start_time))"""
	
	print("Random Forest: \n")
	start_time = time.time()
	du.train_validate_test(args,RandomForestRegressor(criterion='mse', random_state=1), X_train_0, y_train_0, X_test_0, y_test_0)
	print("time:" + str(time.time() - start_time))
	
	# for the following experiments we need the average and the standard deviation as well for evaluation
	
	edge_id = np.array([dm.dataX_train[0]]).T
	mean = np.array([dm.dataX_train[1]]).T
	sigma = np.array([dm.dataX_train[2]]).T
	X_train = np.concatenate((edge_id,mean,sigma,dm.dataX_train[3]),axis=1)
	
	edge_id = np.array([dm.dataX_test[0]]).T
	mean = np.array([dm.dataX_test[1]]).T
	sigma = np.array([dm.dataX_test[2]]).T
	X_test = np.concatenate((edge_id,mean,sigma,dm.dataX_test[3]),axis=1)
	
	print("Random Forest: \n")
	start_time = time.time()
	du.train_validate_test(args,RandomForestRegressor(criterion='mse', random_state=1), np.append(X_train_0,dm.dataX_train[3],axis=1), y_train_0, np.append(X_test_0,dm.dataX_test[3],axis=1), y_test_0)
	print("time:" + str(time.time() - start_time))
	
	print("Lasso: \n")
	start_time = time.time()
	du.train_validate_test(args, Lasso_standardized(alpha=0.2), X_train, dm.dataY_train, X_test, dm.dataY_test)
	print("time:" + str(time.time() - start_time))
	
	print("Ridge: \n")
	start_time = time.time()
	du.train_validate_test(args, Ridge_standardized(alpha=500), X_train, dm.dataY_train, X_test, dm.dataY_test)
	print("time:" + str(time.time() - start_time))
	
	
	
	
	print("TrainNet train-aware: \n")
	start_time = time.time()
	
	args.lr = 0.0001
	
	X_train = dm.dataX_train
	X_train.append(X_train_0[:,1])
	X_train.append(X_train_0[:,2])
	X_train.append(X_train_0[:,3])
	X_train.append(X_train_0[:,4])
	X_train.append(X_train_0[:,5])
	X_test = dm.dataX_test
	X_test.append(X_test_0[:,1])
	X_test.append(X_test_0[:,2])
	X_test.append(X_test_0[:,3])
	X_test.append(X_test_0[:,4])
	X_test.append(X_test_0[:,5])
	
	inputLengths = [len(dm.edge_vocab_map),len(dm.graph_vocab), len(dm.lt_vocab_map) ]
	du.train_validate_test_keras(args, TrainNet(args,inputLengths,train_agnostic=False), X_train, dm.dataY_train, X_test, dm.dataY_test)
	print("time:" + str(time.time() - start_time))
	
	print("graph_net: \n")
	start_time = time.time()
	args.lr = 0.01
	
	args.batch_size = 1
	
	du.train_validate_test_generators(args, dm, GraphNet(args,dm), dm.dataX_train, dm.dataY_train, dm.dataX_test, dm.dataY_test)
	print("time:" + str(time.time() - start_time))
