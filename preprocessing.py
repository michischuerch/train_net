import data_utils
import numpy as np
import sys
import pandas as pd
from multiprocessing import Pool


# choosing the important fields of the data
def extractImportantFields(dataset):
	n = dataset.shape[0]
	
	# only these fields are necessary
	dataset = pd.concat([dataset['ANKUNFTSZEIT'], 
				dataset['AN_PROGNOSE'], 
				dataset['ABFAHRTSZEIT'],
				dataset['AB_PROGNOSE'],
				dataset['AN_PROGNOSE_STATUS'], 
				dataset['VERKEHRSMITTEL_TEXT'], 
				dataset['HALTESTELLEN_NAME'], 
				dataset['BETREIBER_ABK'], 
				dataset['LINIEN_ID'], 
				dataset['LINIEN_TEXT'], 
				dataset['BPUIC']],axis=1)
	#data2 = pd.concat([dataset['ABFAHRTSZEIT'], dataset['AB_PROGNOSE'], dataset['AB_PROGNOSE_STATUS'], dataset['VERKEHRSMITTEL_TEXT'], dataset['HALTESTELLEN_NAME'], dataset['BETREIBER_ABK'], dataset['LINIEN_ID'], dataset['LINIEN_TEXT'], dataset['BPUIC']],axis=1)
	"""
	data1 = data1.rename(index=str, columns={"ANKUNFTSZEIT":"SCHEDULED_TIME", "AN_PROGNOSE":"ACTUAL_TIME", "AN_PROGNOSE_STATUS":"PROGNOSE_STATUS"})
	data2 = data2.rename(index=str, columns={"ABFAHRTSZEIT":"SCHEDULED_TIME", "AB_PROGNOSE":"ACTUAL_TIME", "AB_PROGNOSE_STATUS":"PROGNOSE_STATUS"})
	
	
	# add a column that tells us if the row belongs to a arrival or departure time
	data1['is_arrival'] = np.ones(dataset.shape[0], dtype=int)
	data2['is_arrival'] = np.zeros(dataset.shape[0], dtype=int)
	
	data = pd.concat([data1,data2])"""
	
	# throw away data that has no reported time...
	#data = removeInvalidData(data)
	
	# now add the delay to each row
	#data = add_delay_column(data)
	
	
	
	# rename field to reduce irritation
	dataset = dataset.rename(index=str, columns={"ANKUNFTSZEIT":"SCHEDULED_TIME_ARRIVAL", "AN_PROGNOSE":"ACTUAL_TIME_ARRIVAL"})
	dataset = dataset.rename(index=str, columns={"ABFAHRTSZEIT":"SCHEDULED_TIME_DEPARTURE", "AB_PROGNOSE":"ACTUAL_TIME_DEPARTURE"})
	
	
	dataset = dataset[dataset['BETREIBER_ABK'] == 'SBB']
	# drop the column, save some space
	dataset = dataset.drop('BETREIBER_ABK', 1)
	dataset = dataset[dataset['LINIEN_ID'].astype(int) < 40000]
	
	# remove everything that is not a measurement
	dataset = dataset[dataset['AN_PROGNOSE_STATUS'] != 'UNBEKANNT']
	dataset = dataset[dataset['AN_PROGNOSE_STATUS'] != 'PROGNOSE']
	
	# sort
	dataset = dataset.sort_values(by=['LINIEN_ID','ACTUAL_TIME_DEPARTURE'])	
	
	# add next information
	dataset['next_BPUIC'] = np.roll(dataset['BPUIC'],-1)
	dataset['next_SCHEDULED_TIME_ARRIVAL'] = np.roll(dataset['SCHEDULED_TIME_ARRIVAL'],-1)
	dataset['next_HALTESTELLEN_NAME'] = np.roll(dataset['HALTESTELLEN_NAME'],-1)
	dataset['next_ACTUAL_TIME_ARRIVAL'] = np.roll(dataset['ACTUAL_TIME_ARRIVAL'],-1)
	dataset = dataset.drop('ACTUAL_TIME_ARRIVAL', 1)
	dataset = dataset.drop('SCHEDULED_TIME_ARRIVAL', 1)
	
	# remove all data entries that are not valid 
	validMask = np.equal(np.roll(dataset['LINIEN_ID'],-1),dataset['LINIEN_ID'])
	dataset = dataset[validMask]
	
	# this should not happen but it seems sometimes there is an error in the dataset, 
	validMask = np.logical_not(pd.isnull(dataset['next_ACTUAL_TIME_ARRIVAL']))
	dataset = dataset[validMask]
	validMask = np.logical_not(pd.isnull(dataset['ACTUAL_TIME_DEPARTURE']))
	dataset = dataset[validMask]
	
	# only allow positive running times, it seems there can be an error in the dataset, or two trains with same train id
	dataset = dataset[dataset['next_ACTUAL_TIME_ARRIVAL'] > dataset['ACTUAL_TIME_DEPARTURE']]
	
	
	# add running time information
	rt = (dataset['next_ACTUAL_TIME_ARRIVAL']-dataset['ACTUAL_TIME_DEPARTURE']).astype('timedelta64[s]').astype(int)
	dataset['running_time'] = rt
	
	# add running time information
	rt = (dataset['next_SCHEDULED_TIME_ARRIVAL']-dataset['SCHEDULED_TIME_DEPARTURE']).astype('timedelta64[s]').astype(int)
	dataset['scheduled_running_time'] = rt
	
	# add delay information
	delays = (dataset['ACTUAL_TIME_DEPARTURE']-dataset['SCHEDULED_TIME_DEPARTURE']).astype('timedelta64[s]').astype(int)
	dataset['DEPARTURE_DELAY'] = delays
	
	# add delay information
	delays = (dataset['next_ACTUAL_TIME_ARRIVAL']-dataset['next_SCHEDULED_TIME_ARRIVAL']).astype('timedelta64[s]').astype(int)
	dataset['ARRIVAL_DELAY'] = delays
	
	
	# and add information about the next stop of the same train	
	#data = add_next_information(data)
	
	# add running time information
	#data = add_running_time_column(data)
	
	return dataset

"""def remove_outliers(dataset):
	# remove outliers
	
	allData = pd.concat(dataset)
	
	allData[np.logical_and(allData['BPUIC'] == 8501020,allData['next_BPUIC'] == 8501011)].to_csv("wtf2")
	
	mean_per_edge = allData.groupby(['BPUIC','next_BPUIC'])['running_time'].mean().to_dict()
	sigma_per_edge = allData.groupby(['BPUIC','next_BPUIC'])['running_time'].std().to_dict()
	
	all_removed = 0
	all_kept = 0
	output = []
	for d in dataset:
		
		all_mean = [mean_per_edge[e] for e in list(zip(d['BPUIC'],d['next_BPUIC']))]
		all_sigma = [sigma_per_edge[e] for e in list(zip(d['BPUIC'],d['next_BPUIC']))]
		
		safe_sigma = np.array(all_sigma)
		safe_sigma[safe_sigma==0] = 1
		
		mask = (d['running_time']-all_mean)/safe_sigma < 5
		output.append(d[mask])
		
		all_removed += np.sum(mask==0)
		all_kept += np.sum(mask==1)
	
	#allData[mask==0].to_csv("wtf")
	#print(all_mean)
	print("remove outliers: " + str(all_removed))
	print("non outliers: " + str(all_kept))
	
	return output"""
	
"""def extractFeatures(dm,dataset):
	
	dataX = np.zeros((dataset.shape[0],5),dtype=np.float64)
	dataY = np.zeros(dataset.shape[0],dtype=np.float64)
	
	date_delta = dataset['next_SCHEDULED_TIME']-dataset['SCHEDULED_TIME']

	srt = date_delta.astype('timedelta64[s]')
	srt = srt.astype(int)
	
	#dataX[:,0] = dataset['edge_id']
	dataX[:,0] = np.vectorize(dm.lt_vocab_map.get)(dataset['LINIEN_TEXT'])
	dataX[:,1] = dataset['SCHEDULED_TIME'].dt.weekday
	dataX[:,2] = dataset['SCHEDULED_TIME'].dt.hour 
	dataX[:,3] = dataset['delay']
	dataX[:,4] = srt

	#dataX[:,1] = np.vectorize(dm.vt_vocab_map.get)(dataset['VERKEHRSMITTEL_TEXT'])	
	#dataX[:,5] = dataset['SCHEDULED_TIME'].dt.minute
	#dataX[:,3] = [dm.line_id_vocab_map.get(str(d),0) for d in dataset['LINIEN_ID']]

	dataY = np.array(dataset['running_time'])
	
	return dataX,dataY"""
	
def prepare_X_y(dm,allData_train, dataTrain, dataTest, dataformat, predictionFormat, multi_input=False):
	
	# remove outliers from training set
	
	mean_per_edge = allData_train.groupby(['edge_id'])['running_time'].mean().to_dict()
	sigma_per_edge = allData_train.groupby(['edge_id'])['running_time'].std().to_dict()
	
	all_means_train = np.vectorize(mean_per_edge.get)(allData_train['edge_id'])
	all_sigma_train = np.vectorize(sigma_per_edge.get)(allData_train['edge_id'])
	
	saveDivision = all_sigma_train
	saveDivision[saveDivision==0]=1
	
	clipping = 5
	
	

	
	standardized = (allData_train['running_time']-all_means_train)/saveDivision
	
	print("clipping>" + str(np.sum(standardized>clipping)))
	print("clipping<=" + str(np.sum(standardized<=clipping)))
	
	#rt_clipped = np.array(allData_train['running_time'])
	#rt_clipped[standardized>clipping] = all_means_train[standardized>clipping] + clipping*all_sigma_train[standardized>clipping]
	allData_train = allData_train[standardized<clipping]
	
	mean_per_edge = allData_train.groupby(['edge_id'])['running_time'].mean().to_dict()
	sigma_per_edge = allData_train.groupby(['edge_id'])['running_time'].std().to_dict()
	
	allData_train['mean'] = np.vectorize(mean_per_edge.get)(allData_train['edge_id'])
	allData_train['sigma'] = np.vectorize(sigma_per_edge.get)(allData_train['edge_id'])
	
	
	#mean_per_edge = allData_train.groupby(['edge_id'])['running_time_clipped'].mean().to_dict()
	#sigma_per_edge = allData_train.groupby(['edge_id'])['running_time_clipped'].std().to_dict()
	
	# train information: train_type, weekday, hour, departure delay
	ti_train = np.concatenate([td[0][0] for td in dataTrain])
	# edge information: edge id
	ei_train = np.concatenate([td[0][1] for td in dataTrain])
	# graph information: last encountered running time per edge
	gi_train = np.concatenate([td[0][2] for td in dataTrain])
	y_train = np.array(np.concatenate([td[1] for td in dataTrain]),dtype=np.float)
	
	ti_test = np.concatenate([td[0][0] for td in dataTest])
	ei_test = np.concatenate([td[0][1] for td in dataTest])
	gi_test = np.concatenate([td[0][2] for td in dataTest])
	y_test = np.array(np.concatenate([td[1] for td in dataTest]),dtype=np.float)
	
	y_mean_train = np.array([mean_per_edge[et] for et in ei_train])
	y_mean_test = np.array([mean_per_edge[et] for et in ei_test])
	
	y_sigma_train = np.array([sigma_per_edge[et] for et in ei_train])
	y_sigma_test = np.array([sigma_per_edge[et] for et in ei_test])
	
	#print(y_mean_train)
	
	#ti_train[:,2] = ti_train[:,2] /24 * 0
	#ti_train[:,3] = np.clip(ti_train[:,3] - np.mean(ti_train[:,3]) / np.std(ti_train[:,3]),-1.0,1.0)
	
	
	
	"""y_mean_train = np.array([mean_per_edge[et] for et in ei_train])
	y_sigma_train = np.array([sigma_per_edge[et] for et in ei_train])
	
	y_mean_test = np.array([mean_per_edge[et] for et in ei_test])
	y_sigma_test = np.array([sigma_per_edge[et] for et in ei_test])"""
	
	graph_mean = allData_train.groupby(['graph_id'])['mean'].mean().to_dict()
	graph_sigma = allData_train.groupby(['graph_id'])['sigma'].mean().to_dict()
	
	#print(np.unique(allData_train[allData_train["edge_id"] == 2382]["sigma"]))
	
	
	mean_per_edge_graph = []
	sigma_per_edge_graph = []
	
	for i in range(len(graph_mean)-1):
		mean_per_edge_graph.append(graph_mean[i])
		sigma_per_edge_graph.append(graph_sigma[i])
	
	# to prevent singularity in sigma we set it to 1 if it is 0
	safe_division = [1 if x == 0 else x for x in sigma_per_edge_graph]
	
	mask = [gi_train==0] # these are empty entries
	graph_information_train = np.divide(np.subtract(gi_train,mean_per_edge_graph),safe_division)
	"""graph_information_train[graph_information_train>clipping]=clipping
	graph_information_train[graph_information_train<-clipping]=-clipping"""
	graph_information_train[tuple(mask)] = 0
	graph_information_train[graph_information_train>clipping] = clipping
	graph_information_train = graph_information_train/clipping
	
	saveDivision_train = y_sigma_train
	saveDivision_train[saveDivision_train==0]=1
	
	saveDivision_test = y_sigma_test
	saveDivision_test[saveDivision_test==0]=1
	
	#print(graph_information_train) 
	
	mask = [gi_test==0] # these are empty entries
	graph_information_test = np.divide(np.subtract(gi_test,mean_per_edge_graph),safe_division)
	#graph_information_test[graph_information_test>5] = 5
	"""graph_information_test[graph_information_test>clipping]=clipping
	graph_information_test[graph_information_test<-clipping]=-clipping"""
	graph_information_test[tuple(mask)] = 0
	graph_information_test[graph_information_test>clipping] = clipping
	graph_information_test = graph_information_test/clipping
	
	"""ti_train[:,0] = ti_train[:,0]*0
	ti_train[:,1] = ti_train[:,1]*0
	ti_train[:,2] = ti_train[:,2]*0"""
	
	#ti_train[:,2] = ti_train[:,2]
	ti_train[:,3] = np.divide(ti_train[:,3] -y_mean_train,saveDivision_train)
	ti_train[:,4] = np.divide(ti_train[:,4] -y_mean_train,saveDivision_train)
	
	ti_train[:,3][ti_train[:,3]>clipping] = clipping
	ti_train[:,3] = ti_train[:,3]/clipping
	
	ti_train[:,4][ti_train[:,4]>clipping] = clipping
	ti_train[:,4] = ti_train[:,4]/clipping
	
	"""ti_test[:,0] = ti_test[:,0]*0
	ti_test[:,1] = ti_test[:,1]*0
	ti_test[:,2] = ti_test[:,2]*0"""
	
	#ti_test[:,2] = ti_test[:,2]
	ti_test[:,3] = np.divide(ti_test[:,3] -y_mean_test,saveDivision_test)
	ti_test[:,4] = np.divide(ti_test[:,4] -y_mean_test,saveDivision_test)
	
	ti_test[:,3][ti_test[:,3]>clipping] = clipping
	ti_test[:,3] = ti_test[:,3]/clipping
	
	ti_test[:,4][ti_test[:,4]>clipping] = clipping
	ti_test[:,4] = ti_test[:,4]/clipping
	
	
	if dataformat == "allData":
		numerical_input_train = np.zeros((len(ti_train),2))
		numerical_input_test = np.zeros((len(ti_test),2))
		
		numerical_input_train[:,0:2] = ti_train[:,3:5]
		numerical_input_test[:,0:2] = ti_test[:,3:5]
		
		#print(numerical_input_train)
		
		#print(np.array(ti_train[:,0]))
		#sys.exit()
		
		
		#print(np.unique(ti_train[:,0]))
		
		
		#graph_information_test[graph_information_test<0]=0
		
		# standardize graph information
		X_train = [ti_train[:,0],ti_train[:,1],ti_train[:,2],numerical_input_train, ei_train,graph_information_train,y_mean_train, y_sigma_train]
		X_test = [ti_test[:,0],ti_test[:,1],ti_test[:,2],numerical_input_test, ei_test,graph_information_test,y_mean_test,y_sigma_test]
		
	elif dataformat == "onlyTrainInformation":
		
		#print(ti_train.shape, graph_information_train.shape)
		
		"""X_train = np.concatenate((ti_train,graph_information_train),axis=1)
		X_test = np.concatenate((ti_test,graph_information_test),axis=1)"""
		X_train = ti_train
		X_test = ti_test
		
		
	elif dataformat == "onlyGraphInformation":
		
		# standardize graph information
		X_train = graph_information_train
		X_test = graph_information_test
		
	elif dataformat == "concatBoth":
		X_train = np.concatenate((ti_train,gi_train),axis=1)
		X_test = np.concatenate((ti_test,gi_test),axis=1)
		
	elif dataformat == "baseline1":
		
		"""X_train = ti_train[:,4]
		X_test = ti_test[:,4]"""
		
		if(predictionFormat=="standardized"):
			X_train = y_mean_train * 0
			X_test = y_mean_test * 0
		else:
			X_train = y_mean_train
			X_test = y_mean_test
		
	

	
	
	
		
	#print(np.max(y_train))	
	
	print("max y train" + str(np.max(y_train)))
	print("min y train" + str(np.min(y_train)))
	print("mean/ std y train" + str(np.mean(y_train)) + " " + str(np.std(y_train)))
	print("max y test" + str(np.max(y_test)))
	print("min y test" + str(np.min(y_test)))
	print("mean/ std y test" + str(np.mean(y_test)) + " " + str(np.std(y_test)))
	
	"""if(predictionFormat=="standardized"):
		print("standardizing")
		print(y_train)
		print(y_mean_train)
		print(y_sigma_train)
		y_train = np.divide(np.subtract(y_train,y_mean_train),y_sigma_train)
		y_train[y_train>clipping] = clipping
		print(y_train)
		#y_test = np.divide(np.subtract(y_test,y_mean_test),y_sigma_test)
	elif(predictionFormat=="standardizedEverything"):
		y_train = np.divide(np.subtract(y_train,y_mean_train),y_sigma_train)
		#y_train[y_train>clipping] = clipping
		
		y_test = np.divide(np.subtract(y_test,y_mean_test),y_sigma_test)
		#y_test[y_test>clipping] = clipping
	elif(predictionFormat=="onlyCenter"):
		y_train = np.subtract(y_train,y_mean_train)
		y_test = np.subtract(y_test,y_mean_test)
		
		y_sigma_train[:] = 1
		y_sigma_test[:] = 1"""
		
	"""elif(predictionFormat=="values"):
		y_train = np.divide(np.subtract(y_train,y_mean_train),y_sigma_train)
		y_train[y_train>clipping] = clipping
		y_train = np.add(np.multiply(y_train,y_sigma_train),y_mean_train)"""
		
	"""y_test = np.divide(np.subtract(y_test,y_mean_test),y_sigma_test)
		y_test[y_test>clipping] = clipping
		y_test = np.add(np.multiply(y_test,y_sigma_test),y_mean_test)"""

	
	
	
	
	
	
	
	if(predictionFormat=="standardized"):
		y_train = np.divide(y_train-y_mean_train,saveDivision_train)
		y_test = np.divide(y_test-y_mean_test,saveDivision_test)

	# removing outliers
	if predictionFormat=="values":
		y_train_standardized = np.divide(y_train-y_mean_train,saveDivision_train)
	else:
		y_train_standardized = y_train
	
	#if not multi_input:
		
	#y_train = np.add(np.multiply(y_train,y_sigma_train),y_mean_train)
	

	
	if predictionFormat=="values":
		y_test_standardized = np.divide(y_test-y_mean_test,saveDivision_test)
	else:
		y_test_standardized = y_test
	
	if not multi_input:
		X_train = X_train[y_train_standardized<clipping]
		X_test = X_test[y_test_standardized<clipping]
	else:
		X_train = [x[y_train_standardized<clipping] for x in X_train]
		X_test = [x[y_test_standardized<clipping] for x in X_test]
	y_mean_train = y_mean_train[y_train_standardized<clipping]
	y_sigma_train = y_sigma_train[y_train_standardized<clipping]
	y_train = y_train[y_train_standardized<clipping]
	
	y_mean_test = y_mean_test[y_test_standardized<clipping]
	y_sigma_test = y_sigma_test[y_test_standardized<clipping]
	y_test = y_test[y_test_standardized<clipping]
	#y_test = np.add(np.multiply(y_test,y_sigma_test),y_mean_test)
	#####
	
	print("num removed: " + str(np.sum(y_test_standardized>=clipping) + np.sum(y_train_standardized>=clipping)))
	
	
	return X_train, y_train, X_test, y_test, y_mean_train ,y_mean_test,y_sigma_train ,y_sigma_test
