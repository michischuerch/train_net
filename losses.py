import math
import keras
import os
import tensorflow as tf
from keras import backend as K
from keras.layers import Dense, Input, LSTM, Embedding, Flatten, Lambda

def root_mean_squared_error(y_true, y_pred):
	out = K.sqrt(K.mean(K.square(y_pred - y_true), axis=-1))
	return out

def mean_squared_error(y_true, y_pred):
	out = K.mean(K.square(y_pred - y_true), axis=-1)
	return out


def aleatoric_loss(sigma2):
	def custom_loss(y_true, y_pred):
		
		return K.mean((1/sigma2)*K.square(y_pred - y_true) + K.log(sigma2), axis=-1)
	return custom_loss

def show_max_softmax(sm):
	def softmax(y_true, y_pred):
		return K.max(sm,axis=-1)
	return softmax
	
def show_num_0(output):
	def num_0(y_true, y_pred):
		return K.sum(K.cast(K.equal(output,0),'float16'),axis=-1)
	return num_0
	
# custom loss to incorporate a uncertainty estimate
"""def aleatoric_loss(y_true, y_pred):
	print(y_true)
	print(y_pred)
	
	y_true_mean = y_true[:,0:1]
	
	mean = y_pred[:,0:1]
	sigma2 = y_pred[:,1:2]
	
	out = 
	print(out)
	
	return out"""
	#return K.mean((1/sigma2)*K.square(mean - y_true) + K.log(sigma2), axis=-1)

# this function should not be used as a loss function, this is just to see how sigma behaves while training
def sigma(sigma2):
	def watch_sigma(y_true, y_pred):
		return K.mean(sigma2)
	return watch_sigma
